package config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Conf {

	private final static Conf instance = new Conf();
	private Properties props = new Properties();

	private Conf() {
		try {
			FileInputStream fis = new FileInputStream("src/config.properties");
			props.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Conf getInstance() {
		return instance;
	}

	public String getProperty(String prop) {
		return props.getProperty(prop);
	}

}
