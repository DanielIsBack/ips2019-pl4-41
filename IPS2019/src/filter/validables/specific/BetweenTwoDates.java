package filter.validables.specific;

import java.util.Date;

import filter.validables.Validable;

public class BetweenTwoDates implements Validable {

	private Date date, startingDate, endDate;

	public BetweenTwoDates(Date date, Date startingDate, Date endDate) {
		this.date = date;
		this.startingDate = startingDate;
		this.endDate = endDate;
	}

	@Override
	public boolean isValid() {
		if ((startingDate == null && endDate == null) || date == null)
			return true;
		if (startingDate == null)
			return date.compareTo(endDate) <= 0;
		if (endDate == null)
			return date.compareTo(startingDate) >= 0;
		return date.compareTo(startingDate) >= 0 && date.compareTo(endDate) <= 0;
	}

}
