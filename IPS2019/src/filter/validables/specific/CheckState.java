package filter.validables.specific;

import filter.validables.Validable;

public class CheckState implements Validable {

	private final static String ANY = "Cualquiera";

	private String state;
	private String acceptedState;

	public CheckState(String state, String acceptedState) {
		this.state = state;
		this.acceptedState = acceptedState;
	}

	@Override
	public boolean isValid() {
		if (acceptedState.equals(ANY))
			return true;
		return state.equals(acceptedState);
	}

}
