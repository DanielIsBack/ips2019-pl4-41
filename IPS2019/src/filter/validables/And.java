package filter.validables;

public class And implements Validable {

	private Validable v1, v2;

	public And(Validable v1, Validable v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	@Override
	public boolean isValid() {
		return v1.isValid() && v2.isValid();
	}

}
