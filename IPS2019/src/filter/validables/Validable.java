package filter.validables;

public interface Validable {

	boolean isValid();

}