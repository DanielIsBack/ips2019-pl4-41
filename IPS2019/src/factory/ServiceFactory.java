package factory;

import service.ActivityService;
import service.CosteInscripcionService;
import service.InscripcionCrudService;
import service.ParticipanteCrudService;
import service.PaymentService;
import service.SessionService;
import service.TeacherService;
import service.impl.ActivityServiceImpl;
import service.impl.ActivityTeacherServiceImpl;
import service.impl.CosteInscripcionServiceImpl;
import service.impl.InscripcionCrudServiceImpl;
import service.impl.ParticipanteCrudServiceImpl;
import service.impl.PaymentServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TeacherServiceImpl;

public class ServiceFactory {

	public static ActivityService getActivityService() {
		return new ActivityServiceImpl();
	}

	public static TeacherService getTeacherService() {
		return new TeacherServiceImpl();
	}

	public static InscripcionCrudService getInscriptionService() {
		return new InscripcionCrudServiceImpl();
	}

	public static InscripcionCrudService getInscripcionCrudService() {
		return new InscripcionCrudServiceImpl();
	}

	public static ParticipanteCrudService getParticipanteCrudService() {
		return new ParticipanteCrudServiceImpl();
	}

	public static PaymentService getPaymentService() {
		return new PaymentServiceImpl();
	}

	public static CosteInscripcionService getCosteInscripcionService() {
		return new CosteInscripcionServiceImpl();
	}
	
	public static SessionService getSessionService() {
		return new SessionServiceImpl();
	}
	
	public static ActivityTeacherServiceImpl getActivityTeacherService() {
		return new ActivityTeacherServiceImpl();
	}

}
