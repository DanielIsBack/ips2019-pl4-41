package factory;

import business.dto.ActivityDto;
import business.dto.ActivityTeacherDto;
import business.dto.PagoInscripcionDto;
import business.dto.SesionDto;
import business.dto.TeacherDto;
import persistence.Gateway;
import persistence.impl.ActivityGateway;
import persistence.impl.ActivityTeacherGateway;
import persistence.impl.CosteInscripcionGateway;
import persistence.impl.ParticipanteGateway;
import persistence.impl.PaymentGateway;
import persistence.impl.SessionGateway;
import persistence.impl.TeacherGateway;

public class PersistenceFactory {

	public static Gateway<ActivityDto> getActivityGateway() {
		return new ActivityGateway();
	}

	public static Gateway<TeacherDto> getTeacherGateway() {
		return new TeacherGateway();
	}

	public static Gateway<PagoInscripcionDto> getPaymentGateway() {
		return new PaymentGateway();
	}

	public static ParticipanteGateway getParticipanteGateway() {
		return new ParticipanteGateway();
	}

	public static CosteInscripcionGateway getCosteInscripcionGateway() {
		return new CosteInscripcionGateway();
	}

	public static Gateway<SesionDto> getSessionGateway() {
		return new SessionGateway();
	}
	
	public static Gateway<ActivityTeacherDto> getActivityTeacherGateway() {
		return new ActivityTeacherGateway();
	}

}
