package gui.activityplanification.teacherspanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.text.NumberFormatter;

import business.dto.ActivityTeacherDto;
import factory.ServiceFactory;

public class TeachersRemunerationPanel extends JPanel {

	private static final long serialVersionUID = -3095582535192027577L;
	private JScrollPane spTeachersRemuneration;
	private JTable tableTeachersRemuneration;
	private JPanel pnButtons;
	private JButton btnEliminarProfesor;
	
	public TeachersRemunerationPanel() {
		setLayout(new BorderLayout(0, 0));
		add(getSpTeachersRemuneration(), BorderLayout.CENTER);
		add(getPnButtons(), BorderLayout.SOUTH);

	}

	private JScrollPane getSpTeachersRemuneration() {
		if (spTeachersRemuneration == null) {
			spTeachersRemuneration = new JScrollPane();
			spTeachersRemuneration.setViewportView(getTableTeachersRemuneration());
		}
		return spTeachersRemuneration;
	}
	
	private JTable getTableTeachersRemuneration() {
		if (tableTeachersRemuneration == null) {
			String[] columnNames = {"Profesor", "Remuneracion"};
			DefaultTableModel dtm = new DefaultTableModel(columnNames, 0);
			tableTeachersRemuneration = new JTable(dtm) {
				
				private static final long serialVersionUID = 3663456150432520369L;

				@Override
				public boolean isCellEditable(int row, int column) {
					return column == 1;
				}
				
				@Override
				public TableCellEditor getCellEditor(int row, int column) {
					DecimalFormat doubleFormat = new DecimalFormat("#0.00");
					DecimalFormatSymbols dfs = new DecimalFormatSymbols();
					dfs.setDecimalSeparator('.');
					doubleFormat.setDecimalFormatSymbols(dfs);
					NumberFormatter nf = new NumberFormatter(doubleFormat);
					nf.setAllowsInvalid(false);
					JFormattedTextField ftf = new JFormattedTextField(nf);
					DefaultCellEditor dce = new DefaultCellEditor(ftf);
					if(column == 1) return dce;
					return super.getCellEditor(row, column);
				}
				
			};
			tableTeachersRemuneration.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableTeachersRemuneration.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		}
		return tableTeachersRemuneration;
	}
	
	public <T> void addToTable(List<T> list) {
		for(T o : list) {
			Object[] data = {o}; 
			DefaultTableModel dtm = (DefaultTableModel) getTableTeachersRemuneration().getModel();
			dtm.addRow(data);
			dtm.fireTableDataChanged();
		}
	}
	
	public List<ActivityTeacherDto> getTeacherDtos(int activity_id) {
		List<ActivityTeacherDto> teachers = new ArrayList<ActivityTeacherDto>();
		DefaultTableModel tm = (DefaultTableModel) getTableTeachersRemuneration().getModel();
		ActivityTeacherDto teacher;
		for(int row = 0; row < tm.getRowCount(); row++) {
			teacher = new ActivityTeacherDto();
			teacher.activity_id = activity_id;
			String dni = ((String) tm.getValueAt(row, 0)).split(",")[0];
			teacher.teacher_id = ServiceFactory.getTeacherService().findTeacherByDni(dni).id;
			
			// check teachers remuneration is defined
			String r = (String) tm.getValueAt(row, 1);
			double remuneration = r == null ? 0.00 : Double.parseDouble(r);
			teacher.remuneration = remuneration;
			
			teachers.add(teacher);
		}
		return teachers;
	}
	
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			pnButtons.add(getBtnEliminarProfesor());
		}
		return pnButtons;
	}
	
	private JButton getBtnEliminarProfesor() {
		if (btnEliminarProfesor == null) {
			btnEliminarProfesor = new JButton("Eliminar profesor");
			btnEliminarProfesor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					eliminarProfesor();
				}
			});
		}
		return btnEliminarProfesor;
	}
	
	private void eliminarProfesor() {
		DefaultTableModel tm = (DefaultTableModel) tableTeachersRemuneration.getModel();
		int selectedTeacher = tableTeachersRemuneration.getSelectedRow();
		if(selectedTeacher != -1) tm.removeRow(selectedTeacher);
	}
	
}
