package gui.activityplanification.teacherspanel;

import java.awt.GridLayout;
import java.util.List;

import javax.swing.JPanel;

import business.dto.ActivityTeacherDto;

public class TeachersPanel extends JPanel {

	private static final long serialVersionUID = -342489861614658745L;
	
	private TeachersListPanel pnTeachersList;
	private TeachersRemunerationPanel pnTeachersRemuneration;

	public TeachersPanel() {
		// initialize components
		pnTeachersRemuneration = new TeachersRemunerationPanel();
		pnTeachersList = new TeachersListPanel(pnTeachersRemuneration);
		
		// panel setup
		setLayout(new GridLayout(0, 1, 0, 0));
		add(pnTeachersList);
		add(pnTeachersRemuneration);
	}
	
	public List<ActivityTeacherDto> getTeacherDtos(int activity_id) {
		return pnTeachersRemuneration.getTeacherDtos(activity_id);
	}

}
