package gui.activityplanification.teacherspanel;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import business.dto.TeacherDto;
import factory.ServiceFactory;

public class TeachersListPanel extends JPanel {

	private static final long serialVersionUID = 4364764755588259779L;
	private JPanel pnTeachersListHeader;
	private JScrollPane spTeachersList;
	private JPanel pnButtons;
	private JButton btnAadirProfesor;
	private JLabel lblSeleccionarProfesores;
	private JList<String> listTeachers;
	
	// panel for teacher remuneration
	private TeachersRemunerationPanel pnTeachersRemuneration;

	public TeachersListPanel(TeachersRemunerationPanel pnTeachersRemuneration) {
		// save reference for where to add teacher
		this.pnTeachersRemuneration = pnTeachersRemuneration;
		
		// panel setup
		setLayout(new BorderLayout(0, 0));
		add(getSpTeachersList(), BorderLayout.CENTER);
		add(getPnTeachersListHeader(), BorderLayout.NORTH);
		add(getPnButtons(), BorderLayout.SOUTH);
	}
	
	private JPanel getPnTeachersListHeader() {
		if (pnTeachersListHeader == null) {
			pnTeachersListHeader = new JPanel();
			pnTeachersListHeader.add(getLblSeleccionarProfesores());
		}
		return pnTeachersListHeader;
	}
	
	private JScrollPane getSpTeachersList() {
		if (spTeachersList == null) {
			spTeachersList = new JScrollPane();
			spTeachersList.setViewportView(getListTeachers());
		}
		return spTeachersList;
	}
	
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			pnButtons.add(getBtnAadirProfesor());
		}
		return pnButtons;
	}
	private JButton getBtnAadirProfesor() {
		if (btnAadirProfesor == null) {
			btnAadirProfesor = new JButton("A\u00F1adir profesor");
			btnAadirProfesor.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					pnTeachersRemuneration.addToTable(getListTeachers().getSelectedValuesList());
				}
			});
		}
		return btnAadirProfesor;
	}
	private JLabel getLblSeleccionarProfesores() {
		if (lblSeleccionarProfesores == null) {
			lblSeleccionarProfesores = new JLabel("Seleccionar profesores para la actividad");
			lblSeleccionarProfesores.setFont(new Font("Tahoma", Font.BOLD, 16));
		}
		return lblSeleccionarProfesores;
	}
	
	private JList<String> getListTeachers() {
		if (listTeachers == null) {
			listTeachers = new JList<String>();
			DefaultListModel<String> dlm = new DefaultListModel<String>();
			List<TeacherDto> teachers = ServiceFactory.getTeacherService().listTeachers();
			String teacherDisplay;
			for(TeacherDto t : teachers) {
				teacherDisplay = String.format("%s, %s %s", t.dni, t.name, t.surname);
				dlm.addElement(teacherDisplay);
			}
			listTeachers.setModel(dlm);
		}
		return listTeachers;
	}
	
}
