package gui.activityplanification;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import com.toedter.calendar.JDateChooser;

import business.dto.ActivityDto;
import business.dto.ActivityTeacherDto;
import business.dto.CosteInscripcionDto;
import business.dto.SesionDto;
import exception.BusinessException;
import factory.ServiceFactory;
import filter.validables.specific.BetweenTwoDates;
import gui.activityplanification.costesPanel.CostesListPanel;
import gui.activityplanification.sessionspanel.SessionListPanel;
import gui.activityplanification.teacherspanel.TeachersPanel;

public class ActivityPlanificationWindow extends JDialog {

	private static final long serialVersionUID = 1L;
	private JLabel lblActivityPlanification;
	private JPanel pnButtons;
	private JButton btnCancel;
	private JButton btnOk;
	private JPanel pnForm;
	private JPanel pnName;
	private JLabel lblName;
	private JTextField tfName;
	private JPanel pnObjectives;
	private JLabel lblObjectives;
	private JScrollPane scrollPaneObjectives;
	private JTextArea taObjectives;
	private JPanel pnContents;
	private JLabel lblContents;
	private JScrollPane scrollPaneContents;
	private JTextArea taContents;
	private JPanel pnInscription;
	private JPanel pnInscriptionStart;
	private JLabel lblInscriptionStart;
	private JDateChooser dcInscriptionStart;
	private JPanel pnInscriptionEnd;
	private JLabel lblInscriptionEnd;
	private JDateChooser dcInscriptionEnd;
	private JPanel pnPlaces;
	private JLabel lblPlaces;
	private JSpinner spPlaces;
	private JPanel pnLeft;
	private JPanel pnRight;
	
	// sessions panel
	private SessionListPanel pnSessionList;
	
	// teachers panel
	private TeachersPanel pnTeachers;
	
	private CostesListPanel pnCostes;
	private JPanel pnlNorte;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ActivityPlanificationWindow dialog = new ActivityPlanificationWindow();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public ActivityPlanificationWindow() {
		setMinimumSize(new Dimension(800, 600));
		this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
		setTitle("Creaci�n de Actividades");
		
		// initialization
		pnSessionList = new SessionListPanel();
		pnTeachers = new TeachersPanel();
		
		getContentPane().add(getLblActivityPlanification(), BorderLayout.NORTH);
		getContentPane().add(getPnButtons(), BorderLayout.SOUTH);
		getContentPane().add(getPnForm(), BorderLayout.CENTER);

	}

	private JLabel getLblActivityPlanification() {
		if (lblActivityPlanification == null) {
			lblActivityPlanification = new JLabel("Planificaci\u00F3n de actividad");
			lblActivityPlanification.setHorizontalAlignment(SwingConstants.CENTER);
			lblActivityPlanification.setFont(new Font("Tahoma", Font.BOLD, 28));
		}
		return lblActivityPlanification;
	}

	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnButtons.getLayout();
			flowLayout.setAlignment(FlowLayout.RIGHT);
			pnButtons.add(getBtnOk());
			pnButtons.add(getBtnCancel());
		}
		return pnButtons;
	}

	private JButton getBtnCancel() {
		if (btnCancel == null) {
			btnCancel = new JButton("Cancel");
			btnCancel.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		}
		return btnCancel;
	}

	private JButton getBtnOk() {
		if (btnOk == null) {
			btnOk = new JButton("OK");
			btnOk.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					// fill activity dto
					ActivityDto activity = fillActivityDto();
					
					// fill session dtos
					List<SesionDto> sessions = pnSessionList.getSessionDtos(activity.id);
					
					// fill activity teacher dtos
					List<ActivityTeacherDto> teachers =  pnTeachers.getTeacherDtos(activity.id);

					//fill coste inscripciones
					List<CosteInscripcionDto> costes = pnCostes.getCosteInscripcionDtos(activity.id); //fillCosteInscripcionDto(activity.id);
					
					// add activity, sessions and teachers, inscription costs
					try {
						ServiceFactory.getActivityService().addActivity(activity);
						
						for(SesionDto session : sessions) {
							ServiceFactory.getSessionService().addSession(session);
						}
						
						for(ActivityTeacherDto activityTeacher : teachers) {
							ServiceFactory.getActivityTeacherService().addActivityTeacher(activityTeacher);
						}
						
						for(CosteInscripcionDto coste : costes) {
							ServiceFactory.getCosteInscripcionService().addCosteInscripcion(coste);
						}
						
						// if everything goes well
						JOptionPane.showMessageDialog(rootPane, "Actividad creada!");
						dispose();
					} catch (BusinessException be) {
						JOptionPane.showMessageDialog(rootPane, be.getMessage(), "Campo incorrecto",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			});
		}
		return btnOk;
	}

	private JPanel getPnForm() {
		if (pnForm == null) {
			pnForm = new JPanel();
			pnForm.setLayout(new GridLayout(0, 2, 0, 0));
			pnForm.add(getPnLeft());
			pnForm.add(getPnRight());
		}
		return pnForm;
	}

	private JPanel getPnName() {
		if (pnName == null) {
			pnName = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnName.getLayout();
			flowLayout.setVgap(60);
			pnName.add(getLblName());
			pnName.add(getTfName());
		}
		return pnName;
	}

	private JLabel getLblName() {
		if (lblName == null) {
			lblName = new JLabel("Nombre:");
		}
		return lblName;
	}

	private JTextField getTfName() {
		if (tfName == null) {
			tfName = new JTextField();
			tfName.setColumns(30);
		}
		return tfName;
	}

	private JPanel getPnObjectives() {
		if (pnObjectives == null) {
			pnObjectives = new JPanel();
			pnObjectives.add(getLblObjectives());
			pnObjectives.add(getScrollPaneObjectives());
		}
		return pnObjectives;
	}

	private JLabel getLblObjectives() {
		if (lblObjectives == null) {
			lblObjectives = new JLabel("Objetivos:");
		}
		return lblObjectives;
	}

	private JScrollPane getScrollPaneObjectives() {
		if (scrollPaneObjectives == null) {
			scrollPaneObjectives = new JScrollPane();
			scrollPaneObjectives.setViewportView(getTaObjectives());
		}
		return scrollPaneObjectives;
	}

	private JTextArea getTaObjectives() {
		if (taObjectives == null) {
			taObjectives = new JTextArea();
			taObjectives.setColumns(40);
			taObjectives.setRows(2);
			taObjectives.setLineWrap(true);
		}
		return taObjectives;
	}

	private JPanel getPnContents() {
		if (pnContents == null) {
			pnContents = new JPanel();
			pnContents.add(getLblContents());
			pnContents.add(getScrollPaneContents());
		}
		return pnContents;
	}

	private JLabel getLblContents() {
		if (lblContents == null) {
			lblContents = new JLabel("Contenidos:");
		}
		return lblContents;
	}

	private JScrollPane getScrollPaneContents() {
		if (scrollPaneContents == null) {
			scrollPaneContents = new JScrollPane();
			scrollPaneContents.setViewportView(getTaContents());
		}
		return scrollPaneContents;
	}

	private JTextArea getTaContents() {
		if (taContents == null) {
			taContents = new JTextArea();
			taContents.setRows(2);
			taContents.setLineWrap(true);
			taContents.setColumns(40);
		}
		return taContents;
	}

	private JPanel getPnInscription() {
		if (pnInscription == null) {
			pnInscription = new JPanel();
			pnInscription.setLayout(new GridLayout(0, 2, 0, 0));
			pnInscription.add(getPnInscriptionStart());
			pnInscription.add(getPnInscriptionEnd());
		}
		return pnInscription;
	}

	private JPanel getPnInscriptionStart() {
		if (pnInscriptionStart == null) {
			pnInscriptionStart = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnInscriptionStart.getLayout();
			flowLayout.setVgap(60);
			pnInscriptionStart.add(getLblInscriptionStart());
			pnInscriptionStart.add(getDcInscriptionStart());
		}
		return pnInscriptionStart;
	}

	private JLabel getLblInscriptionStart() {
		if (lblInscriptionStart == null) {
			lblInscriptionStart = new JLabel("Inicio Inscripci\u00F3n:");
		}
		return lblInscriptionStart;
	}

	private JDateChooser getDcInscriptionStart() {
		if (dcInscriptionStart == null) {
			dcInscriptionStart = new JDateChooser();
		}
		return dcInscriptionStart;
	}

	private JPanel getPnInscriptionEnd() {
		if (pnInscriptionEnd == null) {
			pnInscriptionEnd = new JPanel();
			FlowLayout flowLayout = (FlowLayout) pnInscriptionEnd.getLayout();
			flowLayout.setVgap(60);
			pnInscriptionEnd.add(getLblInscriptionEnd());
			pnInscriptionEnd.add(getDcInscriptionEnd());
		}
		return pnInscriptionEnd;
	}

	private JLabel getLblInscriptionEnd() {
		if (lblInscriptionEnd == null) {
			lblInscriptionEnd = new JLabel("Fin Inscripci\u00F3n:");
		}
		return lblInscriptionEnd;
	}

	private JDateChooser getDcInscriptionEnd() {
		if (dcInscriptionEnd == null) {
			dcInscriptionEnd = new JDateChooser();
		}
		return dcInscriptionEnd;
	}

	private JPanel getPnPlaces() {
		if (pnPlaces == null) {
			pnPlaces = new JPanel();
			pnPlaces.add(getLblPlaces());
			pnPlaces.add(getSpPlaces());
		}
		return pnPlaces;
	}

	private JLabel getLblPlaces() {
		if (lblPlaces == null) {
			lblPlaces = new JLabel("Numero de plazas:");
		}
		return lblPlaces;
	}

	private JSpinner getSpPlaces() {
		if (spPlaces == null) {
			spPlaces = new JSpinner();
			spPlaces.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
			spPlaces.setPreferredSize(new Dimension(50, 20));
		}
		return spPlaces;
	}

	private JPanel getPnLeft() {
		if (pnLeft == null) {
			pnLeft = new JPanel();
			pnLeft.setLayout(new BorderLayout(0, 0));
			pnCostes = new CostesListPanel();
			pnLeft.add(pnCostes, BorderLayout.CENTER);
			pnLeft.add(getPnInscription(), BorderLayout.SOUTH);
			pnLeft.add(getPnlNorte(), BorderLayout.NORTH);
		}
		return pnLeft;
	}
	private JPanel getPnRight() {
		if (pnRight == null) {
			pnRight = new JPanel();
			pnRight.setLayout(new GridLayout(0, 1, 0, 0));
			pnRight.add(pnSessionList);
			pnRight.add(pnTeachers);
		}
		return pnRight;
	}
	
	private ActivityDto fillActivityDto() {
		ActivityDto activity = new ActivityDto();
		
		// fill activity dto
		activity.id = ServiceFactory.getActivityService().getNextActivityId();
		activity.name = getTfName().getText();
		activity.objectives = getTaObjectives().getText();
		activity.contents = getTaContents().getText();
		activity.startInscriptionDate = getDcInscriptionStart().getDate();
		activity.endInscriptionDate = getDcInscriptionEnd().getDate();
		activity.numOfPlaces = (int) getSpPlaces().getValue();
		
		java.util.Date fechaHoy = new java.util.Date();
		
		if(new BetweenTwoDates(fechaHoy, activity.startInscriptionDate, activity.endInscriptionDate).isValid()) {
			activity.state = "Periodo de Inscripci�n";
		}
		
		return activity;
	}
	private JPanel getPnlNorte() {
		if (pnlNorte == null) {
			pnlNorte = new JPanel();
			pnlNorte.setLayout(new BoxLayout(pnlNorte, BoxLayout.Y_AXIS));
			pnlNorte.add(getPnName());
			pnlNorte.add(getPnObjectives());
			pnlNorte.add(getPnContents());
			pnlNorte.add(getPnPlaces());
		}
		return pnlNorte;
	}

}
