package gui.activityplanification.sessionspanel;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JScrollPane;

import business.dto.SesionDto;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class SessionListPanel extends JPanel {

	private static final long serialVersionUID = -1289313834694316081L;
	private JScrollPane spSessionList;
	private JPanel pnSessionList;
	private JPanel pnButtons;
	private JButton btnAadirSesin;
	private JButton btnBorrarSesin;

	/**
	 * Create the panel.                                                                                                          
	 */
	public SessionListPanel() {
		setLayout(new BorderLayout(0, 0));
		add(getSpSessionList(), BorderLayout.CENTER);
		add(getPnButtons(), BorderLayout.SOUTH);

	}

	private JScrollPane getSpSessionList() {
		if (spSessionList == null) {
			spSessionList = new JScrollPane();
			spSessionList.getVerticalScrollBar().setUnitIncrement(12);
			spSessionList.setViewportView(getPnSessionList());
		}
		return spSessionList;
	}
	private JPanel getPnSessionList() {
		if (pnSessionList == null) {
			pnSessionList = new JPanel();
			pnSessionList.setLayout(new BoxLayout(pnSessionList, BoxLayout.Y_AXIS));
			pnSessionList.add(new SessionPanel());
		}
		return pnSessionList;
	}
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			pnButtons.add(getBtnAadirSesion());
			pnButtons.add(getBtnBorrarSesion());
		}
		return pnButtons;
	}
	private JButton getBtnAadirSesion() {
		if (btnAadirSesin == null) {
			btnAadirSesin = new JButton("A\u00F1adir sesion");
			btnAadirSesin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					getPnSessionList().add(new SessionPanel());
					getSpSessionList().getViewport().validate();
				}
			});
		}
		return btnAadirSesin;
	}
	private JButton getBtnBorrarSesion() {
		if (btnBorrarSesin == null) {
			btnBorrarSesin = new JButton("Borrar sesion");
			btnBorrarSesin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
		}
		return btnBorrarSesin;
	}
	
	public List<SesionDto> getSessionDtos(int activity_id) {
		List<SesionDto> sessions = new ArrayList<SesionDto>();
		SesionDto session;
		for(Component p : getPnSessionList().getComponents()) {
			session = ((SessionPanel) p).getSessionDto();
			session.id_act = activity_id;
			sessions.add(session);
		}
		return sessions;
	}
	
}
