//package gui.activityplanification.sessionspanel.representation;
//
//import java.awt.Component;
//
//import javax.swing.JTable;
//import javax.swing.table.TableCellRenderer;
//
//import business.dto.SesionDto;
//import gui.activityplanification.sessionspanel.SessionPanel;
//
//public class SessionCellRenderer implements TableCellRenderer {
//	
//	private SessionPanel sessionPanel;
//	
//	public SessionCellRenderer() {
//		sessionPanel = new SessionPanel();
//	}
//
//	@Override
//	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
//			int row, int column) {
//		SesionDto session = (SesionDto) value;
//		sessionPanel.updateData(session, isSelected, table);
//		return sessionPanel;
//	}
//	
//	public int getCellHeight() {
//		return sessionPanel.getPreferredSize().height;
//	}
//
//}
