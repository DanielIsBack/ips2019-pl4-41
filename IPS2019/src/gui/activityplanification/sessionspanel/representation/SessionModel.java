package gui.activityplanification.sessionspanel.representation;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import business.dto.SesionDto;

public class SessionModel extends AbstractTableModel {
	
	private static final long serialVersionUID = 391892534428607980L;
	List<SesionDto> sessions;
	
	public SessionModel() {
		sessions = new ArrayList<SesionDto>();
	}
	
	public SessionModel(List<SesionDto> sessions) {
		this.sessions = sessions;
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public int getRowCount() {
		return sessions.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return sessions.get(rowIndex);
	}
	
	@Override
	public Class<SesionDto> getColumnClass(int columnIndex) {
		return SesionDto.class;
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return true;
	} 
	
	public void addRow() {
		sessions.add(new SesionDto());
	}
	
	public void removeRow(int rowIndex) {
		if(rowIndex > 0 && rowIndex < sessions.size())
			sessions.remove(rowIndex);
	}
	
	public List<SesionDto> getModelData() {
		return new ArrayList<SesionDto>(this.sessions);
	}
	
	public String toString() {
		StringBuilder content = new StringBuilder();
		for(SesionDto s : sessions) {
			String session = String.format("[Place: %s]", s.lugar);
			content.append(session);
		}
		return content.toString();
	}

}
