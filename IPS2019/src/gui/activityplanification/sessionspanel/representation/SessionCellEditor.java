//package gui.activityplanification.sessionspanel.representation;
//
//import java.awt.Component;
//
//import javax.swing.AbstractCellEditor;
//import javax.swing.JTable;
//import javax.swing.table.TableCellEditor;
//
//import business.dto.SesionDto;
//import gui.activityplanification.sessionspanel.SessionPanel;
//
//public class SessionCellEditor extends AbstractCellEditor implements TableCellEditor {
//
//	private static final long serialVersionUID = 1463914414694739081L;
//	private SessionPanel sessionPanel;
//	
//	public SessionCellEditor() {
//		sessionPanel = new SessionPanel();
//	}
//
//	@Override
//	public Object getCellEditorValue() {
//		return null;
//	}
//
//	@Override
//	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
//		SesionDto session = (SesionDto) value;
////		System.out.println("SessionPanel: " + sessionPanel.getSession().toString());
////		System.out.println("Session: " + session.toString());
//		sessionPanel.updateData(session, true, table);
//		return sessionPanel;
//	}
//	
//}
