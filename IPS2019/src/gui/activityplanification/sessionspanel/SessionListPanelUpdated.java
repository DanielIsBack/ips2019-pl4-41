package gui.activityplanification.sessionspanel;
//package gui.activityplanification.sessionspanel;
//
//import java.awt.BorderLayout;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import javax.swing.JButton;
//import javax.swing.JPanel;
//import javax.swing.JScrollPane;
//import javax.swing.JTable;
//
//import business.dto.SesionDto;
//import gui.activityplanification.sessionspanel.representation.SessionCellEditor;
//import gui.activityplanification.sessionspanel.representation.SessionCellRenderer;
//import gui.activityplanification.sessionspanel.representation.SessionModel;
//
//public class SessionListPanel extends JPanel {
//
//	private static final long serialVersionUID = -1289313834694316081L;
//	private JScrollPane spSessionList;
//	private JPanel pnButtons;
//	private JButton btnAadirSesin;
//	private JButton btnBorrarSesin;
//	private JTable tableSessions;
//
//	/**
//	 * Create the panel.                                                                                                          
//	 */
//	public SessionListPanel() {
//		setLayout(new BorderLayout(0, 0));
//		add(getSpSessionList(), BorderLayout.CENTER);
//		add(getPnButtons(), BorderLayout.SOUTH);
//
//	}
//
//	private JScrollPane getSpSessionList() {
//		if (spSessionList == null) {
//			spSessionList = new JScrollPane();
//			spSessionList.setViewportView(getTableSessions());
//			spSessionList.getVerticalScrollBar().setUnitIncrement(12);
//		}
//		return spSessionList;
//	}
//	private JPanel getPnButtons() {
//		if (pnButtons == null) {
//			pnButtons = new JPanel();
//			pnButtons.add(getBtnAadirSesion());
//			pnButtons.add(getBtnBorrarSesion());
//		}
//		return pnButtons;
//	}
//	private JButton getBtnAadirSesion() {
//		if (btnAadirSesin == null) {
//			btnAadirSesin = new JButton("A\u00F1adir sesion");
//			btnAadirSesin.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					addSession();
//				}
//			});
//		}
//		return btnAadirSesin;
//	}
//	private JButton getBtnBorrarSesion() {
//		if (btnBorrarSesin == null) {
//			btnBorrarSesin = new JButton("Borrar sesion");
//			btnBorrarSesin.addActionListener(new ActionListener() {
//				public void actionPerformed(ActionEvent e) {
//					int selectedRow = getTableSessions().getSelectedRow();
//					removeSession(selectedRow);
//				}
//			});
//		}
//		return btnBorrarSesin;
//	}
//	
//	public List<SesionDto> getSessionDtos(int activity_id) {
//		SessionModel sm = (SessionModel) getTableSessions().getModel();
//		return sm.getModelData().stream().map(s -> {
//			s.id_act = activity_id;
//			return s;
//		}).collect(Collectors.toList());
//	}
//	
//	private JTable getTableSessions() {
//		if (tableSessions == null) {
////			List<SesionDto> sessions = new ArrayList<SesionDto>();
////			sessions.add(new SesionDto());
//			tableSessions = new JTable(new SessionModel());
//			SessionCellRenderer sessionCellRenderer = new SessionCellRenderer();
//			SessionCellEditor sessionCellEditor = new SessionCellEditor();
//			tableSessions.setDefaultRenderer(SesionDto.class, sessionCellRenderer);
//			tableSessions.setDefaultEditor(SesionDto.class, sessionCellEditor);
//			tableSessions.setRowHeight(sessionCellRenderer.getCellHeight());
//			tableSessions.setTableHeader(null);
//		}
//		return tableSessions;
//	}
//	
//	private void addSession() {
//		SessionModel sm = (SessionModel) getTableSessions().getModel();
//		sm.addRow();
//		sm.fireTableDataChanged();
//		getSpSessionList().getViewport().validate();
//	}
//	
//	private void removeSession(int rowIndex) {
//		SessionModel sm = (SessionModel) getTableSessions().getModel();
//		sm.removeRow(rowIndex);
//		sm.fireTableDataChanged();
//		getSpSessionList().getViewport().validate();
//	}
//	
//}
