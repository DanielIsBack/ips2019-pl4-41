package gui.activityplanification.sessionspanel;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.border.LineBorder;

import com.toedter.calendar.JDateChooser;

import business.dto.SesionDto;

public class SessionPanel extends JPanel {

	private static final long serialVersionUID = 6333977355857578910L;
	private JLabel lblSpace;
	private JLabel lblDate;
	private JLabel lblStartHour;
	private JLabel lblEndHour;
	private JPanel panelDate;
	private JPanel panelSpace;
	private JPanel panelStartHour;
	private JPanel panelEndHour;
	private JDateChooser dcDate;
	private JTextField tfSpace;
	private JSpinner spStartHour;
	private JSpinner spEndHour;
	
	// session it represents
	SesionDto session;

	/**
	 * Create the panel.
	 */
	public SessionPanel() {
		session = new SesionDto();
		setMinimumSize(new Dimension(10, 135));
		setMaximumSize(new Dimension(32767, 135));
		setPreferredSize(new Dimension(470, 135));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(new GridLayout(2, 2, 0, 0));
		add(getPanelDate());
		add(getPanelStartHour());
		add(getPanelSpace());
		add(getPanelEndHour());

	}

	private JLabel getLblSpace() {
		if (lblSpace == null) {
			lblSpace = new JLabel("Espacio:");
		}
		return lblSpace;
	}

	private JLabel getLblDate() {
		if (lblDate == null) {
			lblDate = new JLabel("Fecha:");
			lblDate.setAlignmentX(Component.CENTER_ALIGNMENT);
		}
		return lblDate;
	}

	private JLabel getLblStartHour() {
		if (lblStartHour == null) {
			lblStartHour = new JLabel("Hora de inicio:");
		}
		return lblStartHour;
	}

	private JLabel getLblEndHour() {
		if (lblEndHour == null) {
			lblEndHour = new JLabel("Hora de fin:");
		}
		return lblEndHour;
	}

	private JPanel getPanelDate() {
		if (panelDate == null) {
			panelDate = new JPanel();
			panelDate.add(getLblDate());
			panelDate.add(getDcDate());
		}
		return panelDate;
	}

	private JPanel getPanelSpace() {
		if (panelSpace == null) {
			panelSpace = new JPanel();
			panelSpace.add(getLblSpace());
			panelSpace.add(getTfSpace());
		}
		return panelSpace;
	}

	private JPanel getPanelStartHour() {
		if (panelStartHour == null) {
			panelStartHour = new JPanel();
			panelStartHour.add(getLblStartHour());
			panelStartHour.add(getSpStartHour());
		}
		return panelStartHour;
	}

	private JPanel getPanelEndHour() {
		if (panelEndHour == null) {
			panelEndHour = new JPanel();
			panelEndHour.add(getLblEndHour());
			panelEndHour.add(getSpEndHour());
		}
		return panelEndHour;
	}

	private JDateChooser getDcDate() {
		if (dcDate == null) {
			dcDate = new JDateChooser();
		}
		return dcDate;
	}

	private JTextField getTfSpace() {
		if (tfSpace == null) {
			tfSpace = new JTextField();
			tfSpace.setColumns(20);
		}
		return tfSpace;
	}

	private JSpinner getSpStartHour() {
		if (spStartHour == null) {
			spStartHour = new JSpinner();
			spStartHour.setModel(new SpinnerDateModel(new Date(), null, null, Calendar.HOUR_OF_DAY));
			JSpinner.DateEditor de = new JSpinner.DateEditor(spStartHour, "HH:mm");
			spStartHour.setEditor(de);
		}
		return spStartHour;
	}

	private JSpinner getSpEndHour() {
		if (spEndHour == null) {
			spEndHour = new JSpinner();
			spEndHour.setModel(new SpinnerDateModel(new Date(), null, null, Calendar.HOUR_OF_DAY));
			JSpinner.DateEditor de = new JSpinner.DateEditor(spEndHour, "HH:mm");
			spEndHour.setEditor(de);
		}
		return spEndHour;
	}

	public SesionDto getSessionDto() {
		SesionDto session = new SesionDto();
		
		// convert objects to dates
		Date sHourDate = (Date) getSpStartHour().getValue();
		Date eHourDate = (Date) getSpEndHour().getValue();
		
//		long valuestart = (long)getSpStartHour().getValue();
//		long valueend = (long)getSpEndHour().getValue();
		
		// fill sesion dto
		session.lugar = getTfSpace().getText();
		
		//session.fecha = getDcDate().getDate();
		//session.h_comienzo = new Time(sHourDate.getTime());
		//session.h_finalizacion = new Time(eHourDate.getTime());	 
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(sHourDate);
		
		long millis_start = (cal.get(Calendar.HOUR_OF_DAY) * 3600000) + (cal.get(Calendar.MINUTE)) * 60000;
		
		cal.setTime(eHourDate);
		long millis_end = (cal.get(Calendar.HOUR_OF_DAY) * 3600000) + (cal.get(Calendar.MINUTE)) * 60000;
		
		session.f_comienzo = new Date(getDcDate().getDate().getTime() + millis_start);
		session.f_finalizacion = new Date(getDcDate().getDate().getTime() + millis_end);

		return session;
	}

//	public void updateData(SesionDto session, boolean isSelected, JTable table) {
//		this.session = session;
//		
//		// background for selection
//		Color background = isSelected ? table.getSelectionBackground() : table.getBackground();
//		setBackground(background);
//		
//		// update components
//		getTfSpace().setText(session.lugar); // space
//		getDcDate().setDate(session.f_comienzo); // date
////		getSpStartHour().setValue(session.f_comienzo); //shour
////		getSpEndHour().setValue(session.f_finalizacion); //ehour
//	}
//	
//	public SesionDto getSession()  {
//		return session;
//	}

}
