package gui.activityplanification.costesPanel;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import business.dto.CosteInscripcionDto;

public class CostesListPanel extends JPanel {

	private static final long serialVersionUID = -1289313834694316081L;
	private JScrollPane spCostesList;
	private JPanel pnCostesList;
	private JPanel pnButtons;
	private JButton btnAddCoste;
	private JButton btnBorrarCoste;
	private CostesPanel costesPanel1;
	private CostesPanel costesPanel2;
	private CostesPanel costesPanel;
	private CostesPanel costesPanel3;

	/**
	 * Create the panel.                                                                                                          
	 */
	public CostesListPanel() {
		setLayout(new BorderLayout(0, 0));
		add(getSpCostesList(), BorderLayout.CENTER);
		add(getPnButtons(), BorderLayout.SOUTH);

	}

	private JScrollPane getSpCostesList() {
		if (spCostesList == null) {
			spCostesList = new JScrollPane();
			spCostesList.getVerticalScrollBar().setUnitIncrement(12);
			spCostesList.setViewportView(getPnCostesList());
		}
		return spCostesList;
	}
	private JPanel getPnCostesList() {
		if (pnCostesList == null) {
			pnCostesList = new JPanel();
			pnCostesList.setLayout(new GridLayout(0, 1, 0, 0));
			pnCostesList.add(getCostesPanel1());
			pnCostesList.add(getCostesPanel_1());
			pnCostesList.add(getCostesPanel_2());
			pnCostesList.add(getCostesPanel_2_1());
		}
		return pnCostesList;
	}
	private JPanel getPnButtons() {
		if (pnButtons == null) {
			pnButtons = new JPanel();
			pnButtons.add(getBtnAadirSesion());
			pnButtons.add(getBtnBorrarSesion());
		}
		return pnButtons;
	}
	private JButton getBtnAadirSesion() {
		if (btnAddCoste == null) {
			btnAddCoste = new JButton("A\u00F1adir Coste");
			btnAddCoste.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					getPnCostesList().add(new CostesPanel());
					getSpCostesList().getViewport().validate();
				}
			});
		}
		return btnAddCoste;
	}
	private JButton getBtnBorrarSesion() {
		if (btnBorrarCoste == null) {
			btnBorrarCoste = new JButton("Borrar Coste");
			btnBorrarCoste.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Component[] components = getPnCostesList().getComponents();
					getPnCostesList().removeAll();
					
					for(int i = 0; i < components.length-1; i++) {
						getPnCostesList().add(components[i]);
					}
					
					getSpCostesList().getViewport().validate();
				}
			});
		}
		return btnBorrarCoste;
	}
	
	public List<CosteInscripcionDto> getCosteInscripcionDtos(int activity_id) {
		List<CosteInscripcionDto> costes = new ArrayList<CosteInscripcionDto>();
		CosteInscripcionDto coste;
		
		for(Component p : getPnCostesList().getComponents()) {
			coste = ((CostesPanel) p).getCosteInscripcionDto();
			coste.id_act = activity_id;
			costes.add(coste);
		}
		
		return costes;
	}
	
	private CostesPanel getCostesPanel1() {
		if (costesPanel1 == null) {
			costesPanel1 = new CostesPanel();
			costesPanel1.getTfNombre().setText("Tarifa General");
		}
		return costesPanel1;
	}
	private CostesPanel getCostesPanel_1() {
		if (costesPanel2 == null) {
			costesPanel2 = new CostesPanel();
			costesPanel2.getTfNombre().setText("Tarifa Colegiado");
		}
		return costesPanel2;
	}
	private CostesPanel getCostesPanel_2() {
		if (costesPanel == null) {
			costesPanel = new CostesPanel();
			costesPanel.getTfNombre().setText("Tarifa Colegiado Desempleado");
		}
		return costesPanel;
	}
	private CostesPanel getCostesPanel_2_1() {
		if (costesPanel3 == null) {
			costesPanel3 = new CostesPanel();
			costesPanel3.getTfNombre().setText("Tarifa Estudiante");
		}
		return costesPanel3;
	}
}
