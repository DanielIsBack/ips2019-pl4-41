package gui.activityplanification.costesPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;

import business.dto.CosteInscripcionDto;

public class CostesPanel extends JPanel {

	private static final long serialVersionUID = 6333977355857578910L;
	private JLabel lblNombre;
	private JPanel panelNombre;
	private JTextField tfNombre;
	private JPanel panelCantidad;
	private JLabel lblCantidad;
	private JSpinner spCantidad;

	/**
	 * Create the panel.
	 */
	public CostesPanel() {
		setMinimumSize(new Dimension(10, 135));
		setMaximumSize(new Dimension(32767, 135));
		setPreferredSize(new Dimension(470, 55));
		setBorder(new LineBorder(new Color(0, 0, 0)));
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		add(getPanelNombre());
		add(getPanelCantidad());

	}

	private JLabel getLblNombre() {
		if (lblNombre == null) {
			lblNombre = new JLabel("Nombre: ");
		}
		return lblNombre;
	}

	private JPanel getPanelNombre() {
		if (panelNombre == null) {
			panelNombre = new JPanel();
			panelNombre.add(getLblNombre());
			panelNombre.add(getTfNombre());
		}
		return panelNombre;
	}

	protected JTextField getTfNombre() {
		if (tfNombre == null) {
			tfNombre = new JTextField();
			tfNombre.setColumns(20);
		}
		return tfNombre;
	}

	public CosteInscripcionDto getCosteInscripcionDto() {
		CosteInscripcionDto coste = new CosteInscripcionDto();
		
		coste.cantidad = (double) getSpCantidad().getValue();
		coste.nombre = getTfNombre().getText();

		return coste;
	}

	private JPanel getPanelCantidad() {
		if (panelCantidad == null) {
			panelCantidad = new JPanel();
			panelCantidad.add(getLblCantidad());
			panelCantidad.add(getSpCantidad());
		}
		return panelCantidad;
	}
	private JLabel getLblCantidad() {
		if (lblCantidad == null) {
			lblCantidad = new JLabel("Cantidad: ");
		}
		return lblCantidad;
	}
	private JSpinner getSpCantidad() {
		if (spCantidad == null) {
			spCantidad = new JSpinner();
			spCantidad.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		}
		return spCantidad;
	}
}
