package gui;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import gui.activityplanification.ActivityPlanificationWindow;

public class MainWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnCrearActividad;
	private JButton btnMostrarActividadesE;
	private JButton btnMostrarBalanceDe;
	private JButton btnRegistrarPagosDeInscripciones;
	private JButton btnRegistrarPagosDe;
	private JButton btnInscribirseEnUna;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setLocationRelativeTo(null);
					frame.setTitle("Gestor de Actividades COIIPA");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 1, 0, 0));
		contentPane.add(getBtnCrearActividad());
		contentPane.add(getBtnMostrarActividadesE());
		contentPane.add(getBtnMostrarBalanceDe());
		contentPane.add(getBtnRegistrarPagosDeInscripciones());
		contentPane.add(getBtnRegistrarPagosDe());
		contentPane.add(getBtnInscribirseEnUna());
	}

	private JButton getBtnCrearActividad() {
		if (btnCrearActividad == null) {
			btnCrearActividad = new JButton("Crear Actividad");
			btnCrearActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					new ActivityPlanificationWindow().setVisible(true);
					;

				}
			});
		}
		return btnCrearActividad;
	}

	private JButton getBtnMostrarActividadesE() {
		if (btnMostrarActividadesE == null) {
			btnMostrarActividadesE = new JButton("Mostrar Actividades e Inscripciones");
			btnMostrarActividadesE.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new ActividadesInscripcionesWindow().setVisible(true);
				}
			});
		}
		return btnMostrarActividadesE;
	}

	private JButton getBtnMostrarBalanceDe() {
		if (btnMostrarBalanceDe == null) {
			btnMostrarBalanceDe = new JButton("Mostrar Balance de Actividades");
			btnMostrarBalanceDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new CheckIncomesAndExpenses().setVisible(true);
				}
			});
		}
		return btnMostrarBalanceDe;
	}

	private JButton getBtnRegistrarPagosDeInscripciones() {
		if (btnRegistrarPagosDeInscripciones == null) {
			btnRegistrarPagosDeInscripciones = new JButton("Registrar Pagos y Devoluciones de Inscripciones");
			btnRegistrarPagosDeInscripciones.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new PaymentsWindow().setVisible(true);
				}
			});
		}
		return btnRegistrarPagosDeInscripciones;
	}

	private JButton getBtnRegistrarPagosDe() {
		if (btnRegistrarPagosDe == null) {
			btnRegistrarPagosDe = new JButton("Registrar Pagos de Profesores");
			btnRegistrarPagosDe.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					RegistrarPagosProfesores r=new RegistrarPagosProfesores();
					r.setLocationRelativeTo(null);
					r.setVisible(true);
				}
			});
		}
		return btnRegistrarPagosDe;
	}

	private JButton getBtnInscribirseEnUna() {
		if (btnInscribirseEnUna == null) {
			btnInscribirseEnUna = new JButton("Inscribirse en una actividad");
			btnInscribirseEnUna.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new InscripcionesWindow().setVisible(true);
				}
			});
		}
		return btnInscribirseEnUna;
	}
}
