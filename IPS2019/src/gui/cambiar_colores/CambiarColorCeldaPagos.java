package gui.cambiar_colores;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CambiarColorCeldaPagos  extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		if (value instanceof String) {
			if (column == 0) {
				if (value.equals("Correcto")) {
					cell.setBackground(Color.GREEN);
				} 
				 else {
					cell.setBackground(Color.YELLOW);
				}

			}
		if (column != 0) {
			cell.setBackground(Color.WHITE);
		}
		}
		return cell;
	

	}

}
