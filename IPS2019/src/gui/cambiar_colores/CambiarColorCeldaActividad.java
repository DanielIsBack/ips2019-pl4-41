package gui.cambiar_colores;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CambiarColorCeldaActividad extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		if (value instanceof String) {
			if (column == 6) {
				if (value.equals("Planificada")) {
					cell.setBackground(Color.GREEN);
					// cell.setForeground(Color.WHITE);
				} else if (value.equals("Periodo de Inscripción")) {
					cell.setBackground(Color.YELLOW);
				} else if (value.equals("Inscripción Cerrada")) {
					cell.setBackground(Color.ORANGE);
				} else if (value.equals("Cerrada")) {
					cell.setBackground(Color.RED);
				} else if (value.equals("Cancelada")) {
					cell.setBackground(Color.WHITE);
				}

			}
		}
		if (column != 6) {
			cell.setBackground(Color.WHITE);
		}
		return cell;
	}

}
