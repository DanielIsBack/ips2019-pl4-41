package gui.cambiar_colores;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class CambiarColorCeldaPagosInscripcion extends DefaultTableCellRenderer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int row, int column) {
		JLabel cell = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

		if (value instanceof String) {
			if (value.equals("Recibida") || value.equals("Pagada de m�s") || value.equals("Pagada de menos")) {
				cell.setBackground(Color.YELLOW);
				// cell.setForeground(Color.WHITE);
			} else if (value.equals("Cobrada")) {
				cell.setBackground(Color.GREEN);
			} else if (value.equals("Cancelada devuelta") ||value.equals("Cancelada sin devolver")) {
				cell.setBackground(Color.RED);
			} else {
				cell.setBackground(Color.WHITE);
			}
		}

		return cell;
	}

}
