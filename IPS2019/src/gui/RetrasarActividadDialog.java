package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.border.LineBorder;

import business.Database;
import gui.tableextensions.NonEditableCellTableModel;

import java.awt.Color;
import javax.swing.SpinnerNumberModel;

public class RetrasarActividadDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1070720379278075302L;
	private final JPanel contentPanel = new JPanel();
	private String actividad;
	
	JSpinner spinner;
	

	/**
	 * Create the dialog.
	 * @param nombreUltimaActividadSeleccionada 
	 */
	public RetrasarActividadDialog(String nombreUltimaActividadSeleccionada) {
		actividad=nombreUltimaActividadSeleccionada;
		setTitle("Retrasar Actividad: "+ actividad);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		{
			JPanel panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			contentPanel.add(panel);
			panel.setLayout(null);
			{
				spinner = new JSpinner();
				spinner.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
				spinner.setBounds(10, 42, 29, 20);
				panel.add(spinner);
			}
			{
				JLabel lblRetrasarTodasLas = new JLabel("Retrasar todas las sesiones:");
				lblRetrasarTodasLas.setBounds(10, 23, 187, 14);
				panel.add(lblRetrasarTodasLas);
			}
			{
				JLabel lblSemanas = new JLabel("semanas.");
				lblSemanas.setBounds(49, 45, 96, 14);
				panel.add(lblSemanas);
			}
		}
		{
			JPanel panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			contentPanel.add(panel);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
						Database.retrasarTodasLasSesionesDeUnaActividad((int) spinner.getValue(),actividad);
						
						JOptionPane.showMessageDialog(null, "Todas las sesiones de la actividad han sido retrasadas "+ spinner.getValue()+ " semanas.");
						
						Database.mandarEmailsActividadRetrasada((int) spinner.getValue(),actividad);
						
						ActividadesInscripcionesWindow.actualizarActividades();
						
						dispose();
						}
						catch(Exception e1) {
							
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				
				
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
