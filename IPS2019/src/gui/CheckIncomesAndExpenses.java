package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import business.dto.ActivityDto;
import factory.ServiceFactory;
import filter.validables.specific.CheckState;
import gui.tableextensions.ForcedListSelectionModel;
import gui.tableextensions.NonEditableCellTableModel;

public class CheckIncomesAndExpenses extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JPanel panelHeader;
	private JLabel lblInfoActividades;
	private JPanel panelFilters;
	private JSplitPane splitPaneLists;
	private JScrollPane scrollPaneClosedActivities;
	private JScrollPane scrollPaneNotClosedActivities;
	private JPanel panelStartDateFilter;
	private JLabel lblStartingDate;
	private JPanel panelEndDateFilter;
	private JDateChooser dcStartDate;
	private JLabel lblEndDate;
	private JDateChooser dcEndDate;
	private JPanel panelDateFilter;
	private JPanel panelStateFilter;
	private JLabel lblState;
	private JComboBox<String> cbState;
	private JTable tableClosedActivities;
	private JTable tableNotClosedActivities;

	// list of activities
	private List<ActivityDto> activities;

	// closed state
	private final static String CLOSED = "Cerrada";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CheckIncomesAndExpenses dialog = new CheckIncomesAndExpenses();
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public CheckIncomesAndExpenses() {
		activities = ServiceFactory.getActivityService().listActivitiesExpenseIncome();
		setMinimumSize(new Dimension(650, 550));
		this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
		// setBounds(100, 100, 800, 635);
		getContentPane().add(getPanelHeader(), BorderLayout.NORTH);
		getContentPane().add(getPanelFilters(), BorderLayout.WEST);
		getContentPane().add(getSplitPaneLists(), BorderLayout.CENTER);
	}

	private JPanel getPanelHeader() {
		if (panelHeader == null) {
			panelHeader = new JPanel();
			panelHeader.add(getLblInfoActividades());
		}
		return panelHeader;
	}

	private JLabel getLblInfoActividades() {
		if (lblInfoActividades == null) {
			lblInfoActividades = new JLabel("Ingresos/Gastos - Actividades");
			lblInfoActividades.setFont(new Font("Tahoma", Font.BOLD, 26));
		}
		return lblInfoActividades;
	}

	private JPanel getPanelFilters() {
		if (panelFilters == null) {
			panelFilters = new JPanel();
			panelFilters.setLayout(new GridLayout(0, 1, 0, 0));
			panelFilters.add(getPanelDateFilter());
			panelFilters.add(getPanelStateFilter());
		}
		return panelFilters;
	}

	private JSplitPane getSplitPaneLists() {
		if (splitPaneLists == null) {
			splitPaneLists = new JSplitPane();
			splitPaneLists.setOrientation(JSplitPane.VERTICAL_SPLIT);
			splitPaneLists.setLeftComponent(getScrollPaneClosedActivities());
			splitPaneLists.setRightComponent(getScrollPaneNotClosedActivities());
			splitPaneLists.setDividerLocation(225);
		}
		// fill table with closed activities
		filterAndFillActivities();
		return splitPaneLists;
	}

	private JScrollPane getScrollPaneClosedActivities() {
		if (scrollPaneClosedActivities == null) {
			scrollPaneClosedActivities = new JScrollPane();
			scrollPaneClosedActivities.setViewportView(getTableClosedActivities());
		}
		return scrollPaneClosedActivities;
	}

	private JScrollPane getScrollPaneNotClosedActivities() {
		if (scrollPaneNotClosedActivities == null) {
			scrollPaneNotClosedActivities = new JScrollPane();
			scrollPaneNotClosedActivities.setViewportView(getTableNotClosedActivities());
		}
		return scrollPaneNotClosedActivities;
	}

	private JPanel getPanelStartDateFilter() {
		if (panelStartDateFilter == null) {
			panelStartDateFilter = new JPanel();
			panelStartDateFilter.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			panelStartDateFilter.add(getLblStartingDate());
			panelStartDateFilter.add(getDcStartDate());
		}
		return panelStartDateFilter;
	}

	private JLabel getLblStartingDate() {
		if (lblStartingDate == null) {
			lblStartingDate = new JLabel("Fecha inicial");
		}
		return lblStartingDate;
	}

	private JPanel getPanelEndDateFilter() {
		if (panelEndDateFilter == null) {
			panelEndDateFilter = new JPanel();
			panelEndDateFilter.add(getLblEndDate());
			panelEndDateFilter.add(getDcEndDate());
		}
		return panelEndDateFilter;
	}

	private JDateChooser getDcStartDate() {
		if (dcStartDate == null) {
			dcStartDate = new JDateChooser();
			dcStartDate.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent evt) {
					repaintTables();
				}
			});
		}
		return dcStartDate;
	}

	private JLabel getLblEndDate() {
		if (lblEndDate == null) {
			lblEndDate = new JLabel("Fecha final");
		}
		return lblEndDate;
	}

	private JDateChooser getDcEndDate() {
		if (dcEndDate == null) {
			dcEndDate = new JDateChooser();
			dcEndDate.addPropertyChangeListener(new PropertyChangeListener() {
				public void propertyChange(PropertyChangeEvent evt) {
					repaintTables();
				}
			});
		}
		return dcEndDate;
	}

	private JPanel getPanelDateFilter() {
		if (panelDateFilter == null) {
			panelDateFilter = new JPanel();
			panelDateFilter.setLayout(new GridLayout(0, 1, 0, 0));
			panelDateFilter.add(getPanelStartDateFilter());
			panelDateFilter.add(getPanelEndDateFilter());
		}
		return panelDateFilter;
	}

	private JPanel getPanelStateFilter() {
		if (panelStateFilter == null) {
			panelStateFilter = new JPanel();
			panelStateFilter.add(getLblState());
			panelStateFilter.add(getCbState());
		}
		return panelStateFilter;
	}

	private JLabel getLblState() {
		if (lblState == null) {
			lblState = new JLabel("Estado");
		}
		return lblState;
	}

	private JComboBox<String> getCbState() {
		if (cbState == null) {
			cbState = new JComboBox<String>();
			cbState.setModel(new DefaultComboBoxModel<String>(new String[] { "Cualquiera", "Cerrada", "Planificada",
					"Periodo de Inscripci\u00F3n", "Inscripci\u00F3n Cerrada", "Cancelada" }));
			cbState.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					repaintTables();
				}
			});
			cbState.setPreferredSize(new Dimension(130, 22));
		}
		return cbState;
	}

	private JTable getTableClosedActivities() {
		if (tableClosedActivities == null) {
			tableClosedActivities = new JTable();
			
		}
		return tableClosedActivities;
	}

	private JTable getTableNotClosedActivities() {
		if (tableNotClosedActivities == null) {
			tableNotClosedActivities = new JTable();
		}
		return tableNotClosedActivities;
	}

	private void filterAndFillActivities() {
		// get filters data
		Date sd = getDcStartDate().getDate();
		Date ed = getDcEndDate().getDate();
		String state = (String) getCbState().getSelectedItem();

		// fill tables with filtered data
		fillTables(sd, ed, state);
	}

	private void fillTables(Date sd, Date ed, String state) {
		String[] columnNamesClosed = { "Nombre", "Estado", "Ingresos", "Gastos", "Balance" };
		String[] columnNamesNotClosed = { "Nombre", "Estado", "Ingresos", "Gastos", "Balance",
				"Ingresos Estimados", "Gastos Estimados", "Balance Estimado" };
		NonEditableCellTableModel modelClosedActivities = new NonEditableCellTableModel(columnNamesClosed, 0);
		NonEditableCellTableModel modelNotClosedActivities = new NonEditableCellTableModel(columnNamesNotClosed, 0);
		
		// filter activities between the two dates
		List<ActivityDto> dfactivities = ServiceFactory.getActivityService().findActivitiesB2D(activities, sd, ed);
		
		for (ActivityDto a : dfactivities) {
			if (new CheckState(a.state, state).isValid()) {
				if (new CheckState(a.state, CLOSED).isValid()) {
					Object[] activity = { a.name, a.state, a.income, a.expense, a.balance };
					modelClosedActivities.addRow(activity);
				} else {
					Object[] activity = { a.name, a.state, a.income, a.expense, a.balance,
							a.estimatedIncome, a.estimatedExpense, a.estimatedBalance };
					modelNotClosedActivities.addRow(activity);
				}
			}
		}
		tableClosedActivities.setModel(modelClosedActivities);
		tableNotClosedActivities.setModel(modelNotClosedActivities);
		
		tableClosedActivities.setSelectionModel(new ForcedListSelectionModel());
		tableNotClosedActivities.setSelectionModel(new ForcedListSelectionModel());
	}

	private void repaintTables() {
		// get table models
		DefaultTableModel dmca = (DefaultTableModel) tableClosedActivities.getModel();
		DefaultTableModel dmnca = (DefaultTableModel) tableNotClosedActivities.getModel();

		// delete content
		dmca.setRowCount(0);
		dmnca.setRowCount(0);

		// fill them again
		filterAndFillActivities();
	}

}
