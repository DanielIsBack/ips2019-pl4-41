package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;

import business.Database;
import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.PagoInscripcionDto;
import business.dto.ParticipanteDto;
import factory.ServiceFactory;
import gui.cambiar_colores.CambiarColorCeldaPagosInscripcion;
import gui.tableextensions.NonEditableCellTableModel;
import javax.swing.ListSelectionModel;

public class PaymentsWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3609785470191024335L;
	private static ArrayList<ActivityDto> actividades = new ArrayList<ActivityDto>();
	private static List<InscripcionDto> inscripciones = new ArrayList<InscripcionDto>();
	private static InscripcionDto inscripcionSeleccionada;

	private JPanel contentPane;
	private JButton btnRegistrarPago;
	private JComboBox<String> comboBoxActividad;
	private JPanel panelComboActividades;
	private JPanel panelInscripciones;
	private JPanel panelFecha;
	private JPanel panelCantidad;
	private JTextField textField;
	private JOptionPane jopError;
	private JPanel panelFechaYCantidad;
	private JPanel panelBoton;
	private JScrollPane scrollPane;
	private JTable tableInscripciones;
	private JPanel panel;
	private JPanel panelCantidadyTipoPago;
	private JPanel panelTipoPago;
	private JRadioButton rdbtnPago;
	private JRadioButton rdbtnDevolucion;
	private final ButtonGroup buttonGroupPagoDevolucion = new ButtonGroup();
	private JDateChooser dateChooser;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PaymentsWindow frame = new PaymentsWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PaymentsWindow() {
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1030, 546);
		contentPane = new JPanel();
		setTitle("Registrar Pagos y Devoluciones de Inscripciones");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.add(getPanelComboActividades());
		contentPane.add(getPanelInscripciones());
		contentPane.add(getPanel());
	}

	private JButton getBtnRegistrarPago() {
		if (btnRegistrarPago == null) {
			btnRegistrarPago = new JButton("Registrar Pago");
			btnRegistrarPago.setAlignmentX(Component.CENTER_ALIGNMENT);
			btnRegistrarPago.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					registrarPago();
					tableAddInscripciones();
				}

				private void registrarPago() {					
					ActivityDto actividad = actividades.get(getComboBoxActividad().getSelectedIndex());

					java.util.Date fechaJava = (java.util.Date) getDateChooser().getDate();
					Date fecha = new Date(fechaJava.getTime());

					try {
						double cantidad = comprobarCantidad();

						String tipo = getSelectedRadioButton();

						PagoInscripcionDto p = crearPagoDto(fecha, cantidad, tipo, inscripcionSeleccionada.id, actividad.id);
						
						boolean fechaCorrecta = Database.comprobarFecha(inscripcionSeleccionada, fecha, tipo);
						
						String estado = ServiceFactory.getPaymentService().registerPayment(p);

						enviarEmail(actividad, p, estado);
					
						if(estado.toLowerCase().equals("correcto") && fechaCorrecta) {
							JOptionPane.showMessageDialog(null, "El pago ha sido registrado correctamente", "",
									JOptionPane.INFORMATION_MESSAGE);
						}else if(!estado.toLowerCase().equals("correcto") && !fechaCorrecta){
							JOptionPane.showMessageDialog(null, "La cantidad pagada no es correcta y el pago se ha hecho fuera de plazo.\nEl pago ha sido registrado", "",
									JOptionPane.WARNING_MESSAGE);
						}else if(estado.toLowerCase().equals("correcto") && !fechaCorrecta){
							JOptionPane.showMessageDialog(null, "El pago ha sido registrado fuera de plazo", "",
									JOptionPane.WARNING_MESSAGE);
						}else {
							JOptionPane.showMessageDialog(null, "La cantidad pagada no es correta.\nEl pago se ha registrado", "",
									JOptionPane.WARNING_MESSAGE);
						}
					} catch (NumberFormatException e) {
						jopError = new JOptionPane("El valor " + getTextField().getText() + " no es un valor v�lido.",
								JOptionPane.ERROR_MESSAGE);
						JDialog dialog = jopError.createDialog("error");
						dialog.setVisible(true);
					} catch (IllegalArgumentException e) {
						if (e.getMessage()
								.equals("No se puede realizar un pago en una fecha anterior a la inscripcion")) {
							jopError = new JOptionPane(e.getMessage(), JOptionPane.ERROR_MESSAGE);
							JDialog dialog = jopError.createDialog("Error");
							dialog.setVisible(true);
						}
					}
				}

				private String getSelectedRadioButton() {
					if (getRdbtnDevolucion().isSelected()) {
						return "Devoluci�n";
					} else {
						return "Pago";
					}
				}

				private void enviarEmail(ActivityDto actividad, PagoInscripcionDto p, String estado) {
					if (estado == "Correcto")
						ServiceFactory.getPaymentService().enviarEmail(
								ServiceFactory.getParticipanteCrudService().getParticipanteByIdInscripcion(p.id_insc),
								"Tu pago de " + p.cantidad + "� para la actividad " + actividad.name
										+ " ha sido registrado correctamente");
					else if (estado == "Incorrecto_Menos")
						ServiceFactory.getPaymentService().enviarEmail(
								ServiceFactory.getParticipanteCrudService().getParticipanteByIdInscripcion(p.id_insc),
								"Tu pago de " + p.cantidad + "� para la actividad " + actividad.name
										+ " es incorrecto, debes ingresar mas dinero");
					else if (estado == "Incorrecto_Menos")
						ServiceFactory.getPaymentService().enviarEmail(
								ServiceFactory.getParticipanteCrudService().getParticipanteByIdInscripcion(p.id_insc),
								"Tu pago  de " + p.cantidad + "�para la actividad " + actividad.name
										+ " es incorrecto, se te proceder� a devolverte el sobrante");
				}

				private PagoInscripcionDto crearPagoDto(Date fecha, double cantidad, String tipo, int id_insc, int id_act) {
					PagoInscripcionDto p = new PagoInscripcionDto();
					p.fecha = fecha;
					p.cantidad = cantidad;
					p.tipo = tipo;
					p.id_insc = id_insc;

					return p;
				}

				private double comprobarCantidad() {
					double cantidad = Double.parseDouble(getTextField().getText());

					if (cantidad <= 0)
						throw new NumberFormatException("error");
					return cantidad;
				}
			});
		}
		return btnRegistrarPago;
	}

	private JComboBox<String> getComboBoxActividad() {
		if (comboBoxActividad == null) {
			comboBoxActividad = new JComboBox<String>();

			comboAddActividad();

			comboBoxActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					tableAddInscripciones();
				}
			});
		}
		return comboBoxActividad;
	}

	private void comboAddActividad() {
		actividades = (ArrayList<ActivityDto>) ServiceFactory.getActivityService().listActivitiesWithInscriptions();

		for (ActivityDto a : actividades) {
			comboBoxActividad.addItem("Actividad: " + a.name);
		}
	}

	private JPanel getPanelComboActividades() {
		if (panelComboActividades == null) {
			panelComboActividades = new JPanel();
			panelComboActividades.setBorder(new TitledBorder(null, "Selecciona la actividad:", TitledBorder.CENTER,
					TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelComboActividades.setLayout(new GridLayout(0, 1, 0, 0));
			panelComboActividades.add(getComboBoxActividad());
		}
		return panelComboActividades;
	}

	private JPanel getPanelInscripciones() {
		if (panelInscripciones == null) {
			panelInscripciones = new JPanel();
			panelInscripciones.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Selecciona la inscripcion a la que abonar un pago:", TitledBorder.CENTER, TitledBorder.TOP, null,
					new Color(0, 0, 0)));
			panelInscripciones.setLayout(new BorderLayout(0, 0));
			panelInscripciones.add(getScrollPane(), BorderLayout.CENTER);
		}
		return panelInscripciones;
	}

	private JPanel getPanelFecha() {
		if (panelFecha == null) {
			panelFecha = new JPanel();
			panelFecha.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Selecciona la fecha en la que se abon� el pago:", TitledBorder.CENTER, TitledBorder.TOP, null,
					new Color(0, 0, 0)));
			panelFecha.setLayout(new BorderLayout(0, 0));
			panelFecha.add(getDateChooser(), BorderLayout.NORTH);
		}
		return panelFecha;
	}

	private JPanel getPanelCantidad() {
		if (panelCantidad == null) {
			panelCantidad = new JPanel();
			panelCantidad.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
					"Introduce la cantidad:", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelCantidad.setLayout(new BorderLayout(0, 0));
			panelCantidad.add(getTextField());
		}
		return panelCantidad;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setColumns(10);
		}
		return textField;
	}

	private JPanel getPanelFechaYCantidad() {
		if (panelFechaYCantidad == null) {
			panelFechaYCantidad = new JPanel();
			panelFechaYCantidad.setLayout(new GridLayout(0, 2, 0, 0));
			panelFechaYCantidad.add(getPanelFecha());
			panelFechaYCantidad.add(getPanelCantidadyTipoPago());
		}
		return panelFechaYCantidad;
	}

	private JPanel getPanelBoton() {
		if (panelBoton == null) {
			panelBoton = new JPanel();
			panelBoton.setLayout(new GridLayout(0, 1, 0, 0));
			panelBoton.add(getBtnRegistrarPago());
		}
		return panelBoton;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableInscripciones());
		}
		return scrollPane;
	}

	private JTable getTableInscripciones() {
		if (tableInscripciones == null) {
			tableInscripciones = new JTable();
			tableInscripciones.setSelectionBackground(Color.CYAN);
			tableInscripciones.setSelectionForeground(new Color(0, 0, 255));
			tableInscripciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

			tableAddInscripciones();

			tableInscripciones.setDefaultRenderer(Object.class, new CambiarColorCeldaPagosInscripcion());

			tableInscripciones.addMouseListener(new MouseAdapter() {

				public void mouseClicked(MouseEvent e) {
					Point point = e.getPoint();
					int row = tableInscripciones.rowAtPoint(point);
					String email = (String) tableInscripciones.getValueAt(row, 2);
					int idParticipante = ServiceFactory.getParticipanteCrudService().getIDParticipanteByEmail(email);
					inscripcionSeleccionada = buscarInscripcionPorIdParticipante(idParticipante);
				}

			});

		}
		return tableInscripciones;
	}

	private InscripcionDto buscarInscripcionPorIdParticipante(int idParticipante) {
		for (InscripcionDto i : inscripciones) {
			List<ParticipanteDto> clientes = ServiceFactory.getParticipanteCrudService()
					.getParticipanteByIdInscripcion(i.id);

			for (ParticipanteDto c : clientes) {
				if (c.id == idParticipante) {
					return i;
				}
			}
		}

		return null;
	}

	private void tableAddInscripciones() {
		String[] columnNames = {"Nombre", "Apellidos", "Email", "Colectivo", "Fecha Inscripci�n", "Estado", "Cantidad A Pagar", "Cantidad Abonada"};
		
		if(getComboBoxActividad().getSelectedIndex()>=0) {
		
			NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(
				parseInscripciones((actividades.get(getComboBoxActividad().getSelectedIndex()).id)), columnNames);
			tableInscripciones.setModel(defTableModel);
		}
	}

	private String[][] parseInscripciones(int activityId) {
		inscripciones = ServiceFactory.getInscriptionService().findInscriptionsByActivity(activityId);
		String[][] activityData = new String[inscripciones.size()][8];

		for (int i = 0; i < inscripciones.size(); i++) {

			ParticipanteDto cliente = ServiceFactory.getParticipanteCrudService()
					.getParticipanteById(inscripciones.get(i).id_part);

			activityData[i][0] = cliente.nombre;
			activityData[i][1] = cliente.apellidos;
			activityData[i][2] = cliente.email;
			
			String colectivo = ServiceFactory.getParticipanteCrudService().getColectivoByIdInscripcion(inscripciones.get(i).id);
			if(colectivo == null) {
				colectivo = "Tarifa Gratuita";
			}else if(colectivo.toLowerCase().equals("tarifa_general")) {
				colectivo = "General";
			}
			
			activityData[i][3] = colectivo;
			
			activityData[i][4] = inscripciones.get(i).f_insc.toString();
			
			String estado = inscripciones.get(i).estado_pago;

			if (estado.equals("Sin_pagar")) {
				activityData[i][5] = "Recibida";
			} else if (estado.equals("Cancelada_Sin_Devolver")) {
				activityData[i][5] = "Cancelada sin devolver";
			}  else if (estado.equals("Cancelada_Devuelta")) {
				activityData[i][5] = "Cancelada devuelta";
			} else if (estado.equals("Incorrecto_Menos")) {
				activityData[i][5] = "Pagada de menos";
			} else if (estado.equals("Incorrecto_Mas")) {
				activityData[i][5] = "Pagada de m�s";
			} else if (estado.equals("Correcto")) {
				activityData[i][5] = "Cobrada";
			}
			
			activityData[i][6] = String.valueOf(ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripciones.get(i).id));
			activityData[i][7] = String.valueOf(ServiceFactory.getInscripcionCrudService()
					.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id)); 
		}

		return activityData;

	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getPanelBoton(), BorderLayout.CENTER);
			panel.add(getPanelFechaYCantidad(), BorderLayout.NORTH);
		}
		return panel;
	}

	private JPanel getPanelCantidadyTipoPago() {
		if (panelCantidadyTipoPago == null) {
			panelCantidadyTipoPago = new JPanel();
			panelCantidadyTipoPago.setLayout(new BoxLayout(panelCantidadyTipoPago, BoxLayout.X_AXIS));
			panelCantidadyTipoPago.add(getPanelCantidad());
			panelCantidadyTipoPago.add(getPanelTipoPago());
		}
		return panelCantidadyTipoPago;
	}

	private JPanel getPanelTipoPago() {
		if (panelTipoPago == null) {
			panelTipoPago = new JPanel();
			panelTipoPago.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "",
					TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
			panelTipoPago.setLayout(new GridLayout(2, 2, 0, 0));
			panelTipoPago.add(getRdbtnPago());
			panelTipoPago.add(getRdbtnDevolucion());
		}
		return panelTipoPago;
	}

	private JRadioButton getRdbtnPago() {
		if (rdbtnPago == null) {
			rdbtnPago = new JRadioButton("Pago");
			buttonGroupPagoDevolucion.add(rdbtnPago);
			rdbtnPago.setSelected(true);
		}
		return rdbtnPago;
	}

	private JRadioButton getRdbtnDevolucion() {
		if (rdbtnDevolucion == null) {
			rdbtnDevolucion = new JRadioButton("Devoluci�n");
			buttonGroupPagoDevolucion.add(rdbtnDevolucion);
		}
		return rdbtnDevolucion;
	}

	private JDateChooser getDateChooser() {
		if (dateChooser == null) {
			dateChooser = new JDateChooser();
			dateChooser.setDateFormatString("dd-MM-yyyy");
			dateChooser.setDate(new java.util.Date());
		}
		return dateChooser;
	}
}
