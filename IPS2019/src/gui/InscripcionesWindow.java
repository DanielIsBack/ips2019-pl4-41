package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import business.dto.ParticipanteDto;
import business.dto.SesionDto;
import business.dto.TeacherDto;
import factory.ServiceFactory;
import gui.tableextensions.ForcedListSelectionModel;
import gui.tableextensions.NonEditableCellTableModel;
import javax.swing.border.TitledBorder;
import javax.swing.JRadioButton;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import javax.swing.border.BevelBorder;

public class InscripcionesWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// GUI elements
	private JPanel contentPane;
	private JPanel pnInputParticipante;
	private JPanel pnBanner;
	private JLabel lblListaDeActividades;
	private JTable tActividades;
	private JLabel lblNombrePart;
	private JTextField txtNombrePart;
	private JLabel lblApellidosPart;
	private JTextField txtApellidosPart;
	private JButton btnEnviarInscripcion;
	private JLabel lblEmailPart;
	private JTextField txtEmailPart;
	private JPanel pnTActividades;
	private JLabel lblNombreDesc;
	private JPanel pnDescActividad;
	private JLabel lblObjetivosDesc;
	private JLabel lblContenidosDesc;
	private JLabel lblInstruidoPorDesc;
	private JLabel lblCosteInscripcionDesc;
	private JLabel lblFechaYLugarDesc;
	private JPanel pnDatosParticipante;
	private JPanel pnEnviarInscripcionBtn;
	private JScrollPane scpnTActividades;
	private JPanel pnTActividadesDesc;
	private JPanel pnVariablesSimplesDesc;
	private JPanel pnVariablesComplejasDesc;
	private JLabel lblNombreDescVar;
	private JTextArea txtaObjetivosDescVar;
	private JTextArea txtaContenidosDescVar;
	private JLabel lblInstruidoPorDescVar;
	private JTextArea txtaFechaYLugarDescVar;
	private JLabel lblConsteInscripcionDescVar;
	private JPanel pnlColectivo;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private JPanel pnLabelCosteInscripcion;
	private JLabel lblColectivoVar;
	private JLabel lblCierreCosteInscripcionesDesc;
	
	// database
	static ArrayList<ActivityDto> actividades;
	static int row;
	String[][] parsedActividades;
	NonEditableCellTableModel tableModel;
	String[] columnNames = { "Nombre", "Plazas Disponibles" };
	List<CosteInscripcionDto> inscostselectedactivity;
	private JScrollPane spnFechaLugarDescVar;
	private JScrollPane spnContenidosDescVar;
	private JScrollPane spnObjetivosDescVar;
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					InscripcionesWindow frame = new InscripcionesWindow();
					frame.setTitle("Lista de actividades disponibles");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Logic
	private String[][] parseActividades() {
		String[][] actividadesData = new String[actividades.size()][2];
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd 'at' HH:mm");

		for (int cl = 0; cl < actividades.size(); cl++) {

			//int plazas = actividades.get(cl).numOfPlaces;
//			int inscripcionesAct = ServiceFactory.getInscripcionCrudService()
//					.getNumeroTotalInscripcionesActividad(actividades.get(cl).id);
			//int inscripcionesAct = actividades.get(cl).numinsc;
			int plazasLibres = actividades.get(cl).freePlaces;

			actividadesData[cl][0] = actividades.get(cl).name;
			//actividadesData[cl][1] = Integer.toString(plazasLibres) + "/" + Integer.toString(plazas);
			actividadesData[cl][1] = Integer.toString(plazasLibres);
			// actividadesData[cl][2]=actividades.get(cl).curseDate.toString();
		}
		return actividadesData;
	}

	private void updateDescData(int index) {
		ActivityDto act = actividades.get(index);
		List<TeacherDto> teachers = ServiceFactory.getTeacherService().findTeachersOfActivity(act.id);
		List<SesionDto> sesiones = ServiceFactory.getActivityService().findSesionesOfActivity(act.id); 
		inscostselectedactivity = ServiceFactory.getActivityService().findCostesInscripcionesOfActividad(act.id); 
		lblNombreDescVar.setText(act.name);
		txtaObjetivosDescVar.setText(act.objectives);
		txtaContenidosDescVar.setText(act.contents);
		lblInstruidoPorDescVar.setText(listOfTeachersToString(teachers));
		txtaFechaYLugarDescVar.setText(listOfSesionesToString(sesiones)); 
		
		//lblConsteInscripcionDescVar.setText(listOfCostesActividadToString(inscost)); 
		
		txtaFechaYLugarDescVar.setCaretPosition(0);
		txtaContenidosDescVar.setCaretPosition(0);
		txtaObjetivosDescVar.setCaretPosition(0);
		
		//TODO
		addRadioButtons();
		updateColectivoPrecio();
		
		
	}
	
	private void updateColectivoPrecio() {
		
		String colectivo = "";
		double precio;
		
		 for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
	            AbstractButton button = buttons.nextElement();
	            if (button.isSelected()) {
	            	colectivo = button.getText();
	                lblColectivoVar.setText(colectivo);
	            }
		 }
		 
		 for(int i = 0; i<inscostselectedactivity.size();i++) {
			 if(inscostselectedactivity.get(i).nombre.equals(colectivo)) {
				 precio = inscostselectedactivity.get(i).cantidad;
				 lblConsteInscripcionDescVar.setText(Double.toString(precio));
			 }
		 }
		 
		 if(colectivo == "") {
			 lblColectivoVar.setText("Tarifa Gratuita");
			 lblConsteInscripcionDescVar.setText(Double.toString(0.0));
		 }
		 
		 
	}

	private String listOfTeachersToString(List<TeacherDto> list) {
		String str = "";
//		for (TeacherDto t : list) {
//			str += t.name + " " + t.surname + "\n";
//		}
		
		for(int i=0;i<list.size();i++) {
			TeacherDto t = list.get(i);
			str+= t.name + " " + t.surname;
			if(i+1!=list.size()) str+=", ";
		}
		
		return str;
	}


	private String listOfSesionesToString(List<SesionDto> list) {
		String str = "";
		int i = 1;
		for (SesionDto s : list) {
			Date d_com = s.f_comienzo;
			Date d_fin = s.f_finalizacion;
			
			SimpleDateFormat format_day = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat format_hour = new SimpleDateFormat("hh:mm aa");
			
			//SimpleDateFormat format_whole = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					
			String formatted_dd_MM_yyyy = format_day.format(d_com);
			String formatted_h_com = format_hour.format(d_com);
			String formatted_h_fin = format_hour.format(d_fin);
			
			//String formatted_h_com = format_whole.format(d_com);
			//String formatted_h_fin = format_whole.format(d_fin);
			str += "Sesi�n ";
			str += i+":\n";
			str += "        D�a: "+formatted_dd_MM_yyyy+"\n";
			str += "        Horas: "+formatted_h_com+ "-" +formatted_h_fin+"\n";
			str += "        Lugar: "+s.lugar+"\n\n";
			
			//str += (formatted_dd_MM_yyyy+ " [" +formatted_h_com+ "-" +formatted_h_fin+ "] --- "+s.lugar+"\n");
			
			//str += ("[" +formatted_h_com+ "-" +formatted_h_fin+ "] --- "+s.lugar+"\n");
			
			i++;
		}
		
		

		return str;
	}

	private String listOfCostesActividadToString(List<CosteInscripcionDto> inscost) {
		String str = "";
		
		for(CosteInscripcionDto coste : inscost) {
			str += "" +coste.nombre + "->"+coste.cantidad+"�\n";
		}
		
		return str;
	}

	private void clickEnviarInscripcion() {
		// create participantedto
		ParticipanteDto p = new ParticipanteDto();
		p.nombre = txtNombrePart.getText();
		p.apellidos = txtApellidosPart.getText();
		p.email = txtEmailPart.getText();

		// check if participante is in db, if not insert it in db
		if (!ServiceFactory.getParticipanteCrudService().comprobarParticipanteExiste(p)) {
			ServiceFactory.getParticipanteCrudService().a�adirParticipante(p);
		}

		// get actividaddto
		int activitySelected = tActividades.getSelectedRow();
		ActivityDto a = actividades.get(activitySelected);

		// check if participante has already made an inscripcion, en caso afirmativo
		// avisar y no hacer nada
		if (ServiceFactory.getInscripcionCrudService().participanteEstaInscritoEnActividad(p, a)) {
			JOptionPane.showMessageDialog(contentPane, "ERROR: No te puedes inscribir dos veces en la misma actividad");
		} else {
			// empty text fields
			txtNombrePart.setText("");
			txtApellidosPart.setText("");
			txtEmailPart.setText("");

			// conseguir el id del participante en la base de datos, porque la base de datos
			// es horrible
			p.id = ServiceFactory.getParticipanteCrudService().getIDParticipanteByEmail(p.email);

			// crear inscripcion
			String colectivo = getSelectedButton();
			ServiceFactory.getInscripcionCrudService().crearInscripcion(p, a, colectivo);

			// crear correo de prueba
			ServiceFactory.getInscripcionCrudService().enviarInscripcionPorCorreoMock(p, a, colectivo);

			// mostrar al usuario que su isncripci�n de ha realizadp
			JOptionPane.showMessageDialog(contentPane,
					"Te has inscrito en " + a.name + " con �xito\nSe ha enviado un email a tu correo con los detalles");

			// actualizar el estado de la actividad si ya no le quedan plazas
			int inscripcionesAct = ServiceFactory.getInscripcionCrudService()
					.getNumeroTotalInscripcionesActividad(a.id);
			
			int plazasLibres = a.numOfPlaces - inscripcionesAct;
			
			if (plazasLibres == 0) {
				a.state = "Inscripci�n Cerrada";
				ServiceFactory.getActivityService().updateActivity(a); // ONLY UPDATES STATUS
			}

			// actualizar la lista de actividades
			actividades = ServiceFactory.getActivityService().ListAvailableActivities();
			
//			for(int i = 0;i<actividades.size();i++) {
//				if(actividades.get(i).id == a.id && plazasLibres == 0) {
//					actividades.remove(i);
//				}
//			}
			
			parsedActividades = parseActividades();
			
			tableModel = new NonEditableCellTableModel(parsedActividades, columnNames);
			tActividades.setModel(tableModel);

		}

	}
	
	private String getSelectedButton() throws IllegalArgumentException{
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton boton = buttons.nextElement();
            if(boton.isSelected()) {
				return boton.getName();
			}
            
            if(!buttons.hasMoreElements()) {
            	throw new IllegalArgumentException("No has seleccionado ningun colectivo");
            }
		 }
		
		return null;
	}

	// GUI
	/**
	 * Create the frame.
	 */
	public InscripcionesWindow() {
		actividades = ServiceFactory.getActivityService().ListAvailableActivities();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// setBounds(100, 100, 950, 500);
		
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnBanner(), BorderLayout.NORTH);
		contentPane.add(getPnDatosParticipante(), BorderLayout.SOUTH);
		contentPane.add(getPnTActividadesDesc(), BorderLayout.CENTER);

		if (parsedActividades.length > 0)
			updateDescData(0);
	}

	private JPanel getPnInputParticipante() {
		if (pnInputParticipante == null) {
			pnInputParticipante = new JPanel();
			pnInputParticipante.setLayout(new GridLayout(2, 0, 0, 0));
			pnInputParticipante.add(getLblNombrePart());
			pnInputParticipante.add(getLblApellidosPart());
			pnInputParticipante.add(getLblEmailPart());
			pnInputParticipante.add(getTxtNombrePart());
			pnInputParticipante.add(getTxtApellidosPart());
			pnInputParticipante.add(getTxtEmailPart());
		}
		return pnInputParticipante;
	}

	private JPanel getPnBanner() {
		if (pnBanner == null) {
			pnBanner = new JPanel();
			pnBanner.add(getLblListaDeActividades());
		}
		return pnBanner;
	}

	private JLabel getLblListaDeActividades() {
		if (lblListaDeActividades == null) {
			lblListaDeActividades = new JLabel("Lista de actividades disponibles:");
			lblListaDeActividades.setFont(new Font("Tahoma", Font.BOLD, 26));
		}
		return lblListaDeActividades;
	}

	private JTable getTActividades() {
		if (tActividades == null) {
			String[] columnNames = { "Nombre", "Plazas Disponibles" };
			parsedActividades = parseActividades();
			tableModel = new NonEditableCellTableModel(parsedActividades, columnNames);

			tActividades = new JTable(tableModel);
			tActividades.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent evt) {
					row = tActividades.rowAtPoint(evt.getPoint());
					// int col = tActividades.columnAtPoint(evt.getPoint());
					if (row >= 0) {
						updateDescData(row);
					}
				}

			});
			tActividades.setSelectionModel(new ForcedListSelectionModel());
			if (parsedActividades.length > 0) { // if not empty
				tActividades.changeSelection(0, 0, false, false); // click first row
			}
			// tActividades.getColumnModel().getColumn(1).setPreferredWidth(10);
			// tActividades.getColumnModel().getColumn(2).setPreferredWidth(10);

		}
		return tActividades;
	}

	private JLabel getLblNombrePart() {
		if (lblNombrePart == null) {
			lblNombrePart = new JLabel("Nombre:");
			lblNombrePart.setHorizontalAlignment(SwingConstants.LEFT);
			lblNombrePart.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblNombrePart;
	}

	private JTextField getTxtNombrePart() {
		if (txtNombrePart == null) {
			txtNombrePart = new JTextField();
			txtNombrePart.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					super.keyReleased(e);
					if (txtNombrePart.getText().length() > 0 && txtApellidosPart.getText().length() > 0
							&& txtEmailPart.getText().length() > 0)
						btnEnviarInscripcion.setEnabled(true);
					else
						btnEnviarInscripcion.setEnabled(false);
				}
			});
			txtNombrePart.setColumns(10);
		}
		return txtNombrePart;
	}

	private JLabel getLblApellidosPart() {
		if (lblApellidosPart == null) {
			lblApellidosPart = new JLabel("Apellidos:");
			lblApellidosPart.setHorizontalAlignment(SwingConstants.LEFT);
			lblApellidosPart.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblApellidosPart;
	}

	private JTextField getTxtApellidosPart() {
		if (txtApellidosPart == null) {
			txtApellidosPart = new JTextField();
			txtApellidosPart.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					super.keyReleased(e);
					if (txtNombrePart.getText().length() > 0 && txtApellidosPart.getText().length() > 0
							&& txtEmailPart.getText().length() > 0)
						btnEnviarInscripcion.setEnabled(true);
					else
						btnEnviarInscripcion.setEnabled(false);
				}
			});
			txtApellidosPart.setColumns(10);
		}
		return txtApellidosPart;
	}

	private JButton getBtnEnviarInscripcion() {
		if (btnEnviarInscripcion == null) {
			btnEnviarInscripcion = new JButton("Enviar Inscripci\u00F3n");
			btnEnviarInscripcion.setEnabled(false);
			btnEnviarInscripcion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					try {
						clickEnviarInscripcion();
					}catch (IllegalArgumentException e) {
						JOptionPane.showMessageDialog(null, e.getMessage(), "",
								JOptionPane.ERROR_MESSAGE);
					}

				}

			});
		}
		return btnEnviarInscripcion;
	}

	private JLabel getLblEmailPart() {
		if (lblEmailPart == null) {
			lblEmailPart = new JLabel("E-mail:");
			lblEmailPart.setHorizontalAlignment(SwingConstants.LEFT);
			lblEmailPart.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblEmailPart;
	}

	private JTextField getTxtEmailPart() {
		if (txtEmailPart == null) {
			txtEmailPart = new JTextField();
			txtEmailPart.addKeyListener(new KeyAdapter() {
				@Override
				public void keyReleased(KeyEvent e) {
					super.keyReleased(e);
					if (txtNombrePart.getText().length() > 0 && txtApellidosPart.getText().length() > 0
							&& txtEmailPart.getText().length() > 0)
						btnEnviarInscripcion.setEnabled(true);
					else
						btnEnviarInscripcion.setEnabled(false);
				}
			});
			txtEmailPart.setColumns(10);
		}
		return txtEmailPart;
	}

	private JPanel getPnTActividades() {
		if (pnTActividades == null) {
			pnTActividades = new JPanel();
			pnTActividades.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnTActividades.setLayout(new BorderLayout(0, 0));
			pnTActividades.add(getScpnTActividades(), BorderLayout.CENTER);

		}
		return pnTActividades;
	}

	private JLabel getLblNombreDesc() {
		if (lblNombreDesc == null) {
			lblNombreDesc = new JLabel("Nombre:");
			lblNombreDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblNombreDesc.setVerticalAlignment(SwingConstants.TOP);
			lblNombreDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblNombreDesc;
	}

	private JPanel getPnDescActividad() {
		if (pnDescActividad == null) {
			pnDescActividad = new JPanel();
			pnDescActividad.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnDescActividad.setLayout(new BorderLayout(0, 0));
			pnDescActividad.add(getPnVariablesSimplesDesc(), BorderLayout.NORTH);
			pnDescActividad.add(getPnVariablesComplejasDesc());
		}
		return pnDescActividad;
	}

	private JLabel getLblObjetivosDesc() {
		if (lblObjetivosDesc == null) {
			lblObjetivosDesc = new JLabel("Objetivos:");
			lblObjetivosDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblObjetivosDesc.setVerticalAlignment(SwingConstants.TOP);
			lblObjetivosDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblObjetivosDesc;
	}

	private JLabel getLblContenidosDesc() {
		if (lblContenidosDesc == null) {
			lblContenidosDesc = new JLabel("Contenidos:");
			lblContenidosDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblContenidosDesc.setVerticalAlignment(SwingConstants.TOP);
			lblContenidosDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblContenidosDesc;
	}

	private JLabel getLblInstruidoPorDesc() {
		if (lblInstruidoPorDesc == null) {
			lblInstruidoPorDesc = new JLabel("Instruido por:");
			lblInstruidoPorDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblInstruidoPorDesc.setVerticalAlignment(SwingConstants.TOP);
			lblInstruidoPorDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblInstruidoPorDesc;
	}

	private JLabel getLblCosteInscripcionDesc() {
		if (lblCosteInscripcionDesc == null) {
			lblCosteInscripcionDesc = new JLabel("Coste inscripci\u00F3n(");
			lblCosteInscripcionDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblCosteInscripcionDesc.setVerticalAlignment(SwingConstants.TOP);
			lblCosteInscripcionDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblCosteInscripcionDesc;
	}

	private JLabel getLblFechaYLugarDesc() {
		if (lblFechaYLugarDesc == null) {
			lblFechaYLugarDesc = new JLabel("Fecha y lugar:");
			lblFechaYLugarDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
			lblFechaYLugarDesc.setVerticalAlignment(SwingConstants.TOP);
			lblFechaYLugarDesc.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return lblFechaYLugarDesc;
	}

	private JPanel getPnDatosParticipante() {
		if (pnDatosParticipante == null) {
			pnDatosParticipante = new JPanel();
			pnDatosParticipante.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
			pnDatosParticipante.setLayout(new BorderLayout(0, 0));
			pnDatosParticipante.add(getPnInputParticipante(), BorderLayout.CENTER);
			pnDatosParticipante.add(getPnlColectivo(), BorderLayout.WEST);
			pnDatosParticipante.add(getPnEnviarInscripcionBtn(), BorderLayout.EAST);
		}
		return pnDatosParticipante;
	}

	private JPanel getPnEnviarInscripcionBtn() {
		if (pnEnviarInscripcionBtn == null) {
			pnEnviarInscripcionBtn = new JPanel();
			pnEnviarInscripcionBtn.setLayout(new GridLayout(0, 1, 0, 0));
			pnEnviarInscripcionBtn.add(getBtnEnviarInscripcion());
		}
		return pnEnviarInscripcionBtn;
	}

	private JScrollPane getScpnTActividades() {
		if (scpnTActividades == null) {
			scpnTActividades = new JScrollPane(getTActividades());
		}
		return scpnTActividades;
	}

	private JPanel getPnTActividadesDesc() {
		if (pnTActividadesDesc == null) {
			pnTActividadesDesc = new JPanel();
			pnTActividadesDesc.setLayout(new GridLayout(1, 0, 0, 0));
			pnTActividadesDesc.add(getPnTActividades());
			pnTActividadesDesc.add(getPnDescActividad());
		}
		return pnTActividadesDesc;
	}

	private JPanel getPnVariablesSimplesDesc() {
		if (pnVariablesSimplesDesc == null) {
			pnVariablesSimplesDesc = new JPanel();
			GridBagLayout gbl_pnVariablesSimplesDesc = new GridBagLayout();
			gbl_pnVariablesSimplesDesc.columnWidths = new int[]{212, 0};
			gbl_pnVariablesSimplesDesc.rowHeights = new int[]{28, 30, 28, 30, 28, 30};
			gbl_pnVariablesSimplesDesc.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_pnVariablesSimplesDesc.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			pnVariablesSimplesDesc.setLayout(gbl_pnVariablesSimplesDesc);
			
			GridBagConstraints gbc_lblNombreDesc = new GridBagConstraints();
			gbc_lblNombreDesc.fill = GridBagConstraints.BOTH;
			gbc_lblNombreDesc.insets = new Insets(0, 0, 5, 0);
			gbc_lblNombreDesc.gridx = 0;
			gbc_lblNombreDesc.gridy = 0;
			gbc_lblNombreDesc.gridwidth = 5;
			pnVariablesSimplesDesc.add(getLblNombreDesc(), gbc_lblNombreDesc);
			
			GridBagConstraints gbc_lblNombreDescVar = new GridBagConstraints();
			gbc_lblNombreDescVar.fill = GridBagConstraints.BOTH;
			gbc_lblNombreDescVar.insets = new Insets(0, 0, 5, 0);
			gbc_lblNombreDescVar.gridx = 0;
			gbc_lblNombreDescVar.gridy = 1;
			gbc_lblNombreDescVar.gridwidth = 5;
			pnVariablesSimplesDesc.add(getLblNombreDescVar(), gbc_lblNombreDescVar);
			
			GridBagConstraints gbc_lblInstruidoPorDesc = new GridBagConstraints();
			gbc_lblInstruidoPorDesc.fill = GridBagConstraints.BOTH;
			gbc_lblInstruidoPorDesc.insets = new Insets(0, 0, 5, 0);
			gbc_lblInstruidoPorDesc.gridx = 0;
			gbc_lblInstruidoPorDesc.gridy = 2;
			gbc_lblInstruidoPorDesc.gridwidth = 5;
			pnVariablesSimplesDesc.add(getLblInstruidoPorDesc(), gbc_lblInstruidoPorDesc);
			
			GridBagConstraints gbc_lblInstruidoPorDescVar = new GridBagConstraints();
			gbc_lblInstruidoPorDescVar.fill = GridBagConstraints.BOTH;
			gbc_lblInstruidoPorDescVar.insets = new Insets(0, 0, 5, 0);
			gbc_lblInstruidoPorDescVar.gridx = 0;
			gbc_lblInstruidoPorDescVar.gridy = 3;
			gbc_lblInstruidoPorDescVar.gridwidth = 5;
			pnVariablesSimplesDesc.add(getLblInstruidoPorDescVar(), gbc_lblInstruidoPorDescVar);
			
			GridBagConstraints gbc_pnLabelCosteInscripcion = new GridBagConstraints();
			gbc_pnLabelCosteInscripcion.insets = new Insets(0, 0, 0, 5);
			gbc_pnLabelCosteInscripcion.fill = GridBagConstraints.BOTH;
			gbc_pnLabelCosteInscripcion.gridx = 0;
			gbc_pnLabelCosteInscripcion.gridy = 4;
			pnVariablesSimplesDesc.add(getPnLabelCosteInscripcion(), gbc_pnLabelCosteInscripcion);
			
			GridBagConstraints gbc_lblConsteInscripcionDescVar = new GridBagConstraints();
			gbc_lblConsteInscripcionDescVar.insets = new Insets(0, 0, 5, 0);
			gbc_lblConsteInscripcionDescVar.fill = GridBagConstraints.BOTH;
			gbc_lblConsteInscripcionDescVar.gridx = 0;
			gbc_lblConsteInscripcionDescVar.gridy = 5;
			gbc_lblConsteInscripcionDescVar.gridwidth = 5;
			pnVariablesSimplesDesc.add(getLblConsteInscripcionDescVar(), gbc_lblConsteInscripcionDescVar);
			
		}
		return pnVariablesSimplesDesc;
	}

	private JPanel getPnVariablesComplejasDesc() {
		if (pnVariablesComplejasDesc == null) {
			pnVariablesComplejasDesc = new JPanel();
			GridBagLayout gbl_pnVariablesComplejasDesc = new GridBagLayout();
			gbl_pnVariablesComplejasDesc.columnWidths = new int[]{208, 0};
			gbl_pnVariablesComplejasDesc.rowHeights = new int[]{28, 90, 28, 90, 28, 90};
			gbl_pnVariablesComplejasDesc.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_pnVariablesComplejasDesc.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
			pnVariablesComplejasDesc.setLayout(gbl_pnVariablesComplejasDesc);
			
			GridBagConstraints gbc_lblObjetivosDesc = new GridBagConstraints();
			gbc_lblObjetivosDesc.insets = new Insets(0, 0, 5, 0);
			gbc_lblObjetivosDesc.fill = GridBagConstraints.BOTH;
			gbc_lblObjetivosDesc.gridx = 0;
			gbc_lblObjetivosDesc.gridy = 0;
			gbc_lblObjetivosDesc.gridwidth = 5;
			gbc_lblObjetivosDesc.gridheight = 1;
			pnVariablesComplejasDesc.add(getLblObjetivosDesc(), gbc_lblObjetivosDesc);
			
			GridBagConstraints gbc_spnObjetivosDescVar = new GridBagConstraints();
			gbc_spnObjetivosDescVar.insets = new Insets(0, 0, 0, 5);
			gbc_spnObjetivosDescVar.fill = GridBagConstraints.BOTH;
			gbc_spnObjetivosDescVar.gridx = 0;
			gbc_spnObjetivosDescVar.gridy = 1;
			gbc_spnObjetivosDescVar.gridwidth = 5;
			pnVariablesComplejasDesc.add(getSpnObjetivosDescVar(), gbc_spnObjetivosDescVar);
			
//			GridBagConstraints gbc_txtaObjetivosDescVar = new GridBagConstraints();
//			gbc_txtaObjetivosDescVar.fill = GridBagConstraints.BOTH;
//			gbc_txtaObjetivosDescVar.insets = new Insets(0, 0, 5, 0);
//			gbc_txtaObjetivosDescVar.gridx = 0;
//			gbc_txtaObjetivosDescVar.gridy = 1;
//			gbc_txtaObjetivosDescVar.gridwidth = 5;
//			pnVariablesComplejasDesc.add(getTxtaObjetivosDescVar(), gbc_txtaObjetivosDescVar);
			
			GridBagConstraints gbc_lblContenidosDesc = new GridBagConstraints();
			gbc_lblContenidosDesc.fill = GridBagConstraints.BOTH;
			gbc_lblContenidosDesc.insets = new Insets(0, 0, 5, 0);
			gbc_lblContenidosDesc.gridx = 0;
			gbc_lblContenidosDesc.gridy = 2;
			gbc_lblContenidosDesc.gridwidth = 5;
			pnVariablesComplejasDesc.add(getLblContenidosDesc(), gbc_lblContenidosDesc);
			
			GridBagConstraints gbc_spnContenidosDescVar = new GridBagConstraints();
			gbc_spnContenidosDescVar.insets = new Insets(0, 0, 5, 0);
			gbc_spnContenidosDescVar.fill = GridBagConstraints.BOTH;
			gbc_spnContenidosDescVar.gridx = 0;
			gbc_spnContenidosDescVar.gridy = 3;
			gbc_spnContenidosDescVar.gridwidth = 5;
			pnVariablesComplejasDesc.add(getSpnContenidosDescVar(), gbc_spnContenidosDescVar);
			
//			GridBagConstraints gbc_txtaContenidosDescVar = new GridBagConstraints();
//			gbc_txtaContenidosDescVar.fill = GridBagConstraints.BOTH;
//			gbc_txtaContenidosDescVar.insets = new Insets(0, 0, 5, 0);
//			gbc_txtaContenidosDescVar.gridx = 0;
//			gbc_txtaContenidosDescVar.gridy = 3;
//			gbc_txtaContenidosDescVar.gridwidth = 5;
//			pnVariablesComplejasDesc.add(getTxtaContenidosDescVar(), gbc_txtaContenidosDescVar);
			
			GridBagConstraints gbc_lblFechaYLugarDesc = new GridBagConstraints();
			gbc_lblFechaYLugarDesc.fill = GridBagConstraints.BOTH;
			gbc_lblFechaYLugarDesc.insets = new Insets(0, 0, 5, 0);
			gbc_lblFechaYLugarDesc.gridx = 0;
			gbc_lblFechaYLugarDesc.gridy = 4;
			gbc_lblFechaYLugarDesc.gridwidth = 5;
			pnVariablesComplejasDesc.add(getLblFechaYLugarDesc(), gbc_lblFechaYLugarDesc);
			
//			GridBagConstraints gbc_txtaFechaYLugarDescVar = new GridBagConstraints();
//			gbc_txtaFechaYLugarDescVar.fill = GridBagConstraints.BOTH;
//			gbc_txtaFechaYLugarDescVar.insets = new Insets(0, 0, 5, 0);
//			gbc_txtaFechaYLugarDescVar.gridx = 0;
//			gbc_txtaFechaYLugarDescVar.gridy = 5;
//			gbc_txtaFechaYLugarDescVar.gridwidth = 5;
//			pnVariablesComplejasDesc.add(getTxtaFechaYLugarDescVar(), gbc_txtaFechaYLugarDescVar);
			
			GridBagConstraints gbc_spnFechaLugarDescVar = new GridBagConstraints();
			gbc_spnFechaLugarDescVar.insets = new Insets(0, 0, 5, 0);
			gbc_spnFechaLugarDescVar.fill = GridBagConstraints.BOTH;
			gbc_spnFechaLugarDescVar.gridx = 0;
			gbc_spnFechaLugarDescVar.gridy = 5;
			gbc_spnFechaLugarDescVar.gridwidth = 5;
			pnVariablesComplejasDesc.add(getSpnFechaLugarDescVar(), gbc_spnFechaLugarDescVar);
			
			
			

			

			

			

			

		}
		return pnVariablesComplejasDesc;
	}

	private JLabel getLblNombreDescVar() {
		if (lblNombreDescVar == null) {
			lblNombreDescVar = new JLabel();
			lblNombreDescVar.setHorizontalAlignment(SwingConstants.CENTER);
			lblNombreDescVar.setOpaque(true);
			lblNombreDescVar.setBackground(Color.WHITE);
			lblNombreDescVar.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblNombreDescVar.setAlignmentY(CENTER_ALIGNMENT);
		}
		return lblNombreDescVar;
	}

	private JTextArea getTxtaObjetivosDescVar() {
		if (txtaObjetivosDescVar == null) {
			txtaObjetivosDescVar = new JTextArea(5,50);
			txtaObjetivosDescVar.setEditable(false);
		}
		return txtaObjetivosDescVar;
	}

	private JTextArea getTxtaContenidosDescVar() {
		if (txtaContenidosDescVar == null) {
			txtaContenidosDescVar = new JTextArea(5,50);
			txtaContenidosDescVar.setEditable(false);
		}
		return txtaContenidosDescVar;
	}

	private JLabel getLblInstruidoPorDescVar() {
		if (lblInstruidoPorDescVar == null) {
			lblInstruidoPorDescVar = new JLabel();
			lblInstruidoPorDescVar.setHorizontalAlignment(SwingConstants.CENTER);
			lblInstruidoPorDescVar.setOpaque(true);
			lblInstruidoPorDescVar.setBackground(Color.WHITE);
			lblInstruidoPorDescVar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblInstruidoPorDescVar;
	}

	private JTextArea getTxtaFechaYLugarDescVar() {
		if (txtaFechaYLugarDescVar == null) {
			txtaFechaYLugarDescVar = new JTextArea(5,50);
			txtaFechaYLugarDescVar.setEditable(false);
		}
		return txtaFechaYLugarDescVar;
	}

	private JLabel getLblConsteInscripcionDescVar() {
		if (lblConsteInscripcionDescVar == null) {
			lblConsteInscripcionDescVar = new JLabel();
			lblConsteInscripcionDescVar.setHorizontalAlignment(SwingConstants.CENTER);
			lblConsteInscripcionDescVar.setOpaque(true);
			lblConsteInscripcionDescVar.setBackground(Color.WHITE);
			lblInstruidoPorDescVar.setHorizontalAlignment(SwingConstants.CENTER);
			lblInstruidoPorDescVar.setOpaque(true);
			lblInstruidoPorDescVar.setBackground(Color.WHITE);
			lblConsteInscripcionDescVar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		}
		return lblConsteInscripcionDescVar;
	}
	private JPanel getPnlColectivo() {
		if (pnlColectivo == null) {
			pnlColectivo = new JPanel();
			pnlColectivo.setBorder(new TitledBorder(null, "Colectivo:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
			pnlColectivo.setLayout(new BoxLayout(pnlColectivo, BoxLayout.Y_AXIS));
		}
		return pnlColectivo;
	}

	private void addRadioButtons() {
		ActivityDto a = actividades.get(row);
		List<CosteInscripcionDto> costes = ServiceFactory.getCosteInscripcionService().getCosteInscripcionByIdActividad(a.id);
		getPnlColectivo().removeAll();
		buttonGroup = new ButtonGroup();
		
		for(CosteInscripcionDto coste : costes) {
			JRadioButton boton = new JRadioButton(coste.nombre);
			boton.setName(coste.nombre);
			boton.setSelected(true);
			boton.addActionListener(new ActionListener() {
		        @Override
		        public void actionPerformed(ActionEvent e) {
		        	double precio;
		        	
		        	lblColectivoVar.setText(coste.nombre);
		        	
		        	 for(int i = 0; i<inscostselectedactivity.size();i++) {
		    			 if(inscostselectedactivity.get(i).nombre.equals(coste.nombre)) {
		    				 precio = inscostselectedactivity.get(i).cantidad;
		    				 lblConsteInscripcionDescVar.setText(Double.toString(precio));
		    			 }
		    		 }
		        	 
		        	 
		        	 
		        }
		    });
			
			buttonGroup.add(boton);
			pnlColectivo.add(boton);
		}
		
		
	}
	private JPanel getPnLabelCosteInscripcion() {
		if (pnLabelCosteInscripcion == null) {
			pnLabelCosteInscripcion = new JPanel();
			pnLabelCosteInscripcion.add(getLblCosteInscripcionDesc());
			pnLabelCosteInscripcion.add(getLblColectivoVar());
			pnLabelCosteInscripcion.add(getLblCierreCosteInscripcionesDesc());
		}
		return pnLabelCosteInscripcion;
	}
	private JLabel getLblColectivoVar() {
		if (lblColectivoVar == null) {
			lblColectivoVar = new JLabel("");
			lblColectivoVar.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblColectivoVar;
	}
	private JLabel getLblCierreCosteInscripcionesDesc() {
		if (lblCierreCosteInscripcionesDesc == null) {
			lblCierreCosteInscripcionesDesc = new JLabel("):");
			lblCierreCosteInscripcionesDesc.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblCierreCosteInscripcionesDesc;
	}
	private JScrollPane getSpnFechaLugarDescVar() {
		if (spnFechaLugarDescVar == null) {
			spnFechaLugarDescVar = new JScrollPane(getTxtaFechaYLugarDescVar());
		}
		return spnFechaLugarDescVar;
	}
	private JScrollPane getSpnContenidosDescVar() {
		if (spnContenidosDescVar == null) {
			spnContenidosDescVar = new JScrollPane(getTxtaContenidosDescVar());
		}
		return spnContenidosDescVar;
	}
	private JScrollPane getSpnObjetivosDescVar() {
		if (spnObjetivosDescVar == null) {
			spnObjetivosDescVar = new JScrollPane(getTxtaObjetivosDescVar());
		}
		return spnObjetivosDescVar;
	}
}
