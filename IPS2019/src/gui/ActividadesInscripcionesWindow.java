package gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

import business.Client;
import business.Database;
import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.SesionDto;
import factory.ServiceFactory;
import gui.cambiar_colores.CambiarColorCeldaActividad;
import gui.cambiar_colores.CambiarColorCeldaInscripcion;
import gui.tableextensions.NonEditableCellTableModel;

import java.awt.BorderLayout;
import java.awt.Color;

public class ActividadesInscripcionesWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// database
	static ArrayList<ActivityDto> actividades;
	LinkedList<InscripcionDto> inscripciones = null;
	static InscripcionDto inscripcionSeleccionada = null;
	static String nombreUltimaActividadSeleccionada = null;

	// GUI elements
	private JPanel contentPane;
	private JPanel pnBanner;
	private JLabel lblListaDeActividades;
	private JScrollPane scrollPane;
	static JTable tableActividades;
	private JLabel lblSeleccioneElNombre;
	private JScrollPane scrollPane2;
	private JTable tableInscripciones;
	private JButton btnCancelarInscripcion;
	private JOptionPane jopError;
	private JButton btnCerrarActividad;
	static ActividadesInscripcionesWindow frame;

	static // lógica
	String[] columnNamesActividades = { "Nombre", "Plazas", "Plazas libres","Inicio de inscripción", "Final de inscripción",
			"Fecha de inicio","Estado", "Gastos estimados", "Gastos confirmados", "Ingresos estimados", "Ingresos confirmados" };
	
	String[] columnNamesInscripciones = { "Email", "Nombre", "Apellidos", "Estado", "Colectivo","Cantidad Abonada","Cantidad Pendiente" };
	
	static String[][] parsedActividades;
	static NonEditableCellTableModel defTableModel;
	private JButton btnCancelarActividad;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JLabel lblListaDeInscripciones;
	private JPanel panel_3;
	private JPanel panel_4;
	private JButton btnRetrasarActividad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new ActividadesInscripcionesWindow();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// GUI
	/**
	 * Create the frame.
	 */
	public ActividadesInscripcionesWindow() {
		setTitle("Listado Actividades e Inscripciones");
		actividades = (ArrayList<ActivityDto>) ServiceFactory.getActivityService().listActivities();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1286, 832);
		
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		contentPane.add(getPnBanner());
		contentPane.add(getScrollPane());
		contentPane.add(getPanel());
		contentPane.add(getPanel_2());
		contentPane.add(getPanel_4());
		contentPane.add(getPanel_3());
		contentPane.add(getScrollPane2());
		contentPane.add(getPanel_1());
		
	}

	private JPanel getPnBanner() {
		if (pnBanner == null) {
			pnBanner = new JPanel();
			pnBanner.add(getLblListaDeActividades());
		}
		return pnBanner;
	}

	private JLabel getLblListaDeActividades() {
		if (lblListaDeActividades == null) {
			lblListaDeActividades = new JLabel("Lista de Actividades:");
			lblListaDeActividades.setFont(new Font("Tahoma", Font.BOLD, 18));
		}
		return lblListaDeActividades;
	}

	
	private static String[][] parseActividades() {
		String[][] activityData = new String[actividades.size()][11];
		// DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd 'at' HH:mm");

		for (int i = 0; i < actividades.size(); i++) {
			List<InscripcionDto> inscriptions = ServiceFactory.getInscripcionCrudService().findInscriptionsByActivity(actividades.get(i).id);
			
			Double ingresosTotalesEstimados = 0.0;
			
			for(InscripcionDto in:inscriptions) {
				if(!in.estado_pago.equals("Cancelada_Devuelta") && !in.estado_pago.equals("Cancelada_Sin_Devolver"))
					ingresosTotalesEstimados+=ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(in.id);
			}

			Double ingresosTotalesConfirmados = Database
					.getIngresosConfirmadosDeUnaActividad(actividades.get(i).name);

			Double gastosTotalesEstimados = Database.getRemuneracionTotalByIdActividad(actividades.get(i).id);
		
			Double gastosTotalesConfirmados = Database.getRemuneracionTotalConfirmadaByIdActividad(actividades.get(i).id);

		if (ingresosTotalesConfirmados < 0) {
				ingresosTotalesConfirmados = 0.0;
			}
			if (ingresosTotalesEstimados < 0) {
				ingresosTotalesEstimados = 0.0;
			}
			 try {
			if (actividades.get(i).name != null) {
				activityData[i][0] = actividades.get(i).name;
			}
//			
			if (actividades.get(i).numOfPlaces != 0) {
				activityData[i][1] = String.valueOf(actividades.get(i).numOfPlaces);
			}
			
			int freePlaces=(actividades.get(i).numOfPlaces)-(Database.getPlazasOcupadasSegunActividad(actividades.get(i).id));
				
			if(freePlaces>=0) {
			activityData[i][2] = String.valueOf(freePlaces);
			}
			if (actividades.get(i).startInscriptionDate != null) {
				activityData[i][3] = actividades.get(i).startInscriptionDate.toString();
			}
			if (actividades.get(i).endInscriptionDate != null) {
				activityData[i][4] = actividades.get(i).endInscriptionDate.toString();
			}
			
			Date fechaInicio=Database.getInicioActividad(actividades.get(i).id);
			
			if(fechaInicio!=null) {
			activityData[i][5] = String.valueOf(fechaInicio.toString());
			
			
			}
			if (actividades.get(i).state != null) {
				activityData[i][6] = actividades.get(i).state;
			}
			activityData[i][7] = String.valueOf(gastosTotalesEstimados);
			activityData[i][8] = String.valueOf(gastosTotalesConfirmados);
			activityData[i][9] = String.valueOf(ingresosTotalesEstimados);
			activityData[i][10] = String.valueOf(ingresosTotalesConfirmados);
			 }catch(Exception e) {

			 }
		}

		return activityData;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setViewportView(getTableActividades());
		}
		return scrollPane;
	}

	private JTable getTableActividades() {
		if (tableActividades == null) {
			parsedActividades = parseActividades();
			defTableModel = new NonEditableCellTableModel(parsedActividades, columnNamesActividades);
			

			tableActividades = new JTable(defTableModel);
			tableActividades.setBounds(0, 0, 450, 16);
			tableActividades.setSelectionBackground(Color.CYAN);
			tableActividades.setSelectionForeground(Color.BLUE);
			tableActividades.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableActividades.setDefaultRenderer(Object.class, new CambiarColorCeldaActividad());

			tableActividades.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					Point point = e.getPoint();
					int row = tableActividades.rowAtPoint(point);
					int column = tableActividades.columnAtPoint(point);
					String nombre = (String) tableActividades.getValueAt(row, column);
					nombreUltimaActividadSeleccionada = nombre;
					NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(parseInscripciones(nombre), columnNamesInscripciones);
					
					tableInscripciones.setSelectionBackground(Color.CYAN);
					tableInscripciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					tableInscripciones.setSelectionForeground(Color.BLUE);
					tableInscripciones.setModel(defTableModel);

					if (column == 0) {

						lblSeleccioneElNombre
								.setText("Las inscripciones para la actividad llamada " + nombre + " son:");
						
						lblSeleccioneElNombre.setForeground(Color.BLACK);

					} else {
						lblSeleccioneElNombre
								.setText("Seleccione el nombre de una actividad para ver sus inscripciones.");
						lblSeleccioneElNombre.setForeground(Color.RED);
					}
				}
			});
		}
		return tableActividades;
	}

	private JLabel getLblSeleccioneElNombre() {
		if (lblSeleccioneElNombre == null) {
			lblSeleccioneElNombre = new JLabel("Seleccione el nombre de una actividad para ver sus inscripciones.");
			lblSeleccioneElNombre.setForeground(Color.RED);
		}
		return lblSeleccioneElNombre;
	}

	private JScrollPane getScrollPane2() {
		if (scrollPane2 == null) {
			scrollPane2 = new JScrollPane();
			scrollPane2.setViewportView(getTableInscripciones());
		}
		return scrollPane2;
	}

	private JTable getTableInscripciones() {
		if (tableInscripciones == null) {
			tableInscripciones = new JTable();
			tableInscripciones.setSelectionBackground(Color.CYAN);
			tableInscripciones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			tableInscripciones.setSelectionForeground(Color.BLUE);
			
			NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(null, columnNamesInscripciones);

			tableInscripciones = new JTable(defTableModel);

			tableInscripciones.setDefaultRenderer(Object.class, new CambiarColorCeldaInscripcion());

			tableInscripciones.addMouseListener(new MouseAdapter() {

				public void mouseClicked(MouseEvent e) {
					// System.out.println("Pressed");

					Point point = e.getPoint();
					int row = tableInscripciones.rowAtPoint(point);
					// int column = tableInscripciones.columnAtPoint(point);
					String email = (String) tableInscripciones.getValueAt(row, 0);
					inscripcionSeleccionada = buscarInscripcionPorEmail(email);
				}

			});

		}
		return tableInscripciones;
	}

	private String[][] parseInscripciones(String nombre) {
		inscripciones = Database.getInscripcionesByNombre(nombre);
		// System.out.println(inscripciones.size());
		String[][] activityData = new String[inscripciones.size()][7];

		for (int i = 0; i < inscripciones.size(); i++) {
			Client cliente = Database.getClienteById(inscripciones.get(i).id_part);
			activityData[i][0] = cliente.getEmail();
			activityData[i][1] = cliente.getName();
			activityData[i][2] = cliente.getSurname();
			String estado = inscripciones.get(i).estado_pago;

			if (estado.equals("Sin_pagar")) {
				activityData[i][3] = "Recibida";
				
				activityData[i][6] = String.valueOf((ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripciones.get(i).id))-(ServiceFactory.getInscripcionCrudService()
						.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id)));
				
			} else if (estado.equals("Cancelada_Sin_Devolver")) {
				activityData[i][3] = "Cancelada sin devolver";
				
				activityData[i][6] = String.valueOf(0.0);
			} else if (estado.equals("Cancelada_Devuelta")) {
				activityData[i][3] = "Cancelada y devuelta";
				
				activityData[i][6] = String.valueOf(0.0);
				
			} else if (estado.equals("Correcto")) {
				activityData[i][3] = "Cobrada";
				
				activityData[i][6] = String.valueOf((ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripciones.get(i).id))-(ServiceFactory.getInscripcionCrudService()
						.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id)));
			}else if(estado.equals("Incorrecto_Mas")) {
				activityData[i][3] = "Pagada de más";
				activityData[i][6] = String.valueOf((ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripciones.get(i).id))-(ServiceFactory.getInscripcionCrudService()
						.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id)));
			}else if(estado.equals("Incorrecto_Menos")) {
				activityData[i][3] = "Pagada de menos";
				activityData[i][6] = String.valueOf((ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripciones.get(i).id))-(ServiceFactory.getInscripcionCrudService()
						.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id)));
			}

		String colectivo = ServiceFactory.getParticipanteCrudService().getColectivoByIdInscripcion(inscripciones.get(i).id);
		if(colectivo == null) {
			colectivo = "Tarifa Gratuita";
		}else if(colectivo.toLowerCase().equals("tarifa_general")) {
			colectivo = "Tarifa General";
		}
		
		
		activityData[i][4] = colectivo;
		
		activityData[i][5] = String.valueOf(ServiceFactory.getInscripcionCrudService()
				.calcularCantidadAbonadaAInscripcionById(inscripciones.get(i).id));
		
		}

		return activityData;

	}

	private JButton getBtnCancelarInscripcion() {
		if (btnCancelarInscripcion == null) {
			btnCancelarInscripcion = new JButton("Cancelar Inscripci\u00F3n");
			btnCancelarInscripcion.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (inscripcionSeleccionada == null) {
						jopError = new JOptionPane(
								"No hay ninguna inscripcion seleccionada. Seleccione una inscripción.",
								JOptionPane.ERROR_MESSAGE);
						JDialog dialog = jopError.createDialog("");
						dialog.setVisible(true);
					} else if (inscripcionSeleccionada.estado_pago.equals("Cancelada_Sin_Devolver") || inscripcionSeleccionada.estado_pago.equals("Cancelada_Devuelta")) {
						jopError = new JOptionPane("La inscripción seleccionada ya ha sido cancelada",
								JOptionPane.ERROR_MESSAGE);
						JDialog dialog = jopError.createDialog("");
						dialog.setVisible(true);
					} else {
						try {

							ServiceFactory.getPaymentService().cancelarInscripcion(inscripcionSeleccionada);
							inscripcionSeleccionada = null;

							NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(
									parseInscripciones(nombreUltimaActividadSeleccionada), columnNamesInscripciones);
							tableInscripciones.setModel(defTableModel);
							defTableModel = new NonEditableCellTableModel(parseInscripciones(nombreUltimaActividadSeleccionada), columnNamesInscripciones);
							actualizarActividades();
							
						} catch (IllegalArgumentException e) {
							jopError = new JOptionPane(e.getMessage(), JOptionPane.ERROR_MESSAGE);
							JDialog dialog = jopError.createDialog("");
							dialog.setVisible(true);
						}
					}
				}
			});
		}
		return btnCancelarInscripcion;
	}

	private InscripcionDto buscarInscripcionPorEmail(String email) {
		for (InscripcionDto i : inscripciones) {
			Client c = Database.getCliente(i);
			if (c.getEmail().equals(email)) {
				return i;
			}
		}

		return null;
	}

	
	private JButton getBtnCerrarActividad() {
		if (btnCerrarActividad == null) {
			btnCerrarActividad = new JButton("Cerrar Actividad");
			btnCerrarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent a) {
					int row = tableActividades.getSelectedRow();
					if (row > 0) { // if there is a row selected

						ActivityDto act = actividades.get(row);

						if (act.state.equals("Periodo de Inscripción") || act.state.equals("Inscripción Cerrada")) { 

							Calendar c = Calendar.getInstance();
							Date today = c.getTime();
							
							c.add(Calendar.DATE, -1);
							Date mindate = c.getTime(); //initialize to yesterday in case there are no sesions (always true)
							
							List<SesionDto> sesiones = ServiceFactory.getActivityService().findSesionesOfActivity(act.id);
							//set mindate to the f_finalizacion of the latest sesion
							for(SesionDto s : sesiones) {
								if(s.f_finalizacion.compareTo(mindate)>0) {
									mindate = s.f_finalizacion;
								}
							}
							

							if (today.after(mindate)) { // solo se pueden cerrar actividades que ya se han celebrado (ya han pasado todas las sesiones)
	
								System.out.println(Database.getRemuneracionTotalByIdActividad(act.id) +"=="+ Database.getRemuneracionTotalConfirmadaByIdActividad(act.id));
								
								//if (ServiceFactory.getActivityService().isActivityReadyToBeClosed(act)) {
								if(Database.getRemuneracionTotalByIdActividad(act.id).equals(Database.getRemuneracionTotalConfirmadaByIdActividad(act.id))) {
									ServiceFactory.getActivityService().closeActivity(act);
									JOptionPane.showMessageDialog(null, "La actividad ha sido cerrada");
									// update lista actividades
									actualizarActividades();

								} else {
									Object[] options = { "Continuar de todos modos", "Cancelar" };
									String aviso = "La actividad que intentas cerrar tiene pagos sin confirmar, ¿está seguro de que quiere continuar?";
									String title = "Alerta";

									int respuesta = JOptionPane.showOptionDialog(null, aviso, title,
											JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
											options[0]);
									if (respuesta == 0) {
										// ServiceFactory.getActivityService().closeActivity(act);
										act.state = "Cerrada";
										ServiceFactory.getActivityService().updateActivity(act);
										// update lista actividades
										actualizarActividades();

									}
								}
							} else {
								JOptionPane.showMessageDialog(null,
										"No se pueden cerrar actividades que no se han celebrado todavía", "ERROR",
										JOptionPane.ERROR_MESSAGE, null);
							}

						} else {
							JOptionPane.showMessageDialog(null,
									"No se pueden cerrar actividades que estén en estado 'Cerrada' o 'Cancelada'",
									"ERROR", JOptionPane.ERROR_MESSAGE, null);

						}
					}else {
						JOptionPane.showMessageDialog(null,
								"Para cerrar una actividad tienes que seleccionarla",
								"ERROR", JOptionPane.ERROR_MESSAGE, null);
					}
				}
			});
		}
		return btnCerrarActividad;
	}
	
	
	
	
	private JButton getBtnCancelarActividad() {
		if (btnCancelarActividad == null) {
			btnCancelarActividad = new JButton("Cancelar Actividad");
			btnCancelarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					int row = tableActividades.getSelectedRow();
					if (row > 0) { // if there is a row selected

						ActivityDto act = actividades.get(row);

						if (act.state.equals("Periodo de Inscripción") || act.state.equals("Inscripción Cerrada") || act.state.equals("Planificada")) { //la actividad tiene que estar en curso o planificada

							Calendar c = Calendar.getInstance();
							Date today = c.getTime();
							
							c.add(Calendar.DAY_OF_YEAR, 1);
							Date maxdate = c.getTime(); //initialize to tomorrow in case there are no sesions (always false)
							
							List<SesionDto> sesiones = ServiceFactory.getActivityService().findSesionesOfActivity(act.id);
							//set maxdate to the f_finalizacion of the latest sesion
							for(SesionDto s : sesiones) {
								if(s.f_finalizacion.compareTo(maxdate)<0) {
									maxdate = s.f_finalizacion;
								}
							}
							

							if (today.before(maxdate)) { // solo se pueden cancelar actividades que no se han celebrado del todo (no han pasado todas las sesiones)

									Object[] options = { "Continuar de todos modos", "Cancelar" };
									String aviso = "Cancelar una actividad supone devolver a sus participantes el 100% de su inscripción, ¿está seguro de que quiere continuar?";
									String title = "Alerta";

									int respuesta = JOptionPane.showOptionDialog(null, aviso, title,
											JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options,
											options[0]);
									if (respuesta == 0) {
										
										
										
										//poner sus inscripciones como cancelada_sin_devolver y avisar a los afectados de la cancelación de la actividad
										List<InscripcionDto> inscripciones = ServiceFactory.getInscripcionCrudService().findInscriptionsByActivity(act.id);
										for(InscripcionDto ins : inscripciones) {
											ServiceFactory.getPaymentService().cancelarInscripcion(ins);
											ServiceFactory.getInscriptionService().avisarInscritoDeCancelaciónActividad(ins);
											
										}
										
										//poner la actividad como cancelada
										act.state = "Cancelada";
										ServiceFactory.getActivityService().updateActivity(act);
										
										// update lista actividades
										actualizarActividades();
										
										//update lista inscripciones
										NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(
												parseInscripciones(nombreUltimaActividadSeleccionada), columnNamesInscripciones);
										tableInscripciones.setModel(defTableModel);
									}
								
								
								
							} else {
								JOptionPane.showMessageDialog(null,
										"No se pueden cancelar actividades que ya se han celebrado ", "ERROR",
										JOptionPane.ERROR_MESSAGE, null);
							}

						} else {
							JOptionPane.showMessageDialog(null,
									"No se pueden cancelar actividades que estén en estado 'Cerrada' o 'Cancelada'",
									"ERROR", JOptionPane.ERROR_MESSAGE, null);

						}
					}else {
						JOptionPane.showMessageDialog(null,
								"Para cancelar una actividad tienes que seleccionarla",
								"ERROR", JOptionPane.ERROR_MESSAGE, null);
					}
					
				}
			});
		}
		return btnCancelarActividad;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.add(getBtnCerrarActividad());
			panel.add(getBtnCancelarActividad());
			panel.add(getBtnRetrasarActividad());
		}
		return panel;
	}
	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.add(getBtnCancelarInscripcion());
		}
		return panel_1;
	}
	private JPanel getPanel_2() {
		if (panel_2 == null) {
			panel_2 = new JPanel();
			panel_2.add(getLblListaDeInscripciones());
		}
		return panel_2;
	}
	private JLabel getLblListaDeInscripciones() {
		if (lblListaDeInscripciones == null) {
			lblListaDeInscripciones = new JLabel("Lista de Inscripciones:");
			lblListaDeInscripciones.setFont(new Font("Tahoma", Font.BOLD, 18));
		}
		return lblListaDeInscripciones;
	}
	private JPanel getPanel_3() {
		if (panel_3 == null) {
			panel_3 = new JPanel();
			panel_3.setLayout(null);
		}
		return panel_3;
	}
	private JPanel getPanel_4() {
		if (panel_4 == null) {
			panel_4 = new JPanel();
			panel_4.setLayout(new BorderLayout(0, 0));
			panel_4.add(getLblSeleccioneElNombre());
		}
		return panel_4;
	}
	private JButton getBtnRetrasarActividad() {
		if (btnRetrasarActividad == null) {
			btnRetrasarActividad = new JButton("Retrasar Actividad");
			btnRetrasarActividad.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int column = tableActividades.getSelectedColumn();
					int row = tableActividades.getSelectedRow();
					if (column == 0) { 
						if (actividades.get(row).state.equals("Periodo de Inscripción") || actividades.get(row).state.equals("Inscripción Cerrada") || actividades.get(row).state.equals("Planificada")) {
					       new RetrasarActividadDialog(nombreUltimaActividadSeleccionada).setVisible(true);
					       
						} else {
							JOptionPane.showMessageDialog(null,
									"No se puede retrasar una actividad que este cerrada o cancelada",
									"ERROR", JOptionPane.ERROR_MESSAGE, null);

						}
					 
					
					}
					else {
						JOptionPane.showMessageDialog(null,
								"Seleccione el nombre de la actividad que quiere cancelar",
								"ERROR", JOptionPane.ERROR_MESSAGE, null);
					}
					
					
					
				}
			});
			
		}
		return btnRetrasarActividad;
	}

	public static void actualizarActividades() {
		actividades = (ArrayList<ActivityDto>) ServiceFactory.getActivityService().listActivities();
		parsedActividades = parseActividades();
		defTableModel = new NonEditableCellTableModel(parsedActividades, columnNamesActividades);
		tableActividades.setModel(defTableModel);
		
	}
}
