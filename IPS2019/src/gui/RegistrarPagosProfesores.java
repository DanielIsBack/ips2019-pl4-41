package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JDateChooser;

import business.Actividad;
import business.Database;
import business.PagoProfesor;
import business.Profesor;
import gui.cambiar_colores.CambiarColorCeldaFactura;
import gui.cambiar_colores.CambiarColorCeldaPagos;
import gui.tableextensions.NonEditableCellTableModel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class RegistrarPagosProfesores extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1234342345346534L;
	private JPanel contentPane;
	private JComboBox<Actividad> comboBox;
	private JLabel lblActividadesFinalizadas;
	private JLabel lblElProfesorEs;
	private JButton btnPagar;
	private JLabel lblElImporteQue;
	private JTextField textField;
	private JLabel lblSeLeHa;
	private JLabel lblQuedaPorAbonarle;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPanel panel;
	private JLabel lblCantidadAAbonar;
	private JTextField cantidadAbonar;

	LinkedList<business.Factura> facturas = null;
	LinkedList<PagoProfesor> pagos = null;
	private JScrollPane scrollPane;
	private JTable tableFacturas;
	private JScrollPane scrollPane_1;
	private JTable tablePagos;
	private JPanel panel_1;
	private JButton btnGenerarFactura;
	private JTextField textField_3;
	private JTextField textField_5;
	private JLabel lblNumeroFactura;
	private JLabel lblFecha;
	private JLabel lblImporteTotal;

	private Actividad actividadSeleccionada;
	private Profesor profesorSeleccionado;
	private JDateChooser dateChooserFactura;
	private JComboBox<Profesor> comboBoxProfesores;
	private JLabel label;
	private JDateChooser dateChooserTransferencia;
	private JRadioButton rdbtnPorCaja;
	private JRadioButton rdbtnPorTransferencia;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegistrarPagosProfesores frame = new RegistrarPagosProfesores();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegistrarPagosProfesores() {
		setResizable(false);
		setLocationRelativeTo(null);
		setTitle("Registrar Pagos Profesores");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 724, 517);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		contentPane.add(getComboBoxActividades());
		contentPane.add(getLblActividadesFinalizadas());
		contentPane.add(getLblElProfesorEs());
		contentPane.add(getLblElImporteQue());
		contentPane.add(getTextField());
		contentPane.add(getLblSeLeHa());
		contentPane.add(getLblQuedaPorAbonarle());
		contentPane.add(getTextField_1());
		contentPane.add(getTextField_2());
		contentPane.add(getPanel());
		contentPane.add(getScrollPane());
		contentPane.add(getScrollPane_1());
		contentPane.add(getPanel_1());
		contentPane.add(getComboBoxProfesores());
	}

	private JComboBox<Actividad> getComboBoxActividades() {
		if (comboBox == null) {
			comboBox = new JComboBox<Actividad>();

			comboBox.setBounds(10, 36, 688, 20);

			// comboBox.removeAllItems();

			LinkedList<Actividad> actividadesFinalizadas = Database.getActividadesPendientesDePagarProfesor();
			for (Actividad a : actividadesFinalizadas) {
				comboBox.addItem(a);
			}
			
			//rellenaComboboxProfesores();
			
			comboBox.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent arg0) {
					rellenaComboboxProfesores();
					
				}
			});
		}
		return comboBox;
	}
	
	private void rellenaComboboxProfesores() {
		comboBoxProfesores.removeAllItems();
		LinkedList<Profesor> profesores = Database.getProfesoresSegunActividad(comboBox.getSelectedItem().toString());
		Actividad actividad = (Actividad) comboBox.getSelectedItem();
		actividadSeleccionada = actividad;
		
		for (Profesor p : profesores) {
			comboBoxProfesores.addItem(p);
		}
		
		
	}

	private JLabel getLblActividadesFinalizadas() {
		if (lblActividadesFinalizadas == null) {
			lblActividadesFinalizadas = new JLabel("Actividades finalizadas:");
			lblActividadesFinalizadas.setBounds(10, 11, 231, 14);
		}
		return lblActividadesFinalizadas;
	}

	private JLabel getLblElProfesorEs() {
		if (lblElProfesorEs == null) {
			lblElProfesorEs = new JLabel("El profesor es:");
			lblElProfesorEs.setBounds(10, 67, 99, 14);
		}
		return lblElProfesorEs;
	}

	private JButton getBtnPagar() {
		if (btnPagar == null) {
			btnPagar = new JButton("Enviar Transferencia");
			btnPagar.setBounds(10, 111, 315, 23);
			btnPagar.addActionListener(new ActionListener() {
				private String estado;

				public void actionPerformed(ActionEvent arg0) {
					// Actualizar cantidades y estados EN LAS TABLAS NECESARIAS LAS TABLAS CON
					// INSERTS...

					if (cantidadAbonar.getText().isEmpty() || dateChooserTransferencia.getDate()==null) {
						JOptionPane jopError = new JOptionPane("Los campos no pueden estar vacios.",
								JOptionPane.ERROR_MESSAGE);
						JDialog dialog = jopError.createDialog("");
						dialog.setVisible(true);

					} else {
						try {
							if (Double.valueOf(cantidadAbonar.getText()) > Double.valueOf(textField_2.getText())) {
								JOptionPane jopError = new JOptionPane(
										"No puedes hacer una transferencia mayor que la cantidad pendiente a abonar.",
										JOptionPane.ERROR_MESSAGE);
								JDialog dialog = jopError.createDialog("");
								dialog.setVisible(true);

							} else if (Double.valueOf(cantidadAbonar.getText()) <= 0) {
								JOptionPane jopError = new JOptionPane(
										"No puedes hacer una transferencia negativa o igual a 0.",
										JOptionPane.ERROR_MESSAGE);
								JDialog dialog = jopError.createDialog("");
								dialog.setVisible(true);

							} else {
								if (Double.valueOf(cantidadAbonar.getText())
										.equals(Double.valueOf(textField_2.getText()))) {
									estado = "Correcto";
								} else {
									estado = "Incorrecto_Menos";
								}
								Database.pagarProfesor(actividadSeleccionada,profesorSeleccionado,
										Double.valueOf(cantidadAbonar.getText()), estado,dateChooserTransferencia.getDate());
//								Database.generarTransferencia((Actividad) comboBox.getSelectedItem(),
//										cantidadAbonar.getText(), profesor.get);
								double importe = Database.getImporteSegunActividadProfesor(actividadSeleccionada, profesorSeleccionado);
		

								//LinkedList<Profesor> profesor = Database
								//		.getProfesoresSegunActividad(comboBox.getSelectedItem().toString());
								Actividad actividad = (Actividad) comboBox.getSelectedItem();
								textField_1.setText(String.valueOf(
										Database.getDineroAbonadoAunProfesorPorUnaActividad(profesorSeleccionado, actividad)));
								textField_2.setText(String.valueOf(importe
										- Database.getDineroAbonadoAunProfesorPorUnaActividad(profesorSeleccionado, actividad)));

//								if (estado == "Correcto") {
//									dispose();
//									RegistrarPagosProfesores r = new RegistrarPagosProfesores();
//									r.setVisible(true);
//						comboBox.removeAllItems();
//						
//						LinkedList<Actividad> actividadesFinalizadas = Database.getActividadesPendientesDePagarProfesor();
//						for(Actividad a : actividadesFinalizadas) {
//								comboBox.addItem(a);
//							}

								//}
								String[] columnNames2 = { "Estado", "Fecha", "Cantidad abonada" };
								DefaultTableModel defTableModel2 = new DefaultTableModel(
										parsePagos(actividad.getAct_id()), columnNames2);

								tablePagos.setModel(defTableModel2);
							}
						} catch (NumberFormatException e) {
							JOptionPane jopError = new JOptionPane("La cantidad a abonar tiene que ser un numero.",
									JOptionPane.ERROR_MESSAGE);
							JDialog dialog = jopError.createDialog("");
							dialog.setVisible(true);

						}

					}
				}
			});
			btnPagar.setVisible(true);
		}
		return btnPagar;
	}

	private JLabel getLblElImporteQue() {
		if (lblElImporteQue == null) {
			lblElImporteQue = new JLabel("El importe que le corresponde cobrar por la actividad impartida es:");
			lblElImporteQue.setBounds(10, 95, 427, 14);
		}
		return lblElImporteQue;
	}

	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setEnabled(false);
			textField.setEditable(false);
			textField.setBounds(402, 92, 86, 20);
			textField.setColumns(10);
		}
		return textField;
	}

	private JLabel getLblSeLeHa() {
		if (lblSeLeHa == null) {
			lblSeLeHa = new JLabel("Se le ha abonado:");
			lblSeLeHa.setBounds(10, 120, 171, 14);
		}
		return lblSeLeHa;
	}

	private JLabel getLblQuedaPorAbonarle() {
		if (lblQuedaPorAbonarle == null) {
			lblQuedaPorAbonarle = new JLabel("Queda por abonarle:");
			lblQuedaPorAbonarle.setBounds(10, 145, 137, 14);
		}
		return lblQuedaPorAbonarle;
	}

	private JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.setEditable(false);
			textField_1.setEnabled(false);
			textField_1.setBounds(184, 120, 86, 20);
			textField_1.setColumns(10);
		}
		return textField_1;
	}

	private JTextField getTextField_2() {
		if (textField_2 == null) {
			textField_2 = new JTextField();
			textField_2.setEnabled(false);
			textField_2.setEditable(false);
			textField_2.setBounds(184, 145, 86, 20);
			textField_2.setColumns(10);
		}
		return textField_2;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel.setBounds(363, 176, 335, 145);
			panel.setLayout(null);
			panel.add(getBtnPagar());
			panel.add(getLblCantidadAAbonar());
			panel.add(getCantidadAbonar());
			panel.add(getLabel());
			panel.add(getDateChooser_1());
			panel.add(getRdbtnPorCaja());
			panel.add(getRdbtnPorTransferencia());
		}
		return panel;
	}

	private JLabel getLblCantidadAAbonar() {
		if (lblCantidadAAbonar == null) {
			lblCantidadAAbonar = new JLabel("Cantidad a abonar:");
			lblCantidadAAbonar.setBounds(19, 52, 160, 14);
		}
		return lblCantidadAAbonar;
	}

	private JTextField getCantidadAbonar() {
		if (cantidadAbonar == null) {
			cantidadAbonar = new JTextField();
			cantidadAbonar.setBounds(211, 49, 86, 20);
			cantidadAbonar.setColumns(10);
		}
		return cantidadAbonar;
	}

	private String[][] parseFacturas(int actividad) {
		
		facturas = Database.getFactura(actividad,profesorSeleccionado.getId());
		// System.out.println(inscripciones.size());
		String[][] activityData = new String[facturas.size()][4];

		for (int i = 0; i < facturas.size(); i++) {

			activityData[i][0] = String.valueOf(facturas.get(i).numeroFactura);
			activityData[i][1] = String.valueOf(facturas.get(i).fecha);
			activityData[i][2] = String.valueOf(facturas.get(i).importe);
			activityData[i][3] = String.valueOf(facturas.get(i).estado);

		}

		return activityData;
	}

	private String[][] parsePagos(int actividad) {
		pagos = Database.getPagos(actividad,profesorSeleccionado.getId());
		// System.out.println(inscripciones.size());
		String[][] activityData = new String[pagos.size()][3];

		for (int i = 0; i < pagos.size(); i++) {

			if(String.valueOf(pagos.get(i).estado).equals("Incorrecto_Menos")) {
				activityData[i][0] = "Falta por pagarle";
			}else {
			activityData[i][0] = String.valueOf(pagos.get(i).estado);
			}
			activityData[i][1] = String.valueOf(pagos.get(i).f_pago);
			activityData[i][2] = String.valueOf(pagos.get(i).cantidad_abonada);

		}

		return activityData;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 332, 343, 126);
			scrollPane.setViewportView(getTableFacturas());
		}
		return scrollPane;
	}

	private JTable getTableFacturas() {
		if (tableFacturas == null) {
			tableFacturas = new JTable();
			tableFacturas.setDefaultRenderer(Object.class, new CambiarColorCeldaFactura());
			String[] columnNames = { "N�mero factura", "Fecha", "Importe","Estado" };

			NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(null, columnNames);
			tableFacturas.setModel(defTableModel);
		}
		return tableFacturas;
	}

	private JScrollPane getScrollPane_1() {
		if (scrollPane_1 == null) {
			scrollPane_1 = new JScrollPane();
			scrollPane_1.setBounds(363, 332, 335, 126);
			scrollPane_1.setViewportView(getTablePagos());
		}
		return scrollPane_1;
	}

	private JTable getTablePagos() {
		if (tablePagos == null) {
			tablePagos = new JTable();
			String[] columnNames = { "Estado", "Fecha", "Cantidad abonada" };
			tablePagos.setDefaultRenderer(Object.class, new CambiarColorCeldaPagos());

			NonEditableCellTableModel defTableModel = new NonEditableCellTableModel(null, columnNames);
			tablePagos.setModel(defTableModel);
		}
		return tablePagos;
	}

	private JPanel getPanel_1() {
		if (panel_1 == null) {
			panel_1 = new JPanel();
			panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel_1.setBounds(10, 176, 343, 145);
			panel_1.setLayout(null);
			panel_1.add(getBtnGenerarFactura());
			panel_1.add(getTextField_3());
			panel_1.add(getTextField_5());
			panel_1.add(getLblNumeroFactura());
			panel_1.add(getLblFecha());
			panel_1.add(getLblImporteTotal());
			panel_1.add(getDateChooserFactura());
		}
		return panel_1;
	}

	private JButton getBtnGenerarFactura() {
		if (btnGenerarFactura == null) {
			btnGenerarFactura = new JButton("Generar factura");
			btnGenerarFactura.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					if (textField_3.getText().isEmpty() || textField_5.getText().isEmpty() || dateChooserFactura.getDate()==null) {
						JOptionPane jopError = new JOptionPane("Los campos no pueden estar vacios.",
								JOptionPane.ERROR_MESSAGE);
						JDialog dialog = jopError.createDialog("");
						dialog.setVisible(true);

					} else {
						try {
//						if (!(Double.valueOf(textField_5.getText()) == (Double.valueOf(textField_2.getText())
//								+ Double.valueOf(textField_1.getText())))) {
//							JOptionPane jopError = new JOptionPane(
//									"El importe total que estas introduciendo no corresponde con el de la actividad.",
//									JOptionPane.ERROR_MESSAGE);
//							JDialog dialog = jopError.createDialog("");
//							dialog.setVisible(true);
//
						//}
                       String estado="Correcto";
							if (!(Double.valueOf(textField_5.getText()) == (Double.valueOf(textField_2.getText())
									+ Double.valueOf(textField_1.getText())))) {
								estado="Incorrecto";
							}
							
							Database.generarFacturaProfesor(actividadSeleccionada.getId_act(),
									profesorSeleccionado.getId(), dateChooserFactura.getDate(),
									Double.valueOf(textField_5.getText()), Integer.valueOf(textField_3.getText()),estado);
						

						String[] columnNames = { "N�mero factura", "Fecha", "Importe", "Estado"};
						DefaultTableModel defTableModel = new DefaultTableModel(
								parseFacturas(actividadSeleccionada.getAct_id()), columnNames);

						tableFacturas.setModel(defTableModel);

						
						  } catch (NumberFormatException e) { JOptionPane jopError = new
						  JOptionPane("Datos introducidos incorrectamente.",
						  JOptionPane.ERROR_MESSAGE); JDialog dialog = jopError.createDialog("");
						  dialog.setVisible(true);
						 
						  }
						 

					}
				}
			});
			btnGenerarFactura.setBounds(10, 111, 323, 23);
		}
		return btnGenerarFactura;
	}

	private JTextField getTextField_3() {
		if (textField_3 == null) {
			textField_3 = new JTextField();
			textField_3.setBounds(202, 11, 86, 20);
			textField_3.setColumns(10);
		}
		return textField_3;
	}

	private JTextField getTextField_5() {
		if (textField_5 == null) {
			textField_5 = new JTextField();
			textField_5.setBounds(202, 69, 86, 20);
			textField_5.setColumns(10);
		}
		return textField_5;
	}

	private JLabel getLblNumeroFactura() {
		if (lblNumeroFactura == null) {
			lblNumeroFactura = new JLabel("Numero factura:");
			lblNumeroFactura.setBounds(34, 14, 142, 14);
		}
		return lblNumeroFactura;
	}

	private JLabel getLblFecha() {
		if (lblFecha == null) {
			lblFecha = new JLabel("Fecha:");
			lblFecha.setBounds(34, 47, 109, 14);
		}
		return lblFecha;
	}

	private JLabel getLblImporteTotal() {
		if (lblImporteTotal == null) {
			lblImporteTotal = new JLabel("Importe Total:");
			lblImporteTotal.setBounds(34, 72, 111, 14);
		}
		return lblImporteTotal;
	}

	private JDateChooser getDateChooserFactura() {
		if (dateChooserFactura == null) {
			dateChooserFactura = new JDateChooser();
			dateChooserFactura.setBounds(202, 42, 102, 20);
		}
		return dateChooserFactura;
	}
	private JComboBox<Profesor> getComboBoxProfesores() {
		if (comboBoxProfesores == null) {
			comboBoxProfesores = new JComboBox<Profesor>();
			comboBoxProfesores.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					Profesor profesor = (Profesor) comboBoxProfesores.getSelectedItem();
					profesorSeleccionado = profesor;
					
					if(profesorSeleccionado!=null) {
					btnPagar.setText("Enviar Transferencia a " + profesorSeleccionado.getNombre());
					}else {
						btnPagar.setText("Enviar Transferencia a ");
					}
					if(profesorSeleccionado!=null) {
					double importe = Database.getImporteSegunActividadProfesor(actividadSeleccionada,profesorSeleccionado);
					textField.setText(String.valueOf(importe));

					textField_1.setText(
							String.valueOf(Database.getDineroAbonadoAunProfesorPorUnaActividad(profesorSeleccionado, actividadSeleccionada)));
					textField_2.setText(String.valueOf(
							importe - Database.getDineroAbonadoAunProfesorPorUnaActividad(profesorSeleccionado, actividadSeleccionada)));
					
					String[] columnNames = { "N�mero factura", "Fecha", "Importe", "Estado"};
					
					DefaultTableModel defTableModel = new DefaultTableModel(parseFacturas(actividadSeleccionada.getAct_id()),
							columnNames);

					tableFacturas.setModel(defTableModel);
					
					
					String[] columnNames2 = { "Estado", "Fecha", "Cantidad abonada" };
					DefaultTableModel defTableModel2 = new DefaultTableModel(parsePagos(actividadSeleccionada.getAct_id()),
							columnNames2);

					tablePagos.setModel(defTableModel2);
					}

				}
			});
			rellenaComboboxProfesores();
			comboBoxProfesores.setBounds(153, 64, 335, 20);
		}
		return comboBoxProfesores;
	}
	private JLabel getLabel() {
		if (label == null) {
			label = new JLabel("Fecha:");
			label.setBounds(20, 77, 78, 14);
		}
		return label;
	}
	private JDateChooser getDateChooser_1() {
		if (dateChooserTransferencia == null) {
			dateChooserTransferencia = new JDateChooser();
			dateChooserTransferencia.setBounds(211, 80, 95, 20);
		}
		return dateChooserTransferencia;
	}
	private JRadioButton getRdbtnPorCaja() {
		if (rdbtnPorCaja == null) {
			rdbtnPorCaja = new JRadioButton("Por caja");
			buttonGroup.add(rdbtnPorCaja);
			rdbtnPorCaja.setBounds(192, 7, 109, 23);
		}
		return rdbtnPorCaja;
	}
	private JRadioButton getRdbtnPorTransferencia() {
		if (rdbtnPorTransferencia == null) {
			rdbtnPorTransferencia = new JRadioButton("Por transferencia");
			rdbtnPorTransferencia.setSelected(true);
			buttonGroup.add(rdbtnPorTransferencia);
			rdbtnPorTransferencia.setBounds(43, 7, 160, 23);
		}
		return rdbtnPorTransferencia;
	}
}
