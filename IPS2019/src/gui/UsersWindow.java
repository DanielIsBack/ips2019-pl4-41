package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import business.Client;
import business.Database;

public class UsersWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// database
	static LinkedList<Client> clients;

	// GUI elements
	private JPanel contentPane;
	private JPanel pnAddUser;
	private JPanel pnBanner;
	private JLabel lblListOfUsers;
	private JTable tUsers;
	private JLabel lblName;
	private JTextField txtName;
	private JLabel lblSurname;
	private JTextField txtSurname;
	private JLabel lblDNI;
	private JTextField txtDNI;
	private JButton btnAddUser;
	private JSeparator separator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					clients = Database.getClients();
					UsersWindow frame = new UsersWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Logic
	private String[][] parseClients() {
		String[][] clientData = new String[clients.size()][3];

		for (int cl = 0; cl < clients.size(); cl++) {
			// clientData[cl][0]=clients.get(cl).getDNI();
			clientData[cl][1] = clients.get(cl).getName();
			clientData[cl][2] = clients.get(cl).getSurname();

		}
		return clientData;
	}

	// GUI
	/**
	 * Create the frame.
	 */
	public UsersWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 500);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		contentPane.add(getPnBanner(), BorderLayout.NORTH);
		contentPane.add(getTUsers(), BorderLayout.CENTER);
		contentPane.add(getPnAddUser(), BorderLayout.SOUTH);
	}

	private JPanel getPnAddUser() {
		if (pnAddUser == null) {
			pnAddUser = new JPanel();
			pnAddUser.setLayout(new GridLayout(1, 0, 0, 0));
			pnAddUser.add(getLblDNI());
			pnAddUser.add(getTxtDNI());
			pnAddUser.add(getLblName());
			pnAddUser.add(getTxtName());
			pnAddUser.add(getLblSurname());
			pnAddUser.add(getTxtSurname());
			pnAddUser.add(getSeparator());
			pnAddUser.add(getBtnAddUser());
		}
		return pnAddUser;
	}

	private JPanel getPnBanner() {
		if (pnBanner == null) {
			pnBanner = new JPanel();
			pnBanner.add(getLblListOfUsers());
		}
		return pnBanner;
	}

	private JLabel getLblListOfUsers() {
		if (lblListOfUsers == null) {
			lblListOfUsers = new JLabel("List of users:");
			lblListOfUsers.setFont(new Font("Tahoma", Font.BOLD, 18));
		}
		return lblListOfUsers;
	}

	private JTable getTUsers() {
		if (tUsers == null) {
			String[] columnNames = { "DNI", "Name", "Surname" };
			String[][] parsedClients = parseClients();

			DefaultTableModel defTableModel = new DefaultTableModel(parsedClients, columnNames);

			tUsers = new JTable(defTableModel);

		}
		return tUsers;
	}

	private JLabel getLblName() {
		if (lblName == null) {
			lblName = new JLabel("Name:");
			lblName.setHorizontalAlignment(SwingConstants.RIGHT);
			lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblName;
	}

	private JTextField getTxtName() {
		if (txtName == null) {
			txtName = new JTextField();
			txtName.setColumns(10);
		}
		return txtName;
	}

	private JLabel getLblSurname() {
		if (lblSurname == null) {
			lblSurname = new JLabel("Surname:");
			lblSurname.setHorizontalAlignment(SwingConstants.RIGHT);
			lblSurname.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblSurname;
	}

	private JTextField getTxtSurname() {
		if (txtSurname == null) {
			txtSurname = new JTextField();
			txtSurname.setColumns(10);
		}
		return txtSurname;
	}

	private JLabel getLblDNI() {
		if (lblDNI == null) {
			lblDNI = new JLabel("DNI:");
			lblDNI.setHorizontalAlignment(SwingConstants.RIGHT);
			lblDNI.setFont(new Font("Tahoma", Font.BOLD, 14));
		}
		return lblDNI;
	}

	private JTextField getTxtDNI() {
		if (txtDNI == null) {
			txtDNI = new JTextField();
			txtDNI.setColumns(10);
		}
		return txtDNI;
	}

	private JButton getBtnAddUser() {
		if (btnAddUser == null) {
			btnAddUser = new JButton("Add User");
			btnAddUser.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					if (!txtDNI.getText().equals("") && !txtName.getText().equals("")
							&& !txtSurname.getText().equals("")) {

						Database.addClient(new Client(txtDNI.getText(), txtName.getText(), txtSurname.getText()));

						txtDNI.setText("");
						txtName.setText("");
						txtSurname.setText("");
						JOptionPane.showMessageDialog(contentPane, "User added to the database successfully");
					}
				}
			});
		}
		return btnAddUser;
	}

	private JSeparator getSeparator() {
		if (separator == null) {
			separator = new JSeparator();
		}
		return separator;
	}
}
