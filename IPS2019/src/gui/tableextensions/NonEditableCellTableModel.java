package gui.tableextensions;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class NonEditableCellTableModel extends DefaultTableModel {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	public NonEditableCellTableModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NonEditableCellTableModel(int rowCount, int columnCount) {
		super(rowCount, columnCount);
		// TODO Auto-generated constructor stub
	}

	public NonEditableCellTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
		// TODO Auto-generated constructor stub
	}

	public NonEditableCellTableModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
		// TODO Auto-generated constructor stub
	}

	public NonEditableCellTableModel(Vector columnNames, int rowCount) {
		super(columnNames, rowCount);
		// TODO Auto-generated constructor stub
	}

	public NonEditableCellTableModel(Vector data, Vector columnNames) {
		super(data, columnNames);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return false;
	};

}
