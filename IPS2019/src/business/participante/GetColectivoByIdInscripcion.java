package business.participante;

import factory.PersistenceFactory;
import persistence.impl.ParticipanteGateway;

public class GetColectivoByIdInscripcion {
	private int idInscripcion;
	private ParticipanteGateway gw = PersistenceFactory.getParticipanteGateway();

	public GetColectivoByIdInscripcion(int idInscripcion) {
		this.idInscripcion = idInscripcion;
	}

	public String execute() {
		return gw.getColectivoByIdInscripcion(idInscripcion);
	}
}
