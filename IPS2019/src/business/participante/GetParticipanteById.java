package business.participante;

import business.dto.ParticipanteDto;
import factory.PersistenceFactory;
import persistence.impl.ParticipanteGateway;

public class GetParticipanteById {

	private int id;
	private ParticipanteGateway gw = PersistenceFactory.getParticipanteGateway();

	public GetParticipanteById(int id) {
		this.id = id;
	}

	public ParticipanteDto execute() {
		return gw.findParticipanteById(id);
	}

}
