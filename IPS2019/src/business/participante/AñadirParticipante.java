package business.participante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import business.dto.ParticipanteDto;
import config.Conf;
import util.DbUtil;

public class AņadirParticipante {

	ParticipanteDto p;

	// SQL
	private String query_aņadir_participante = Conf.getInstance().getProperty("SQL_AŅADIR_PARTICIPANTE");

	public AņadirParticipante(ParticipanteDto p) {
		this.p = p;
	}

	public void execute() {
		Connection c;
		PreparedStatement s;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_aņadir_participante);

			// set properties
			s.setString(1, p.nombre);
			s.setString(2, p.apellidos);
			s.setString(3, p.email);

			// execute insertion
			s.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
