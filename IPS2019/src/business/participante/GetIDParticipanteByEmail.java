package business.participante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import config.Conf;
import util.DbUtil;

public class GetIDParticipanteByEmail {

	String DNI;

	// SQL
	private String query_get_id_by_email = Conf.getInstance().getProperty("SQL_GET_ID_BY_EMAIL");

	public GetIDParticipanteByEmail(String DNI) {
		this.DNI = DNI;
	}

	public int execute() {
		Connection c;
		PreparedStatement s;
		ResultSet rs;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_get_id_by_email);

			// set properties
			s.setString(1, DNI);

			// execute insertion
			rs = s.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1000;

	}

}
