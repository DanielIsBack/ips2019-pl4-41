package business.participante;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import business.dto.ParticipanteDto;
import config.Conf;
import util.DbUtil;

public class ComprobarParticipanteExiste {

	ParticipanteDto p;

	// SQL
	private String query_comprobar_participante_existe = Conf.getInstance()
			.getProperty("SQL_COMPROBAR_PARTICIPANTE_EXISTE");

	public ComprobarParticipanteExiste(ParticipanteDto p) {
		this.p = p;
	}

	/**
	 * 
	 * @return True si hay un participante con el mismo email que el dado, False si
	 *         no.
	 */
	public boolean execute() {
		Connection c;
		PreparedStatement s;
		ResultSet rs;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_comprobar_participante_existe);

			// set properties
			s.setString(1, p.email);

			// execute insertion
			rs = s.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
