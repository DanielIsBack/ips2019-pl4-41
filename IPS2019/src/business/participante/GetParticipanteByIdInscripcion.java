package business.participante;

import java.util.List;

import business.dto.ParticipanteDto;
import factory.PersistenceFactory;
import persistence.impl.ParticipanteGateway;

public class GetParticipanteByIdInscripcion {
	private int idInscripcion;
	private ParticipanteGateway gw = PersistenceFactory.getParticipanteGateway();

	public GetParticipanteByIdInscripcion(int idInscripcion) {
		this.idInscripcion = idInscripcion;
	}

	public List<ParticipanteDto> execute() {
		return gw.findParticipanteByIdInscripcion(idInscripcion);
	}
}
