package business;

import java.util.Date;

public class Factura {

	public int id;
	public int numeroFactura;
	int id_actividad;
	int id_profesor;
	public Date fecha;
	public double importe;
	public String estado;

	public Factura(int id, int id_actividad, int id_profesor, Date fecha, double importe, String estado) {
		this.id = id;
		this.id_actividad = id_actividad;
		this.id_profesor = id_profesor;
		this.fecha = fecha;
		this.importe = importe;
		this.estado=estado;
	}

	public Factura(int numero, Date fecha, Double importe,String estado) {
		this.numeroFactura=numero;
		this.fecha=fecha;
		this.importe = importe;
		this.estado=estado;
	}

}
