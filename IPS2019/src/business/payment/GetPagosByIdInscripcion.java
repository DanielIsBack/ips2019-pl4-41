package business.payment;

import java.util.List;

import business.dto.PagoInscripcionDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.PaymentGateway;

public class GetPagosByIdInscripcion {

	private int id;
	private Gateway<PagoInscripcionDto> gw = PersistenceFactory.getPaymentGateway();

	public GetPagosByIdInscripcion(int id) {
		this.id = id;
	}

	public List<PagoInscripcionDto> execute() {
		return ((PaymentGateway) gw).getPagosByIdInscripcion(id);
	}
}
