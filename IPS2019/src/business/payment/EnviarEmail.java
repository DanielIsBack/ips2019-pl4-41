package business.payment;

import java.util.ArrayList;
import java.util.List;

import business.dto.PagoInscripcionDto;
import business.dto.ParticipanteDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.PaymentGateway;

public class EnviarEmail {

	private List<ParticipanteDto> clientes;
	private String mensaje;
	private Gateway<PagoInscripcionDto> gw = PersistenceFactory.getPaymentGateway();

	public EnviarEmail(List<ParticipanteDto> clientes, String mensaje) {
		this.clientes = clientes;
		this.mensaje = mensaje;
	}

	public EnviarEmail(ParticipanteDto cliente, String mensaje) {
		clientes = new ArrayList<ParticipanteDto>();
		clientes.add(cliente);
		this.mensaje = mensaje;
	}

	public void execute() {
		((PaymentGateway) gw).enviarEmail(clientes, mensaje);
	}

}
