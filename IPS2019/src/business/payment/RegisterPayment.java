package business.payment;

import business.dto.PagoInscripcionDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.PaymentGateway;

public class RegisterPayment {

	private PagoInscripcionDto p;
	private Gateway<PagoInscripcionDto> gw = PersistenceFactory.getPaymentGateway();

	public RegisterPayment(PagoInscripcionDto p) {
		this.p = p;
	}

	public String execute() {
		return ((PaymentGateway) gw).registerPayment(p);
	}
}
