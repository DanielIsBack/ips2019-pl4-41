package business.activityteacher;

import java.util.List;

import business.dto.ActivityTeacherDto;
import factory.PersistenceFactory;
import persistence.impl.ActivityTeacherGateway;

public class FindAllTeachersOfActivity {
	
	private ActivityTeacherGateway gw = (ActivityTeacherGateway) PersistenceFactory.getActivityTeacherGateway();
	private int activity_id;
	
	public FindAllTeachersOfActivity(int activity_id) {
		this.activity_id = activity_id;
	}
	
	public List<ActivityTeacherDto> execute() {
		return gw.findAllTeachersOfActivity(activity_id);
	}

}
