package business.activityteacher;

import business.dto.ActivityTeacherDto;
import exception.BusinessException;
import factory.PersistenceFactory;
import persistence.Gateway;

public class AddActivityTeacher {
	
	private ActivityTeacherDto activityTeacher;
	private Gateway<ActivityTeacherDto> gw = PersistenceFactory.getActivityTeacherGateway();

	public AddActivityTeacher(ActivityTeacherDto activityTeacher) {
		this.activityTeacher = activityTeacher;
	}
	
	public void execute() throws BusinessException {
		gw.add(activityTeacher);
	}
	
}
