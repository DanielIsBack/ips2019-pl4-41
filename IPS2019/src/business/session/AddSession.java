package business.session;

import business.dto.SesionDto;
import exception.BusinessException;
import exception.util.Business;
import factory.PersistenceFactory;
import persistence.Gateway;

public class AddSession {

	private SesionDto session;
	private Gateway<SesionDto> gw = PersistenceFactory.getSessionGateway();

	public AddSession(SesionDto session) {
		this.session = session;
	}
	
	public void execute() throws BusinessException {
		Business.isNotEmpty(session.lugar, 
				"El lugar donde se imparte la sesion no puede estar en blanco");
		Business.isNotNull(session.f_comienzo, 
				"La fecha de de inicio de la sesion no puede estar en blanco");
		Business.isNotNull(session.f_finalizacion, 
				"La fecha de finalizacion de la sesion no puede estar en blanco");
		Business.datesAreOrdered(session.f_comienzo, session.f_finalizacion, 
				"La fecha de inicio de una sesion debe ser anterior a la fecha de finalizacion");

		gw.add(session);
	}
	
}
