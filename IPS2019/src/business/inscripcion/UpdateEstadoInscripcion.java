package business.inscripcion;

import persistence.impl.InscripcionGateway;

public class UpdateEstadoInscripcion {
	private int id;
	private String estado;
	private InscripcionGateway gw = new InscripcionGateway();

	public UpdateEstadoInscripcion(int id, String estado) {
		this.id = id;
		this.estado = estado;
	}

	public void execute() {
		gw.updateEstadoInscripcion(id, estado);
	}

}
