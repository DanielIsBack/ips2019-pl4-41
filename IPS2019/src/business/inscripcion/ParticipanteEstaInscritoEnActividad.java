package business.inscripcion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import business.dto.ActivityDto;
import business.dto.ParticipanteDto;
import config.Conf;
import util.DbUtil;

public class ParticipanteEstaInscritoEnActividad {

	ParticipanteDto p;
	ActivityDto a;

	// SQL
	private String query_participante_esta_inscrito_en_actividad = Conf.getInstance()
			.getProperty("SQL_PARTICIPANTE_ESTA_INSCRITO_EN_ACTIVIDAD");

	public ParticipanteEstaInscritoEnActividad(ParticipanteDto p, ActivityDto a) {
		this.p = p;
		this.a = a;
	}

	/**
	 * 
	 * @return True si el participante ya se ha inscrito en la actividad.
	 */
	public boolean execute() {
		Connection c;
		PreparedStatement s;
		ResultSet rs;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_participante_esta_inscrito_en_actividad);

			// set properties
			s.setString(1, p.email);
			s.setInt(2, a.id);

			// execute insertion
			rs = s.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
