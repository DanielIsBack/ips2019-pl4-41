package business.inscripcion;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import business.dto.ParticipanteDto;
import factory.ServiceFactory;

public class EnviarInscripcionPorCorreoMock {

	ParticipanteDto p;
	ActivityDto a;
	double c;
	String colectivo;

	public EnviarInscripcionPorCorreoMock(ParticipanteDto p, ActivityDto a, String colectivo) {
		this.p = p;
		this.a = a;
		this.c = ServiceFactory.getCosteInscripcionService().getCosteByIdActividadAndIdParticipante(a.id,p.id);
		this.colectivo = colectivo;
	}

	public void execute() {
		
		try {
			
			String fileName = p.email + "-" + a.name;
			File file = new File("./CorreosInscripcion/" + fileName);

			if (file.createNewFile()) {
				PrintWriter out = new PrintWriter(file);
				out.println("-----------------------------------------------------");
				out.println("Nombre: " + p.nombre);
				out.println("Apellidos: " + p.apellidos);
				out.println("Actividad en la que te has inscrito: " + a.name);
				out.println("Importa a abonar: " + c + "�");
				out.println("");
				out.println(
						"Por favor, abone la cantidad solicitada en un plazo de 2 d�as o cancele su inscripci�n llamando al XXX YYY ZZZ");
				out.println("-----------------------------------------------------");
				out.close();
			}

		} catch (IOException e) {

		}

	}

}
