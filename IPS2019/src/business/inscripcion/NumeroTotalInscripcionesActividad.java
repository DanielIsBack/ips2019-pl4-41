package business.inscripcion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import config.Conf;
import util.DbUtil;

public class NumeroTotalInscripcionesActividad {

	int id_act;

	// SQL
	private String query_count_inscripciones = Conf.getInstance().getProperty("SQL_COUNT_INSCRIPCIONES_ACTIVIDAD");

	public NumeroTotalInscripcionesActividad(int id_act) {
		this.id_act = id_act;
	}

	public int execute() {
		Connection c;
		PreparedStatement s;
		ResultSet rs;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_count_inscripciones);

			// set properties
			s.setInt(1, id_act);

			// execute insertion
			rs = s.executeQuery();
			rs.next();
			return rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -10000;
	}

}
