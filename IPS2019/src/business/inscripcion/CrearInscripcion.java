package business.inscripcion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import business.dto.ActivityDto;
import business.dto.ParticipanteDto;
import config.Conf;
import util.DbUtil;

public class CrearInscripcion {

	ParticipanteDto p;
	ActivityDto a;
	String colectivo;
	
	// SQL
	private String query_crear_inscripcion = Conf.getInstance().getProperty("SQL_CREAR_INSCRIPCION");
	private String query_crear_inscripcion_sin_colectivo = Conf.getInstance().getProperty("SQL_CREAR_INSCRIPCION_SIN_COLECTIVO");

	public CrearInscripcion(ParticipanteDto p, ActivityDto a, String colectivo) {
		this.p = p;
		this.a = a;
		this.colectivo = colectivo;
	}

	public void execute() {
		Connection c;
		PreparedStatement s;

		try {
			c = DbUtil.getConnection();
			if(colectivo != null) {
				s = c.prepareStatement(query_crear_inscripcion);
			}else {
				s = c.prepareStatement(query_crear_inscripcion_sin_colectivo);
			}
			

			// set properties
			s.setInt(1, a.id);
			s.setInt(2, p.id);
			s.setDate(3, new Date(System.currentTimeMillis()));
			s.setString(4, colectivo);

			// execute insertion
			s.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
