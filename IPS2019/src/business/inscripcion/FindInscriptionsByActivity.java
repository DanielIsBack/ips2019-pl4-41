package business.inscripcion;

import java.util.List;

import business.dto.InscripcionDto;
import persistence.impl.InscripcionGateway;

public class FindInscriptionsByActivity {

	private int activityId;
	private InscripcionGateway gw = new InscripcionGateway();

	public FindInscriptionsByActivity(int activityId) {
		this.activityId = activityId;
	}

	public List<InscripcionDto> execute() {
		return gw.findInscriptionsByActivity(activityId);
	}

}
