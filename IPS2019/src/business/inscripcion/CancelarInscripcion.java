package business.inscripcion;

import business.dto.InscripcionDto;
import persistence.impl.InscripcionGateway;

public class CancelarInscripcion {
	InscripcionDto inscripcion;
	private InscripcionGateway gw = new InscripcionGateway();

	public CancelarInscripcion(InscripcionDto inscripcion) {
		this.inscripcion = inscripcion;
	}

	public void execute() {
		gw.cancelarInscripcion(inscripcion);
	}
}
