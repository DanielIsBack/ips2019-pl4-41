package business.inscripcion;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.ParticipanteDto;
import factory.ServiceFactory;

public class AvisarInscritoDeCancelaciónActividad {

	InscripcionDto ins;
	ParticipanteDto p;
	ActivityDto a;

	public AvisarInscritoDeCancelaciónActividad(InscripcionDto ins) {
		this.ins = ins;
		p = ServiceFactory.getParticipanteCrudService().getParticipanteById(ins.id_part);
		a = ServiceFactory.getActivityService().findActivityById(ins.id_act);
	}

	public void execute() {
		
		try {
			
			String fileName = p.email + "-" + a.name+"-CANCELACIÓN";
			File file = new File("./CorreosCancelaciónInscripcion/" + fileName);			
			
			if (file.createNewFile()) {
				PrintWriter out = new PrintWriter(file);
				out.println("-----------------------------------------------------");
				out.println("Estimado/a " + p.nombre +" "+ p.apellidos+":");
				out.println("Lamentamos informarte de que la actividad en la que te has inscrito recientemente (" + a.name+") ha sido CANCELADA.");
				out.println("Si la actividad estaba en curso, no habrá más sesiones. El importe de la inscripción se te será devuelto próximamente");
				out.println("Pedimos disculpas por este inconveniente");
				out.println("-----------------------------------------------------");
				out.close();
			}

		} catch (IOException e) {

		}

	}

}
