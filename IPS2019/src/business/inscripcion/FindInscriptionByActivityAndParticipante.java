package business.inscripcion;

import business.dto.InscripcionDto;
import persistence.impl.InscripcionGateway;

public class FindInscriptionByActivityAndParticipante {

	private InscripcionGateway gw = new InscripcionGateway();
	private int idActividad;
	private int idParticipante;
	
	public FindInscriptionByActivityAndParticipante(int idActividad, int idParticipante) {
		this.idActividad = idActividad;
		this.idParticipante = idParticipante;
	}

	public InscripcionDto execute() {
		return gw.findInscriptionsByActivityAndParticipante(idActividad, idParticipante);
	}
}
