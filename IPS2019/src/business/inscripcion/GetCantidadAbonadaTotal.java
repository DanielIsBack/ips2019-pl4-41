package business.inscripcion;

import persistence.impl.InscripcionGateway;

public class GetCantidadAbonadaTotal {
	private int id;
	private InscripcionGateway gw = new InscripcionGateway();

	public GetCantidadAbonadaTotal(int id) {
		this.id = id;
	}

	public double execute() {
		return gw.getCantidadAbonadaTotal(id);
	}

}
