package business.inscripcion;

import business.dto.InscripcionDto;
import persistence.impl.InscripcionGateway;

public class FindInscriptionsById {

	private int id;
	private InscripcionGateway gw = new InscripcionGateway();

	public FindInscriptionsById(int id) {
		this.id = id;
	}

	public InscripcionDto execute() {
		return gw.findInscriptionsById(id);
	}

}
