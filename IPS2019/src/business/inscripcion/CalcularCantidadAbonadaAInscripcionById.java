package business.inscripcion;

import persistence.impl.InscripcionGateway;

public class CalcularCantidadAbonadaAInscripcionById {

	int id;
	private InscripcionGateway gw = new InscripcionGateway();

	public CalcularCantidadAbonadaAInscripcionById(int id) {
		this.id = id;
	}

	public double execute() {
		return gw.calcularCantidadAbonadaAInscripcionById(id);
	}
}
