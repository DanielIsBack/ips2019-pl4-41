package business.inscripcion;

import java.util.List;

import business.dto.InscripcionDto;
import persistence.impl.InscripcionGateway;

public class FindNonPaidInscriptionsByActivity {

	private int activityId;
	private InscripcionGateway gw = new InscripcionGateway();

	public FindNonPaidInscriptionsByActivity(int activityId) {
		this.activityId = activityId;
	}

	public List<InscripcionDto> execute() {
		return gw.findNonPaidInscriptionsByActivity(activityId);
	}

}
