package business;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;

import business.dto.InscripcionDto;

public class Database {

	static Connection con;
	long numeroFactura = 0000;

	private static Connection getConnection() throws SQLException, IOException {
		Properties prop = new Properties();
		FileInputStream file = new FileInputStream("./src/config.properties");
		prop.load(file);
		String URL = prop.getProperty("URL");
		String USER = prop.getProperty("USER");
		String PASS = prop.getProperty("PASS");
		return DriverManager.getConnection(URL, USER, PASS);
	}

	// OBSOLETO - actualizar sql
	public static LinkedList<Client> getClients() {

		String query = "SELECT * FROM participante";
		LinkedList<Client> clients = new LinkedList<Client>();

		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			while (rs.next()) {
				String email = rs.getString("email");
				String name = rs.getString("Nombre");
				String surname = rs.getString("Apellidos");

				clients.add(new Client(name, surname, email));

			}
			return clients;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// OBSOLETO - actualizar sql
	public static void addClient(Client c) {
		String email = c.getEmail();
		String name = c.getName();
		String surname = c.getSurname();

		String query = "INSERT INTO participante(email,nombre,apellidos) VALUES ('" + email + "','" + name + "','"
				+ surname + "');";

		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			stmnt.executeUpdate(query);

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public static LinkedList<Actividad> getActividades() {
		String query = "SELECT * FROM actividad";
		LinkedList<Actividad> actividades = new LinkedList<Actividad>();

		try {
			if (con == null)
				con = getConnection();

			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			while (rs.next()) {
				int id_act = rs.getInt("id");
				String nombre = rs.getString("nombre");
				int plazas = rs.getInt("plazas");
				String objetivos = clobToString(rs.getClob("objetivos"));
				String contenidos = clobToString(rs.getClob("contenidos"));
				int id_prof = rs.getInt("id_profesor");
				double remuneracion = rs.getDouble("remuneracion");
				Date f_apertura = rs.getDate("f_apertura");
				Date f_cierre = rs.getDate("f_cierre");
				Date f_celebracion = rs.getDate("f_celebracion");
				String lugar = rs.getString("lugar");
				double coste_ins = rs.getDouble("coste_ins");
				String estado = rs.getString("estado");

				actividades.add(new Actividad(id_act, nombre, plazas, objetivos, contenidos, id_prof, remuneracion,
						f_apertura, f_cierre, f_celebracion, lugar, coste_ins, estado));

			}
			return actividades;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean inscribirUsuario(int act_id, String dni) {
		String query = "INSERT INTO inscripcion (act_id, dni) VALUES (?, ?);";

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, act_id);
			stmnt.setString(2, dni);
			stmnt.executeUpdate(query);

			return true;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	public static LinkedList<Actividad> getActividadesDisponibles() {
		String query = "SELECT nombre, plazas, objetivos, contenidos, id_profesor, f_celebracion, lugar, coste_ins"
				+ " FROM actividad a, inscripcion i\n" + "WHERE i.id_act = a.id\n"
				+ "AND a.estado = 'Periodo_Inscripcion'\n" + "AND a.plazas >= (SELECT COUNT(*)\n"
				+ "	FROM inscripcion ins, actividad act\n" + "	WHERE ins.id_act = act.id\n"
				+ "	AND act.id = a.id); ";

		LinkedList<Actividad> actividadesDisponibles = new LinkedList<Actividad>();

		try {
			if (con == null)
				con = getConnection();

			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			while (rs.next()) {
				String nombre = rs.getString("nombre");
				int plazas = rs.getInt("plazas");
				String objetivos = clobToString(rs.getClob("objetivos"));
				String contenidos = clobToString(rs.getClob("contenidos"));
				int id_prof = rs.getInt("id_profesor");
				Date f_celebracion = rs.getDate("f_celebracion");
				String lugar = rs.getString("lugar");
				double coste_ins = rs.getDouble("coste_ins");

				actividadesDisponibles.add(
						new Actividad(nombre, plazas, objetivos, contenidos, id_prof, f_celebracion, lugar, coste_ins));

			}
			return actividadesDisponibles;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		} catch (NullPointerException e) {

		}
		return sb.toString();
	}

	public static int getNumeroDeInscripcionesDeUnaActividadQueNoEstenCanceladas(String nombre) {
		String query = "select count(i.id) from inscripcion i, actividad a where a.NOMBRE = ? and a.id = i.ID_ACT and (i.estado_pago in ('Sin_pagar', 'Correcto', 'Incorrecto_Menos', 'Incorrecto_Mas'))";

		int numero = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setString(1, nombre);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				numero = rs.getInt("c1");
			}
			return numero;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static double getIngresosConfirmadosDeUnaActividad(String nombre) {
		String query = "select sum(pi.cantidad_abonada) from inscripcion i, pagosInscripcion pi, actividad a where a.id = i.id_act and a.nombre = ? and pi.id_ins=i.id and i.estado_pago!='Cancelada_Devuelta' and i.estado_pago!='Cancelada_Sin_Devolver'" ;
		int numero = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setString(1, nombre);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				numero = rs.getInt(1);

			}
			return numero;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return 0.0;
	}

	public static LinkedList<Actividad> getActividadesPendientesDePagarProfesor() {

		Date dateActual = new Date(System.currentTimeMillis());
		java.sql.Date dateActualSQL = new java.sql.Date(dateActual.getTime());
        //Meter f_celebracion en la query para que salgan solo las actividades ya terminadas, sacarlo de la ultima sesion
		
		//String query = "select * from actividad where estado!='Cancelada'AND estado!='Cerrada' AND id not in(select id_actividad from pagosprofesor where estado='Correcto')";
		String query = "select * from actividad a where a.id in (select id_act from sesion s where id_act in (select id from actividad where estado='Inscripci�n Cerrada') and s.f_finalizacion < ?)";

		
		LinkedList<Actividad> actividades = new LinkedList<Actividad>();

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement st = con.prepareStatement(query);
			st.setDate(1, dateActualSQL);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				int id_act = rs.getInt("id");
				String nombre = rs.getString("nombre");
				int plazas = rs.getInt("plazas");
				Clob clob = rs.getClob("objetivos");
				String objetivos = null;
				if (clob != null) {
					objetivos = clobToString(clob);
				}
				clob = rs.getClob("contenidos");
				String contenidos = null;
				if (clob != null) {
					contenidos = clobToString(clob);
				}

				//int id_prof = rs.getInt("id_profesor");
				//double remuneracion = rs.getDouble("remuneracion");
				Date f_apertura = rs.getDate("f_apertura_ins");
				Date f_cierre = rs.getDate("f_cierre_ins");
				//Date f_celebracion = rs.getDate("f_celebracion");
				//String lugar = rs.getString("lugar");
				//double coste_ins = rs.getDouble("coste_ins");
				String estado = rs.getString("estado");

				actividades.add(new Actividad(id_act, nombre, plazas, objetivos, contenidos,
						f_apertura, f_cierre, estado));

			}
			return actividades;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static LinkedList<Profesor> getProfesoresSegunActividad(String nombreActividad) {
		String query = "select * from profesor where id in(select id_profesor from actividadprofesor where id_act=(select id from actividad where nombre=?))";
		LinkedList<Profesor> profesores = new LinkedList<Profesor>();

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setString(1, nombreActividad);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String dni = rs.getString("dni");
				String apellidos = rs.getString("apellidos");

				profesores.add(new Profesor(id, nombre, dni, apellidos));
			}
			return profesores;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

//	public static double getImporteSegunActividad(String nombreActividad) {
//		String query = "SELECT remuneracion FROM actividadProfesor where id_profesor=(select distinct id_profesor from actividad where nombre=?)";
//
//		double remuneracion = 0;
//		try {
//			if (con == null)
//				con = getConnection();
//
//			PreparedStatement stmnt = con.prepareStatement(query);
//			stmnt.setString(1, nombreActividad);
//			ResultSet rs = stmnt.executeQuery();
//
//			while (rs.next()) {
//				remuneracion = rs.getDouble("remuneracion");
//
//			}
//			return remuneracion;
//
//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//		}
//		return 0.0;
//	}

	public static void generarFactura(String nombreActividad) { // CAMBIAR PARAMETROS SEGUN LO Q SE NECESITE
		String ruta = "Facturas/" + nombreActividad + ".txt";
		File archivo = new File(ruta);
		String mensaje = "Factura de la actividad: " + nombreActividad + "/n";

		BufferedWriter bw;
		try {
			if (archivo.exists()) {
				bw = new BufferedWriter(new FileWriter(archivo));
				bw.write(mensaje);
			} else {
				archivo.createNewFile();
				bw = new BufferedWriter(new FileWriter(archivo));
				bw.write(mensaje);
			}
			bw.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public static LinkedList<InscripcionDto> getInscripcionesByNombre(String nombre) {
		String query = "SELECT * FROM inscripcion where id_act=(select id from actividad where nombre=?)";
		LinkedList<InscripcionDto> inscripciones = new LinkedList<InscripcionDto>();

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setString(1, nombre);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				int id_act = rs.getInt("id_act");
				int id_part = rs.getInt("id_part");
				Date f_insc = rs.getDate("f_insc");
				String estado_pago = rs.getString("estado_pago");

				InscripcionDto i = new InscripcionDto();
				i.estado_pago = estado_pago;
				i.id = id;
				i.id_act = id_act;
				i.id_part = id_part;
				i.f_insc = new java.sql.Date(f_insc.getTime());
				i.estado_pago = estado_pago;

				inscripciones.add(i);

			}
			return inscripciones;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Client getClienteById(int id_participante) {

		String query = "SELECT * FROM participante where id=?";

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id_participante);
			ResultSet rs = stmnt.executeQuery();
			int id = 0;
			String email = null;
			String name = null;
			String surname = null;

			while (rs.next()) {
				id = rs.getInt("id");
				email = rs.getString("Email");
				name = rs.getString("Nombre");
				surname = rs.getString("Apellidos");

			}

			Client cliente = new Client(id, name, surname, email);
			return cliente;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Inscripcion getInscripciones(Actividad a, Client c) {
		String query = "SELECT * FROM inscripcion where id_act = " + a.getAct_id() + " and id_part = " + c.getId();
		Inscripcion inscripcion = null;

		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");
				int id_actividad = rs.getInt("id_act");
				int id_participante = rs.getInt("id_part");
				String estado = rs.getString("estado_pago");
				Date fecha_insc = rs.getDate("f_insc");

				inscripcion = new Inscripcion(id, id_actividad, id_participante, estado, fecha_insc);

			}
			return inscripcion;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static boolean comprobarFecha(InscripcionDto i, Date fecha, String tipo) {

		Date fechaInscripcion = i.f_insc;
		Date fechaVencimiento = calcularFechaVencimientoPlazo(fechaInscripcion);

		if (fecha.after(fechaVencimiento) && tipo.equals("Pago")) {
			return false;
		} else if (fecha.before(fechaInscripcion)) {
			throw new IllegalArgumentException("No se puede realizar un pago en una fecha anterior a la inscripcion");
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	private static Date calcularFechaVencimientoPlazo(Date fechaInscripcion) {
		int dia = fechaInscripcion.getDate();
		int mes = fechaInscripcion.getMonth();
		int a�o = fechaInscripcion.getYear();

		int nuevoDia = dia;
		int nuevoMes = mes;
		int nuevoA�o = a�o;

		if (mes == 1 && dia >= 27) {
			if (a�o % 4 == 0) {
				if ((dia + 2) / 29 > 0) {
					nuevoDia = (dia + 2) % 29;
					nuevoMes = (mes + 1);
				} else {
					nuevoDia = (dia + 2) % 29;
				}
			} else {
				if ((dia + 2) / 28 > 0) {
					nuevoDia = (dia + 2) % 28;
					nuevoMes = mes + 1;
				} else {
					nuevoDia = (dia + 2) % 28;
				}
			}
		} else if ((mes == 0 || mes == 2 || mes == 4 || mes == 6 || mes == 7 || mes == 9 || mes == 11)) {
			if ((dia + 2) / 31 > 0) {
				nuevoDia = ((dia + 2) % 31);
				if (mes == 11) {
					nuevoMes = (0);
					nuevoA�o = (a�o + 1);
				} else {
					nuevoMes = (mes + 1);
				}
			} else {
				nuevoDia = (dia + 2) % 31;
			}
		} else {
			if ((dia + 2) / 30 > 0) {
				nuevoDia = (dia + 2) % 30;
				nuevoMes = (mes + 1);
			} else {
				nuevoDia = ((dia + 2) % 30);
			}
		}

		return new Date(nuevoA�o, nuevoMes, nuevoDia);
	}

	public static Client getCliente(InscripcionDto i) {
		String query = "SELECT * FROM participante where id = " + i.id_part;
		Client cliente = null;

		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			rs.next();
			String nombre = rs.getString("Nombre");
			String apellidos = rs.getString("apellidos");
			String email = rs.getString("Email");

			cliente = new Client(i.id_part, nombre, apellidos, email);

			return cliente;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static LinkedList<Inscripcion> getInscripciones(Actividad a) {
		String query = "SELECT * FROM inscripcion where id_act = " + a.id_act;
		LinkedList<Inscripcion> inscripciones = new LinkedList<Inscripcion>();

		try {
			con = getConnection();
			Statement stmnt = con.createStatement();
			ResultSet rs = stmnt.executeQuery(query);

			while (rs.next()) {
				int id = rs.getInt("id");
				int id_actividad = rs.getInt("id_act");
				int id_participante = rs.getInt("id_part");
				String estado = rs.getString("estado_pago");
				Date fecha_inscripcion = rs.getDate("f_insc");

				inscripciones.add(new Inscripcion(id, id_actividad, id_participante, estado, fecha_inscripcion));

			}
			return inscripciones;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getNombreActividadDeLaAinscripcionPasadaComoParametro(int id) {
		String query = "select nombre from actividad where id=?";
		PreparedStatement pst;
		try {
			con = getConnection();

			pst = con.prepareStatement(query);
			pst.setDouble(1, id);
			ResultSet rs = pst.executeQuery();
			rs.next();
			return rs.getString(1);

		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";

	}

	public static void generarTransferencia(Actividad actividad, String cantidad, String profesor) {
		// long numero=(int) (Math.random() * 1000000000) + 1;

		Date dateActual = new java.util.Date(System.currentTimeMillis());
		java.sql.Date dateActualSQL = new java.sql.Date(dateActual.getTime());

		String ruta = "Transferencias/" + dateActualSQL.toString() + profesor + ".txt";
		String mensaje = "Transferencia: " + "\n" + "De: COIIPA , a: " + profesor + "\n"
				+ "Concepto: Pago al profesor por actividad(" + actividad.nombre + ")\n" + "Importe: " + cantidad
				+ "�\n" + "Fecha: " + dateActualSQL.toString();
		File archivo = new File(ruta);
		BufferedWriter bw;
		try {
			if (archivo.exists()) {
				bw = new BufferedWriter(new FileWriter(archivo));
				bw.write(mensaje);
			} else {
				archivo.createNewFile();
				bw = new BufferedWriter(new FileWriter(archivo));
				bw.write(mensaje);
			}
			bw.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	public static double getDineroAbonadoAunProfesorPorUnaActividad(Profesor profesor, Actividad actividad) {
		String query = "select sum(cantidad_abonada) from pagosProfesor where id_actpro=(select id from actividadprofesor where id_act=? and id_profesor=?)";
		PreparedStatement pst;
		try {
			con = getConnection();

			pst = con.prepareStatement(query);
			pst.setInt(1, actividad.id_act);
			pst.setInt(2, profesor.id);

			ResultSet rs = pst.executeQuery();
			rs.next();
			return rs.getDouble(1);

		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0.0;
	}

	public static void pagarProfesor(Actividad actividad, Profesor p, Double cantidadAabonar, String estado,Date fecha) {

		//Date dateActual = new java.util.Date(System.currentTimeMillis());
		java.sql.Date dateSQL = new java.sql.Date(fecha.getTime());
		// int id_Profesor =actividad.getId_profesor();

		// String query = "update inscripcion set estado_pago = ?, cantidad_abonada =
		// cantidad_abonada + ? where id_act = ? and id_part = ?";

		String query = "INSERT INTO pagosProfesor(id_actpro,estado_pago,f_pago,cantidad_abonada) VALUES (?,?,?,?)";

        String query2="select id from actividadProfesor where id_act=? and id_profesor=?";
		
		int actpro = 0;
		
		try {

			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query2);
			
			stmnt.setInt(1, actividad.getAct_id());
			stmnt.setInt(2, p.id);
			
			ResultSet rs = stmnt.executeQuery();
			while(rs.next()) {
				actpro= rs.getInt(1);
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		try {
			con = getConnection();

			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, actpro);
			pst.setString(2, estado);
			pst.setDate(3, dateSQL);
			pst.setDouble(4, cantidadAabonar);

			pst.executeUpdate();

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

	}

//	public static void generarFacturasProfesores() {
//		LinkedList<Actividad> actividades = getActividadesPendientesDePagarProfesor();
//		Date dateActual = new Date(System.currentTimeMillis());
//		java.sql.Date dateActualSQL = new java.sql.Date(dateActual.getTime());
//
//		String query = "INSERT INTO factura(id_actividad,id_profesor,fecha,importeTotal) VALUES (?,?,?,?)";
//		for (Actividad a : actividades) {
//			try {
//
//				if (getFactura(a.id_act) == null) {
//
//					if (con == null)
//						con = getConnection();
//
//					PreparedStatement stmnt = con.prepareStatement(query);
//					stmnt.setInt(1, a.id_act);
//					stmnt.setInt(2, a.id_profesor);
//					stmnt.setDate(3, dateActualSQL);
//					stmnt.setDouble(4, a.getRemuneracion());
//					stmnt.executeUpdate();
//				}
//			} catch (SQLException | IOException e) {
//				e.printStackTrace();
//			}
//
//			Factura f = getFactura(a.id_act);
//
//			String ruta = "Facturas/" + f.id + a.nombre + ".txt";
//			File archivo = new File(ruta);
//			String mensaje = "Factura n�mero: " + f.id + "\n" + "Descripci�n: Actividad impartida (" + a.nombre + ")\n"
//					+ "De: " + getProfesorSegunActividad(a.nombre).nombre + " "
//					+ getProfesorSegunActividad(a.nombre).apellidos + "    Para: COIIPA\n" + "Importe total a pagar: "
//					+ a.remuneracion + "\n" + "Fecha: " + dateActualSQL;
//			;
//
//			BufferedWriter bw = null;
//			try {
//				if (archivo.exists()) {
//					// No hace nada, la factura ya esta creada
//				} else {
//					archivo.createNewFile();
//					bw = new BufferedWriter(new FileWriter(archivo));
//					bw.write(mensaje);
//				}
//				if (bw != null) {
//					bw.close();
//				}
//			} catch (IOException e) {
//				System.err.println(e.getMessage());
//				e.printStackTrace();
//			}
//
//		}
//
//	}

	public static void generarFacturaProfesor(int act, int pro, Date date, Double importeTotal, int numero, String estado) {

		java.sql.Date dateSQL = new java.sql.Date(date.getTime());

		String query = "INSERT INTO factura(id_actpro,numero_factura,fecha,importe_Total,estado) VALUES (?,?,?,?,?)";

		String query2="select id from actividadProfesor where id_act=? and id_profesor=?";
		
		int actpro = 0;
		
		try {

			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query2);
			
			stmnt.setInt(1, act);
			stmnt.setInt(2, pro);
			ResultSet rs = stmnt.executeQuery();
			while(rs.next()) {
				actpro= rs.getInt(1);
				//System.out.println(actpro);
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

		
		try {

			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			
			stmnt.setInt(1, actpro);
			stmnt.setInt(2, numero);
			stmnt.setDate(3, dateSQL);
			stmnt.setDouble(4, importeTotal);
			stmnt.setString(5, estado);

			stmnt.executeUpdate();

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

	}

	public static LinkedList<Factura> getFactura(int id_actividad, int id_profesor) {

		//String query = "SELECT * FROM factura where id_act=?";
		String query = "SELECT * FROM factura where id_actpro=(select id from actividadprofesor where id_act=? and id_profesor=?)";
		
		LinkedList<Factura> facturas = new LinkedList<Factura>();

		try {
			con = getConnection();
			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id_actividad);
			stmnt.setInt(2, id_profesor);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				int numero = rs.getInt("numero_factura");
				//System.out.print(numero);
				//int actividad = rs.getInt("id_act");
				//int profesor = rs.getInt("profesor");
				Date fecha = rs.getDate("fecha");
				Double importe = rs.getDouble("importe_Total");
				String estado = rs.getString("estado");
				
				facturas.add(new Factura(numero, fecha, importe,estado));

			}
			return facturas;
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static LinkedList<PagoProfesor> getPagos(int actividad,int id_profesor) {

		String query = "SELECT * FROM pagosprofesor where id_actpro=(select id from actividadprofesor where id_act=? and id_profesor=?)";
		LinkedList<PagoProfesor> pagos = new LinkedList<PagoProfesor>();

		try {
			con = getConnection();
			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, actividad);
			stmnt.setInt(2, id_profesor);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				//int id = rs.getInt("id");
				//int id_actividad = rs.getInt("id_actividad");
				//int profesor = rs.getInt("id_profesor");
				String estado = rs.getString("estado_pago");
				Date fecha = rs.getDate("f_pago");
				Double cantidad = rs.getDouble("cantidad_abonada");

				pagos.add(new PagoProfesor(fecha, cantidad, estado));

			}
			return pagos;
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Double getRemuneracionTotalByIdActividad(int id) {
		String query = "select sum(ap.remuneracion) from actividadProfesor ap where ap.id_act = ?";
		String query2 = "select sum(ae.remuneracion) from actividadEmpresa ae where ae.id_act = ?";
		double numero = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				numero = rs.getInt("c1");
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt2 = con.prepareStatement(query2);
			stmnt2.setInt(1, id);
			ResultSet rs2 = stmnt2.executeQuery();

			while (rs2.next()) {
				numero += rs2.getInt("c1");
			}
			
			return numero;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		return 0.0;
	}

	public static Double getRemuneracionTotalConfirmadaByIdActividad(int id) {
		String query = "select sum(cantidad_abonada) from pagosProfesor where id_actpro in (select id from actividadProfesor where id_act=?)";
		//String query2 = "select sum(cantidad_abonada) from pagosEmpresa where id_actemp in (select id from actividadEmpresa where id_act=?)";
		double numero = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				return numero = rs.getInt("c1");
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
//		try {
//			if (con == null)
//				con = getConnection();
//
//			PreparedStatement stmnt2 = con.prepareStatement(query2);
//			stmnt2.setInt(1, id);
//			ResultSet rs2 = stmnt2.executeQuery();
//
//			while (rs2.next()) {
//				numero += rs2.getInt("c1");
//			}
//			
			//return numero;

//		} catch (SQLException | IOException e) {
//			e.printStackTrace();
//		}
		
		return 0.0;
	}

	public static double getImporteSegunActividadProfesor(Actividad actividad,
			Profesor profesor) {
		
		String query = "SELECT remuneracion FROM actividadProfesor where id_act=? and id_profesor=?";

		double remuneracion = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, actividad.id_act);
			stmnt.setInt(2, profesor.id);
			
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				remuneracion = rs.getDouble("remuneracion");

			}
			return remuneracion;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return 0.0;
		
		
	}

	public static int getPlazasOcupadasSegunActividad(int id) {

		String query = "SELECT count(*) FROM inscripcion where id_act=? and (estado_pago!='Cancelada'AND estado_pago!='Cancelada_Devuelta' AND estado_pago!='Cancelada_Sin_Devolver')";

		int plazasOcupadas = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id);
			
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				plazasOcupadas = rs.getInt("c1");

			}
			return plazasOcupadas;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return 0;
		
		
	}

	public static void retrasarTodasLasSesionesDeUnaActividad(int value, String actividad) {
		
		int dias=value*7;
		//Date date=0000-00-00;
		
		String query1= "select id from actividad where nombre=?";
		//String query2="update sesion set f_comienzo=(f_comienzo+?), f_finalizacion= (f_finalizacion+?) where id_act=? ";
		String query2="update sesion set f_comienzo=dateadd(day,?,f_comienzo), f_finalizacion=dateadd(day,?,f_finalizacion) where id_act=? ";
		int id = 0;
		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query1);
			stmnt.setString(1, actividad);
			
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
			    id = rs.getInt("id");
			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		try {
			con = getConnection();

			PreparedStatement pst = con.prepareStatement(query2);
			
			pst.setInt(1, dias);
			pst.setInt(2, dias);
			pst.setInt(3, id);

			pst.executeUpdate();

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}

	
		
	}

	public static void mandarEmailsActividadRetrasada(int value, String actividad) {
		
		String query="select email from participante where id in(select id_part from inscripcion where id_act=(select id from actividad where nombre=?))";

		LinkedList<String> participantesAfectados = new LinkedList<String>();

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setString(1, actividad);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				String email = rs.getString("email");

				participantesAfectados.add(email
						);

			}

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
	
		int numero=(int)(Math.random()*1000+1);
	    System.out.println(numero);
		for(String s : participantesAfectados) {
		String fileName = s + "-" + actividad+"-"+numero;
		File file = new File("./CorreosActividadesRetrasadas/" + fileName);
	
		
		try {
			if (file.createNewFile()) {
				PrintWriter out = new PrintWriter(file);
				out.println("-----------------------------------------------------");
				out.println("La actividad "+ actividad +" en la que estas inscrito ha sido retrasada "+value+ " semanas");
				out.println(
						"Si desea cancelar su inscripci�n puede hacerlo llamando al XXX YYY ZZZ.");
				out.println(
						"Disculpe las  molestias..");
				
				out.println("-----------------------------------------------------");
				out.close();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}



}

	public static Date getInicioActividad(int id) {
		String query = "SELECT min(f_comienzo) FROM sesion where id_act=(select id from actividad where id=?)";
		Date fecha = null;

		try {
			if (con == null)
				con = getConnection();

			PreparedStatement stmnt = con.prepareStatement(query);
			stmnt.setInt(1, id);
			ResultSet rs = stmnt.executeQuery();

			while (rs.next()) {
				fecha = rs.getDate(1);

			}
			return fecha;

		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}
		
	


}
