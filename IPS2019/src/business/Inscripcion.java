package business;

import java.util.Date;

public class Inscripcion {

	int id;
	int id_actividad;
	int id_participante;
	String estado;
	double dinero_pagado;
	Date fecha_inscripcion;
	String estado_pago;

	public Inscripcion(int id, int id_actividad, int id_participante, Date fecha_inscripcion, double dinero_pagado,
			String estado, String estado_pago) {
		this.id = id;
		this.id_actividad = id_actividad;
		this.id_participante = id_participante;
		this.estado = estado;
		this.dinero_pagado = dinero_pagado;
		this.fecha_inscripcion = fecha_inscripcion;
		this.estado_pago = estado_pago;
	}

	public Inscripcion(int id, int id_actividad, int id_participante, String estado, Date fecha_inscripcion) {
		this.id = id;
		this.id_actividad = id_actividad;
		this.id_participante = id_participante;
		this.estado = estado;
		dinero_pagado = 0;
		this.fecha_inscripcion = fecha_inscripcion;
	}

	public Inscripcion(int id, int id_actividad, int id_participante, Date fecha_inscripcion, double dinero_pagado,
			String estado_pago) {
		this.id = id;
		this.id_actividad = id_actividad;
		this.id_participante = id_participante;
		this.dinero_pagado = dinero_pagado;
		this.fecha_inscripcion = fecha_inscripcion;
		this.estado_pago = estado_pago;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_actividad() {
		return id_actividad;
	}

	public void setId_actividad(int id_actividad) {
		this.id_actividad = id_actividad;
	}

	public int getId_participante() {
		return id_participante;
	}

	public void setId_participante(int id_participante) {
		this.id_participante = id_participante;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstado_pago() {
		return estado_pago;
	}

	public void setEstado_pago(String estado_pago) {
		this.estado_pago = estado_pago;
	}

	public String toString() {
		String cadena = "";
		return cadena;
	}

	public void setDineroPagado(double dinero_pagado) {
		this.dinero_pagado = dinero_pagado;
	}

	public double getDinero_pagado() {
		return dinero_pagado;
	}

	public void setDinero_pagado(double dinero_pagado) {
		this.dinero_pagado = dinero_pagado;
	}

	public Date getFecha_inscripcion() {
		return fecha_inscripcion;
	}

	public void setFecha_inscripcion(Date fecha_inscripcion) {
		this.fecha_inscripcion = fecha_inscripcion;
	}

}
