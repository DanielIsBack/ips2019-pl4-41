package business.dto;

import java.util.Date;

public class SesionDto {
	public int id;
	public int id_act;
	
	//public Date fecha;
	//public Time h_comienzo; 
	//public Time h_finalizacion;
	
	public String lugar;
	
	//nuevo sprint
	public Date f_comienzo;
	public Date f_finalizacion;
	
	@Override
	public String toString() {
		return "SesionDto [id=" + id + ", id_act=" + id_act + ", lugar=" + lugar + ", f_comienzo=" + f_comienzo
				+ ", f_finalizacion=" + f_finalizacion + "]";
	}
	
}
