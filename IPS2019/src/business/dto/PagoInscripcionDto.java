package business.dto;

import java.sql.Date;

public class PagoInscripcionDto {

	public Date fecha;
	public int id_insc;
	public double cantidad;
	public String tipo;
	public int id;

}
