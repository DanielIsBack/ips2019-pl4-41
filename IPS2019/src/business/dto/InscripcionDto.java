package business.dto;

import java.sql.Date;

public class InscripcionDto {

	public int id;
	public int id_act;
	public int id_part;
	public Date f_insc;
	public String estado_pago;
	public String colectivo;
}
