package business.dto;

import java.sql.Date;

public class Factura {
	public int id;
	public int id_act;
	public int id_prof;
	public Date fecha;
	public double importe;
}
