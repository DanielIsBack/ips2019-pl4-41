package business.dto;

import java.util.Date;

public class ActivityDto {

	public int id;
	public String name;
	public String objectives;
	public String contents;
	//public int teacher;
	//public double remuneration;
	//public Date curseDate;
	//public Time sHour;
	//public Time eHour;
	//public String space;
	public Date startInscriptionDate;
	public Date endInscriptionDate;
	public int numOfPlaces;
	public int freePlaces;
	public String state;
	public int numinsc;

	// related to incomes and expenses
	public double expense;
	public double income;
	public double balance;
	public double estimatedExpense;
	public double estimatedIncome;
	public double estimatedBalance;

}
