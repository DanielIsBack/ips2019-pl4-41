package business;

public class Profesor {

	int id;
	String nombre;
	String dni;
	String apellidos;

	public Profesor(int id, String nombre, String dni, String apellidos) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.dni = dni;
		this.apellidos = apellidos;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String toString() {
		String cadena = nombre+" "+ apellidos;
		return cadena;
	}

}
