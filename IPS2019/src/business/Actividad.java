package business;

import java.util.Date;

public class Actividad {
	int id_act;
	String nombre;
	int plazas;
	String objetivos;
	String contenidos;
	int id_profesor;
	double remuneracion;
	Date f_apertura;
	Date f_cierre;
	Date f_celebracion;
	String lugar;
	double coste_inscripcion;
	String estado;

	public Actividad(int id_act, String nombre, int plazas, String objetivos, String contenidos, int id_profesor,
			double remuneracion, Date f_apertura, Date f_cierre, Date f_celebracion, String lugar,
			double coste_inscripcion, String estado) {
		this.id_act = id_act;
		this.nombre = nombre;
		this.plazas = plazas;
		this.objetivos = objetivos;
		this.contenidos = contenidos;
		this.id_profesor = id_profesor;
		this.remuneracion = remuneracion;
		this.f_apertura = f_apertura;
		this.f_cierre = f_cierre;
		this.f_celebracion = f_celebracion;
		this.lugar = lugar;
		this.coste_inscripcion = coste_inscripcion;
		this.estado = estado;
	}

	// constructor inscripciones
	public Actividad(String nombre, int plazas, String objetivos, String contenidos, int id_profesor,
			Date f_celebracion, String lugar, double coste_inscripcion) {
		this.nombre = nombre;
		this.plazas = plazas;
		this.objetivos = objetivos;
		this.contenidos = contenidos;
		this.id_profesor = id_profesor;
		this.f_celebracion = f_celebracion;
		this.lugar = lugar;
		this.coste_inscripcion = coste_inscripcion;
	}

	public Actividad(int id_act, String nombre, int plazas, String objetivos, String contenidos,
			 Date f_apertura, Date f_cierre, String estado) {
		this.id_act = id_act;
		this.nombre = nombre;
		this.plazas = plazas;
		this.objetivos = objetivos;
		this.contenidos = contenidos;
		this.f_apertura = f_apertura;
		this.f_cierre = f_cierre;
		this.estado = estado;
	}

	public int getAct_id() {
		return id_act;
	}

	public void setAct_id(int act_id) {
		this.id_act = act_id;
	}

	public int getPlazas() {
		return plazas;
	}

	public void setPlazas(int plazas) {
		this.plazas = plazas;
	}

	public Date getF_apertura() {
		return f_apertura;
	}

	public void setF_apertura(Date f_apertura) {
		this.f_apertura = f_apertura;
	}

	public Date getF_cierre() {
		return f_cierre;
	}

	public void setF_cierre(Date f_cierre) {
		this.f_cierre = f_cierre;
	}

	public int getId_act() {
		return id_act;
	}

	public void setId_act(int id_act) {
		this.id_act = id_act;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObjetivos() {
		return objetivos;
	}

	public void setObjetivos(String objetivos) {
		this.objetivos = objetivos;
	}

	public String getContenidos() {
		return contenidos;
	}

	public void setContenidos(String contenidos) {
		this.contenidos = contenidos;
	}

	public int getId_profesor() {
		return id_profesor;
	}

	public void setId_profesor(int id_profesor) {
		this.id_profesor = id_profesor;
	}

	public double getRemuneracion() {
		return remuneracion;
	}

	public void setRemuneracion(double remuneracion) {
		this.remuneracion = remuneracion;
	}

	public Date getF_celebracion() {
		return f_celebracion;
	}

	public void setF_celebracion(Date f_celebracion) {
		this.f_celebracion = f_celebracion;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public double getCoste_inscripcion() {
		return coste_inscripcion;
	}

	public void setCoste_inscripcion(double coste_inscripcion) {
		this.coste_inscripcion = coste_inscripcion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String toString() {
		String cadena = nombre;
		return cadena;
	}

}
