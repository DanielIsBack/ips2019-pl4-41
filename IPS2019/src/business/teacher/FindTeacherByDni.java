package business.teacher;

import business.dto.TeacherDto;
import persistence.impl.TeacherGateway;

public class FindTeacherByDni {

	private String dni;
	private TeacherGateway gw = new TeacherGateway();

	public FindTeacherByDni(String dni) {
		this.dni = dni;
	}

	public TeacherDto execute() {
		return gw.findTeacherByDni(dni);
	}

}
