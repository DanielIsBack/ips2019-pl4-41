package business.teacher;

import java.util.List;

import business.dto.TeacherDto;
import persistence.impl.TeacherGateway;

public class FindTeachersOfActivity {

	private int activityid;
	private TeacherGateway gw = new TeacherGateway();

	public FindTeachersOfActivity(int activityid) {
		this.activityid = activityid;
	}

	public List<TeacherDto> execute() {
		return gw.findTeachersOfActivity(activityid);
	}
	
}
