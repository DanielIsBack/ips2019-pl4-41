package business.teacher;

import java.util.List;

import business.dto.TeacherDto;
import factory.PersistenceFactory;
import persistence.Gateway;

public class ListTeachers {

	private Gateway<TeacherDto> gw = PersistenceFactory.getTeacherGateway();

	public List<TeacherDto> execute() {
		return gw.findAll();
	}

}
