package business.teacher;

import business.dto.TeacherDto;
import persistence.impl.TeacherGateway;

public class FindTeacherById {

	private int id;
	private TeacherGateway gw = new TeacherGateway();

	public FindTeacherById(int id) {
		this.id = id;
	}

	public TeacherDto execute() {
		return gw.findTeacherById(id);
	}

}
