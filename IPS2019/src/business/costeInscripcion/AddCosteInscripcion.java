package business.costeInscripcion;

import business.dto.CosteInscripcionDto;
import factory.PersistenceFactory;
import persistence.impl.CosteInscripcionGateway;

public class AddCosteInscripcion {

	private CosteInscripcionDto coste;
	private CosteInscripcionGateway gw = PersistenceFactory.getCosteInscripcionGateway();

	public AddCosteInscripcion(CosteInscripcionDto coste) {
		this.coste = coste;
	}

	public void execute() {
		gw.add(coste);
	}

}
