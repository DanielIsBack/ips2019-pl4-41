package business.costeInscripcion;

import factory.PersistenceFactory;
import persistence.impl.CosteInscripcionGateway;

public class GetCosteByIdActividadAndIdParticipante {

	private int idActividad;
	private int idParticipante;
	private CosteInscripcionGateway gw = PersistenceFactory.getCosteInscripcionGateway();

	public GetCosteByIdActividadAndIdParticipante(int idActividad, int idParticipante) {
		this.idActividad = idActividad;
		this.idParticipante = idParticipante;
	}

	public double execute() {
		return gw.getCosteByIdActividadAndIdParticipante(idActividad, idParticipante);
	}
}
