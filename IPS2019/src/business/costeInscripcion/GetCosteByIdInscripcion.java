package business.costeInscripcion;

import factory.PersistenceFactory;
import persistence.impl.CosteInscripcionGateway;

public class GetCosteByIdInscripcion {

	private int id;
	private CosteInscripcionGateway gw = PersistenceFactory.getCosteInscripcionGateway();

	public GetCosteByIdInscripcion(int id) {
		this.id = id;
	}

	public double execute() {
		return gw.getCosteByIdInscripcion(id);
	}
}
