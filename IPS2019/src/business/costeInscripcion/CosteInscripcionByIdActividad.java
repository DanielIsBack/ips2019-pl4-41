package business.costeInscripcion;

import java.util.List;

import business.dto.CosteInscripcionDto;
import factory.PersistenceFactory;
import persistence.impl.CosteInscripcionGateway;

public class CosteInscripcionByIdActividad {

	private int id;
	private CosteInscripcionGateway gw = PersistenceFactory.getCosteInscripcionGateway();

	public CosteInscripcionByIdActividad(int id) {
		this.id = id;
	}

	public List<CosteInscripcionDto> execute() {
		return gw.costeInscripcionByIdActividad(id);
	}

}
