package business.activity;

import java.util.List;

import business.dto.ActivityDto;
import factory.PersistenceFactory;
import persistence.Gateway;

public class ListActivities {

	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public List<ActivityDto> execute() {
		return gw.findAll();
	}

}
