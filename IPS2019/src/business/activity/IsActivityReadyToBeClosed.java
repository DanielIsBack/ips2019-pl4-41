package business.activity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import business.dto.ActivityDto;
import config.Conf;
import util.DbUtil;

public class IsActivityReadyToBeClosed {

	private ActivityDto activity;

	// SQL
	private String query_is_activity_ready_to_be_closed = Conf.getInstance()
			.getProperty("SQL_IS_ACTIVITY_READY_TO_BE_CLOSED");

	public IsActivityReadyToBeClosed(ActivityDto activity) {
		this.activity = activity;
	}

	public boolean execute() {
		Connection c;
		PreparedStatement s;
		ResultSet rs;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_is_activity_ready_to_be_closed);

			// set properties
			s.setInt(1, activity.id);

			// execute insertion
			rs = s.executeQuery();

			if (rs.next()) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}
}
