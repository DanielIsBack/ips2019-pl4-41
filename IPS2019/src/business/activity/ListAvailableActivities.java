package business.activity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import business.dto.ActivityDto;
import config.Conf;
import util.DbUtil;

public class ListAvailableActivities {

	private String query = Conf.getInstance().getProperty("SQL_LIST_AVAILABLE_ACTIVITIES");
	// private String queryCountInsc = dbu.getQuery("");

	public ArrayList<ActivityDto> execute() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		ArrayList<ActivityDto> activities = new ArrayList<ActivityDto>();
		ActivityDto a = null;

		try {
			c = DbUtil.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(query);

			while (rs.next()) {
				a = new ActivityDto();
				a.id = rs.getInt("id");
				a.name = rs.getString("nombre");
				a.numOfPlaces = rs.getInt("plazas");
				a.objectives = rs.getString("objetivos");
				a.contents = rs.getString("contenidos");
				a.startInscriptionDate = rs.getDate("f_apertura_ins");
				a.endInscriptionDate = rs.getDate("f_cierre_ins");
				a.numinsc = rs.getInt("numinsc");
				a.freePlaces = rs.getInt("plazas") - rs.getInt("numinsc");
				activities.add(a);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return activities;
	}
}
