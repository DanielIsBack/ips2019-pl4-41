package business.activity;

import java.util.List;

import business.dto.ActivityDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.ActivityGateway;

public class ListActivitiesWithInscriptions {

	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public List<ActivityDto> execute() {
		return ((ActivityGateway) gw).listActivitiesWithInscriptions();
	}

}
