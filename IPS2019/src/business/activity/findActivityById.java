package business.activity;

import business.dto.ActivityDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.ActivityGateway;

public class findActivityById {
	private int id;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public findActivityById(int id) {
		this.id = id;
	}

	public ActivityDto execute() {
		return ((ActivityGateway) gw).findActivityById(id);
	}
}
