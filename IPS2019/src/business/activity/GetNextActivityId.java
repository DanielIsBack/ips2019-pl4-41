package business.activity;

import factory.PersistenceFactory;
import persistence.impl.ActivityGateway;

public class GetNextActivityId {
	
	ActivityGateway gw = (ActivityGateway) PersistenceFactory.getActivityGateway();
	
	public int execute() {
		return gw.getNextActivityId();
	}
	
}
