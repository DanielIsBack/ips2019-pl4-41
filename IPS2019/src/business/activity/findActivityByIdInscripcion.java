package business.activity;

import business.dto.ActivityDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.ActivityGateway;

public class findActivityByIdInscripcion {
	private int id_insc;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public findActivityByIdInscripcion(int id_insc) {
		this.id_insc = id_insc;
	}

	public ActivityDto execute() {
		return ((ActivityGateway) gw).findActivityByIdInscripcion(id_insc);
	}
}
