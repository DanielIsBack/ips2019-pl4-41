package business.activity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import business.dto.ActivityDto;
import config.Conf;
import util.DbUtil;

public class CloseActivity {

	private ActivityDto activity;

	// SQL
	private String query_close = Conf.getInstance().getProperty("SQL_CLOSE_ACTIVITY");

	public CloseActivity(ActivityDto activity) {
		this.activity = activity;
	}

	public void execute() {
		Connection c;
		PreparedStatement s;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(query_close);

			// set properties
			s.setInt(1, activity.id);

			// execute insertion
			s.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
