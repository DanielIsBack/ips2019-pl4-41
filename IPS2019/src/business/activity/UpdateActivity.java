package business.activity;

import business.dto.ActivityDto;
import exception.util.Argument;
import factory.PersistenceFactory;
import persistence.Gateway;

public class UpdateActivity {

	private ActivityDto activity;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public UpdateActivity(ActivityDto activity) {
		Argument.isNotEmpty(activity.name, "El nombre no puede estar en blanco");
		Argument.isNotNull(activity.startInscriptionDate, "La fecha de Inicio de inscripcion no puede estar en blanco");
		Argument.isNotNull(activity.endInscriptionDate, "La fecha de Fin de inscripcion no puede estar en blanco");
		this.activity = activity;
	}

	public void execute() {
		gw.update(activity);
	}

}
