package business.activity;

import java.util.List;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.ActivityGateway;

public class FindCostesInscripcionesOfActividad {

	
	private int id_act;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public FindCostesInscripcionesOfActividad(int act_id) {
		this.id_act=act_id;
	}

	public List<CosteInscripcionDto> execute() {
		return ((ActivityGateway)gw).findCostesInscripcionesOfActividad(id_act);
	}
}
