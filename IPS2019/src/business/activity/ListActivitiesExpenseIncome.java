package business.activity;

import java.util.List;

import business.dto.ActivityDto;
import business.dto.ActivityTeacherDto;
import business.dto.InscripcionDto;
import factory.PersistenceFactory;
import factory.ServiceFactory;
import persistence.Gateway;

public class ListActivitiesExpenseIncome {

	private Gateway<ActivityDto> gwa = PersistenceFactory.getActivityGateway();

	public List<ActivityDto> execute() {
		List<ActivityDto> activities = gwa.findAll();
		List<InscripcionDto> inscriptions = null;
		for (ActivityDto a : activities) {
			inscriptions = ServiceFactory.getInscriptionService().findInscriptionsByActivity(a.id);
			a.income = calculateIncome(inscriptions);
			a.expense = a.state.equals("Cerrada") ? calculateExpense(a) : 0.0;
			a.balance = calculateBalance(a.income, a.expense);
			a.estimatedIncome = calculateEstimatedIncome(a, inscriptions);
			a.estimatedExpense = calculateEstimatedExpense(a);
			a.estimatedBalance = calculateEstimatedBalance(a.estimatedIncome, a.estimatedExpense);
		}
		return activities;
	}

	private double calculateBalance(double income, double expense) {
		return income - expense;
	}

	private double calculateExpense(ActivityDto activity) {
		List<ActivityTeacherDto> teachers = ServiceFactory.getActivityTeacherService().findAllTeachersOfActivity(activity.id);
		double expense = 0;
		for(ActivityTeacherDto t : teachers) {
			expense += t.remuneration;
		}
		return expense;
	}

	private double calculateIncome(List<InscripcionDto> inscriptions) {
		double income = 0;
		for (InscripcionDto i : inscriptions) {
			income += ServiceFactory.getInscripcionCrudService().calcularCantidadAbonadaAInscripcionById(i.id);
		}
		return income;
	}

	private double calculateEstimatedIncome(ActivityDto activity, List<InscripcionDto> inscriptions) {
		double cantidadEstimada = 0;
		
		for(InscripcionDto i:inscriptions) {
			cantidadEstimada+=ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(i.id);
		}
		return cantidadEstimada;
		//TODO return activity.inscriptionCost * inscriptions.size();
	}

	private double calculateEstimatedExpense(ActivityDto activity) {
		List<ActivityTeacherDto> teachers = ServiceFactory.getActivityTeacherService().findAllTeachersOfActivity(activity.id);
		double expense = 0;
		for(ActivityTeacherDto t : teachers) {
			expense += t.remuneration;
		}
		return expense;
	}

	private double calculateEstimatedBalance(double estimatedIncome, double estimatedExpense) {
		return estimatedIncome - estimatedExpense;
	}

}
