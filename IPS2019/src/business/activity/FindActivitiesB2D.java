package business.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import business.dto.ActivityDto;
import business.dto.SesionDto;
import factory.PersistenceFactory;
import filter.validables.specific.BetweenTwoDates;
import persistence.impl.ActivityGateway;

public class FindActivitiesB2D {
	
	private ActivityGateway gw = (ActivityGateway) PersistenceFactory.getActivityGateway();
	private List<ActivityDto> activities;
	private Date sd, ed;
	
	public FindActivitiesB2D(List<ActivityDto> activities, Date sd, Date ed) {
		this.activities = activities;
		this.sd = sd;
		this.ed = ed;
	}
	
	
	public List<ActivityDto> execute() {
		List<ActivityDto> filteredActivities = new ArrayList<ActivityDto>();
		List<SesionDto> sessions;
		for(ActivityDto a : activities) {
			sessions = gw.findSesionesOfActivity(a.id);
			boolean fits = true;
			for(SesionDto s : sessions) {
				Date date = getSessionDate(s);
				if(!(new BetweenTwoDates(date, sd, ed).isValid())) {
					fits = false;
					break;
				}
			}
			if(fits) filteredActivities.add(a);
		}
		return filteredActivities;
	}
	
	private Date getSessionDate(SesionDto session) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(session.f_comienzo);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

}
