package business.activity;

import business.dto.ActivityDto;
import factory.PersistenceFactory;
import persistence.Gateway;

public class CambiarActividadAInscripcionCerrada {

	private ActivityDto activity;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public CambiarActividadAInscripcionCerrada(ActivityDto activity) {
		this.activity = activity;
	}

	public void execute() {
		// gw.cambiarActividadAInscripcionCerrada(activity);
	}

}
