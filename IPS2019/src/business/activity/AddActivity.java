package business.activity;

import business.dto.ActivityDto;
import exception.BusinessException;
import exception.util.Business;
import factory.PersistenceFactory;
import persistence.Gateway;

public class AddActivity {

	private ActivityDto activity;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public AddActivity(ActivityDto activity) {
		this.activity = activity;
	}

	public void execute() throws BusinessException {
		Business.isNotEmpty(activity.name, "El nombre no puede estar en blanco");
		Business.isNotNull(activity.startInscriptionDate, "La fecha de Inicio de inscripcion no puede estar en blanco");
		Business.isNotNull(activity.endInscriptionDate, "La fecha de Fin de inscripcion no puede estar en blanco");
		Business.datesAreOrdered(activity.startInscriptionDate, activity.endInscriptionDate, 
				"La fecha de inicio de inscripcion debe ser anterior a la de fin de inscripcion");
		
		gw.add(activity);
	}

}
