package business.activity;

import java.util.List;

import business.dto.ActivityDto;
import business.dto.SesionDto;
import factory.PersistenceFactory;
import persistence.Gateway;
import persistence.impl.ActivityGateway;

public class FindSesionesOfActivity {
	private int id_act;
	private Gateway<ActivityDto> gw = PersistenceFactory.getActivityGateway();

	public FindSesionesOfActivity(int id_act) {
		this.id_act = id_act;
	}

	public List<SesionDto> execute() {
		return ((ActivityGateway) gw).findSesionesOfActivity(id_act);
	}
}
