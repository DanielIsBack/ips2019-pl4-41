package util;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

import config.Conf;

public class DbUtil {

	public static Connection getConnection() throws SQLException {
		String URL = Conf.getInstance().getProperty("URL");
		String USER = Conf.getInstance().getProperty("USER");
		String PASS = Conf.getInstance().getProperty("PASS");
		return DriverManager.getConnection(URL, USER, PASS);
	}

	public static void close(Connection c) {
		try {
			if(c != null) c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static long generateUniqueLongIdentifier() {
		long id = -1;
		do {
			UUID uuid = UUID.randomUUID();
			ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
			buffer.putLong(uuid.getLeastSignificantBits());
			buffer.putLong(uuid.getMostSignificantBits());
			BigInteger bi = new BigInteger(buffer.array());
			id = bi.longValue();
		} while (id < 0); // check that id is positive
		return id;
	}

}
