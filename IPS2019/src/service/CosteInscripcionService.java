package service;

import java.util.List;

import business.dto.CosteInscripcionDto;
import exception.BusinessException;

public interface CosteInscripcionService {

	List<CosteInscripcionDto> getCosteInscripcionByIdActividad(int id);

	double getCosteByIdInscripcion(int id);
	
	void addCosteInscripcion(CosteInscripcionDto coste) throws BusinessException;

	double getCosteByIdActividadAndIdParticipante(int idActividad, int idParticipante);

}
