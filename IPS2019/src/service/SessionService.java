package service;

import business.dto.SesionDto;
import exception.BusinessException;

public interface SessionService {

	void addSession(SesionDto session) throws BusinessException;
	
}
