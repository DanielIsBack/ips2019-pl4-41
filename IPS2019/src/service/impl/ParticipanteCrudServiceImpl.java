package service.impl;

import java.util.List;

import business.dto.ParticipanteDto;
import business.participante.AņadirParticipante;
import business.participante.ComprobarParticipanteExiste;
import business.participante.GetColectivoByIdInscripcion;
import business.participante.GetIDParticipanteByEmail;
import business.participante.GetParticipanteById;
import business.participante.GetParticipanteByIdInscripcion;
import service.ParticipanteCrudService;

public class ParticipanteCrudServiceImpl implements ParticipanteCrudService {

	@Override
	public void aņadirParticipante(ParticipanteDto p) {
		new AņadirParticipante(p).execute();

	}

	@Override
	public boolean comprobarParticipanteExiste(ParticipanteDto p) {
		return new ComprobarParticipanteExiste(p).execute();
	}

	@Override
	public int getIDParticipanteByEmail(String dni) {
		return new GetIDParticipanteByEmail(dni).execute();
	}

	@Override
	public ParticipanteDto getParticipanteById(int id) {
		return new GetParticipanteById(id).execute();
	}

	@Override
	public List<ParticipanteDto> getParticipanteByIdInscripcion(int idInscripcion) {
		return new GetParticipanteByIdInscripcion(idInscripcion).execute();
	}

	@Override
	public String getColectivoByIdInscripcion(int id) {
		return new GetColectivoByIdInscripcion(id).execute();
	}
}
