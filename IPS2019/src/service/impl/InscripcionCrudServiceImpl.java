package service.impl;

import java.util.List;

import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.ParticipanteDto;
import business.inscripcion.AvisarInscritoDeCancelaciónActividad;
import business.inscripcion.CalcularCantidadAbonadaAInscripcionById;
import business.inscripcion.CrearInscripcion;
import business.inscripcion.EnviarInscripcionPorCorreoMock;
import business.inscripcion.FindInscriptionByActivityAndParticipante;
import business.inscripcion.FindInscriptionsByActivity;
import business.inscripcion.FindInscriptionsById;
import business.inscripcion.FindNonPaidInscriptionsByActivity;
import business.inscripcion.GetCantidadAbonadaTotal;
import business.inscripcion.NumeroTotalInscripcionesActividad;
import business.inscripcion.ParticipanteEstaInscritoEnActividad;
import business.inscripcion.UpdateEstadoInscripcion;
import service.InscripcionCrudService;

public class InscripcionCrudServiceImpl implements InscripcionCrudService {

	@Override
	public int getNumeroTotalInscripcionesActividad(int idActividad) {
		return new NumeroTotalInscripcionesActividad(idActividad).execute();
	}

	@Override
	public void crearInscripcion(ParticipanteDto p, ActivityDto i, String colectivo) {
		new CrearInscripcion(p, i, colectivo).execute();

	}

	@Override
	public boolean participanteEstaInscritoEnActividad(ParticipanteDto p, ActivityDto a) {
		return new ParticipanteEstaInscritoEnActividad(p, a).execute();
	}

	@Override
	public void enviarInscripcionPorCorreoMock(ParticipanteDto p, ActivityDto a, String colectivo) {
		new EnviarInscripcionPorCorreoMock(p, a, colectivo).execute();

	}

	@Override
	public List<InscripcionDto> findInscriptionsByActivity(int activityId) {
		return new FindInscriptionsByActivity(activityId).execute();
	}

	@Override
	public InscripcionDto findInscriptionsById(int id) {
		return new FindInscriptionsById(id).execute();
	}

	@Override
	public double getCantidadAbonadaTotal(int id_insc) {
		return new GetCantidadAbonadaTotal(id_insc).execute();
	}

	@Override
	public void updateEstadoInscripcion(int id, String estado) {
		new UpdateEstadoInscripcion(id, estado).execute();

	}

	@Override
	public double calcularCantidadAbonadaAInscripcionById(int id) {
		return new CalcularCantidadAbonadaAInscripcionById(id).execute();
	}

	@Override
	public List<InscripcionDto> findNonPaidInscriptionsByActivity(int activityId) {
		return new FindNonPaidInscriptionsByActivity(activityId).execute();
	}

	@Override
	public void avisarInscritoDeCancelaciónActividad(InscripcionDto ins) {
		new AvisarInscritoDeCancelaciónActividad(ins).execute();
		
	}

	@Override
	public InscripcionDto findInscriptionByActivityAndParticipante(int idActividad, int idParticipante) {
		return new FindInscriptionByActivityAndParticipante(idActividad, idParticipante).execute();
	}

}
