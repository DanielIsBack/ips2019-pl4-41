package service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import business.activity.AddActivity;
import business.activity.CloseActivity;
import business.activity.FindActivitiesB2D;
import business.activity.FindCostesInscripcionesOfActividad;
import business.activity.FindSesionesOfActivity;
import business.activity.GetNextActivityId;
import business.activity.IsActivityReadyToBeClosed;
import business.activity.ListActivities;
import business.activity.ListActivitiesExpenseIncome;
import business.activity.ListActivitiesWithInscriptions;
import business.activity.ListAvailableActivities;
import business.activity.UpdateActivity;
import business.activity.findActivityById;
import business.activity.findActivityByIdInscripcion;
import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import business.dto.SesionDto;
import exception.BusinessException;
import service.ActivityService;

public class ActivityServiceImpl implements ActivityService {

	@Override
	public void addActivity(ActivityDto activity) throws BusinessException {
		new AddActivity(activity).execute();
	}

	@Override
	public void deleteActivity(int id) {

	}

	@Override
	public void updateActivity(ActivityDto activity) {
		new UpdateActivity(activity).execute();
	}

	@Override
	public List<ActivityDto> listActivities() {
		return new ListActivities().execute();
	}

	@Override
	public List<ActivityDto> listActivitiesExpenseIncome() {
		return new ListActivitiesExpenseIncome().execute();
	}

	public ArrayList<ActivityDto> ListAvailableActivities() {
		return new ListAvailableActivities().execute();
	}

	@Override
	public boolean isActivityReadyToBeClosed(ActivityDto actdto) {
		return new IsActivityReadyToBeClosed(actdto).execute();
	}

	@Override
	public void closeActivity(ActivityDto actdto) {
		new CloseActivity(actdto).execute();

	}

	@Override
	public ActivityDto findActivityById(int id_act) {
		return new findActivityById(id_act).execute();
	}

	@Override
	public List<ActivityDto> listActivitiesWithInscriptions() {
		return new ListActivitiesWithInscriptions().execute();
	}

	@Override
	public ActivityDto findActivityByIdInscripcion(int id_insc) {
		return new findActivityByIdInscripcion(id_insc).execute();
	}

	@Override
	public List<SesionDto> findSesionesOfActivity(int id_act) {
		return new FindSesionesOfActivity(id_act).execute();
	}
	
	public List<CosteInscripcionDto> findCostesInscripcionesOfActividad(int id_act) {
		return new FindCostesInscripcionesOfActividad(id_act).execute();
	}

	@Override
	public int getNextActivityId() {
		return new GetNextActivityId().execute();
	}

	@Override
	public List<ActivityDto> findActivitiesB2D(List<ActivityDto> activities, Date sd, Date ed) {
		return new FindActivitiesB2D(activities, sd, ed).execute();
	}

}
