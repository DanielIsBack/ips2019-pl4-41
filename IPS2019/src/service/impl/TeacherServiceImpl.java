package service.impl;

import java.util.List;

import business.dto.TeacherDto;
import business.teacher.FindTeacherByDni;
import business.teacher.FindTeacherById;
import business.teacher.FindTeachersOfActivity;
import business.teacher.ListTeachers;
import service.TeacherService;

public class TeacherServiceImpl implements TeacherService {

	@Override
	public void addTeacher(TeacherDto t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteTeacher(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateTeacher(TeacherDto t) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<TeacherDto> listTeachers() {
		return new ListTeachers().execute();
	}

	@Override
	public TeacherDto findTeacherByDni(String dni) {
		return new FindTeacherByDni(dni).execute();
	}

	public TeacherDto findTeacherById(int id) {
		return new FindTeacherById(id).execute();
	}
	
	public List<TeacherDto> findTeachersOfActivity(int ActivityId){
		return new FindTeachersOfActivity(ActivityId).execute();
	}

}
