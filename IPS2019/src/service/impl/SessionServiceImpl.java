package service.impl;

import business.dto.SesionDto;
import business.session.AddSession;
import exception.BusinessException;
import service.SessionService;

public class SessionServiceImpl implements SessionService {

	@Override
	public void addSession(SesionDto session) throws BusinessException {
		new AddSession(session).execute();
	}

}
