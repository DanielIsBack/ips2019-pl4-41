package service.impl;

import java.util.List;

import business.dto.InscripcionDto;
import business.dto.PagoInscripcionDto;
import business.dto.ParticipanteDto;
import business.inscripcion.CancelarInscripcion;
import business.payment.EnviarEmail;
import business.payment.GetPagosByIdInscripcion;
import business.payment.RegisterPayment;
import service.PaymentService;

public class PaymentServiceImpl implements PaymentService {

	@Override
	public String registerPayment(PagoInscripcionDto p) {
		return new RegisterPayment(p).execute();
	}

	@Override
	public void enviarEmail(ParticipanteDto cliente, String mensaje) {
		new EnviarEmail(cliente, mensaje).execute();
	}

	@Override
	public void enviarEmail(List<ParticipanteDto> clientes, String mensaje) {
		new EnviarEmail(clientes, mensaje).execute();
	}

	@Override
	public List<PagoInscripcionDto> getPagosByIdInscripcion(int id) {
		return new GetPagosByIdInscripcion(id).execute();
	}

	@Override
	public void cancelarInscripcion(InscripcionDto inscripcionSeleccionada) {
		new CancelarInscripcion(inscripcionSeleccionada).execute();

	}

}
