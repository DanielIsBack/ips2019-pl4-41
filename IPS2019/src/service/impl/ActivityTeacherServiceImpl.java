package service.impl;

import java.util.List;

import business.activityteacher.AddActivityTeacher;
import business.activityteacher.FindAllTeachersOfActivity;
import business.dto.ActivityTeacherDto;
import exception.BusinessException;
import service.ActivityTeacherService;

public class ActivityTeacherServiceImpl implements ActivityTeacherService {

	@Override
	public void addActivityTeacher(ActivityTeacherDto activityTeacher) throws BusinessException {
		new AddActivityTeacher(activityTeacher).execute();
	}

	@Override
	public List<ActivityTeacherDto> findAllTeachersOfActivity(int activity_id) {
		return new FindAllTeachersOfActivity(activity_id).execute();
	}

}
