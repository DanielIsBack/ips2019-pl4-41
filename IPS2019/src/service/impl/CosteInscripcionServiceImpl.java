package service.impl;

import java.util.List;

import business.costeInscripcion.AddCosteInscripcion;
import business.costeInscripcion.CosteInscripcionByIdActividad;
import business.costeInscripcion.GetCosteByIdActividadAndIdParticipante;
import business.costeInscripcion.GetCosteByIdInscripcion;
import business.dto.CosteInscripcionDto;
import exception.BusinessException;
import service.CosteInscripcionService;

public class CosteInscripcionServiceImpl implements CosteInscripcionService {

	@Override
	public List<CosteInscripcionDto> getCosteInscripcionByIdActividad(int id) {
		return new CosteInscripcionByIdActividad(id).execute();
	}

	@Override
	public double getCosteByIdInscripcion(int id) {
		return new GetCosteByIdInscripcion(id).execute();
	}
	
	@Override
	public double getCosteByIdActividadAndIdParticipante(int idActividad,int idParticipante) {
		return new GetCosteByIdActividadAndIdParticipante(idActividad,idParticipante).execute();
	}

	@Override
	public void addCosteInscripcion(CosteInscripcionDto coste) throws BusinessException {
		new AddCosteInscripcion(coste).execute();
		
	}

}
