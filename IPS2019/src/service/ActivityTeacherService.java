package service;

import java.util.List;

import business.dto.ActivityTeacherDto;
import exception.BusinessException;

public interface ActivityTeacherService {

	void addActivityTeacher(ActivityTeacherDto activityTeacher) throws BusinessException;
	
	List<ActivityTeacherDto> findAllTeachersOfActivity(int activity_id);
	
}
