package service;

import java.util.List;

import business.dto.TeacherDto;

public interface TeacherService {

	void addTeacher(TeacherDto t);

	void deleteTeacher(int id);

	void updateTeacher(TeacherDto t);

	List<TeacherDto> listTeachers();

	TeacherDto findTeacherByDni(String dni);

	TeacherDto findTeacherById(int id);
	
	List<TeacherDto> findTeachersOfActivity(int id);

}
