package service;

import java.util.List;

import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.ParticipanteDto;

public interface InscripcionCrudService {

	int getNumeroTotalInscripcionesActividad(int idActividad);

	void crearInscripcion(ParticipanteDto p, ActivityDto i, String colectivo);

	boolean participanteEstaInscritoEnActividad(ParticipanteDto p, ActivityDto a);

	void enviarInscripcionPorCorreoMock(ParticipanteDto p, ActivityDto a, String colectivo);

	List<InscripcionDto> findInscriptionsByActivity(int activityId);

	InscripcionDto findInscriptionsById(int id);

	double getCantidadAbonadaTotal(int id_insc);

	void updateEstadoInscripcion(int id, String estado);

	List<InscripcionDto> findNonPaidInscriptionsByActivity(int activityId);

	double calcularCantidadAbonadaAInscripcionById(int id);
	
	void avisarInscritoDeCancelaciónActividad(InscripcionDto ins);

	InscripcionDto findInscriptionByActivityAndParticipante(int idActividad, int idParticipante);
}
