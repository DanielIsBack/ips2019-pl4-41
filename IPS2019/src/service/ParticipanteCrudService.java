package service;

import java.util.List;

import business.dto.ParticipanteDto;

public interface ParticipanteCrudService {

	void aņadirParticipante(ParticipanteDto p);

	boolean comprobarParticipanteExiste(ParticipanteDto p);

	int getIDParticipanteByEmail(String dni);

	ParticipanteDto getParticipanteById(int id);

	List<ParticipanteDto> getParticipanteByIdInscripcion(int idInscripcion);

	String getColectivoByIdInscripcion(int id);

}
