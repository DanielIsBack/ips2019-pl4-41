package service;

import java.util.List;

import business.dto.InscripcionDto;
import business.dto.PagoInscripcionDto;
import business.dto.ParticipanteDto;

public interface PaymentService {

	String registerPayment(PagoInscripcionDto p);

	void enviarEmail(ParticipanteDto cliente, String mensaje);

	void enviarEmail(List<ParticipanteDto> clientes, String mensaje);

	List<PagoInscripcionDto> getPagosByIdInscripcion(int id);

	void cancelarInscripcion(InscripcionDto inscripcionSeleccionada);

}
