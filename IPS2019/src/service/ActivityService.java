package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import business.dto.SesionDto;
import exception.BusinessException;

public interface ActivityService {

	void addActivity(ActivityDto activity) throws BusinessException;

	void deleteActivity(int id);

	void updateActivity(ActivityDto activity);

	List<ActivityDto> listActivities();

	List<ActivityDto> listActivitiesExpenseIncome();

	ArrayList<ActivityDto> ListAvailableActivities();

	boolean isActivityReadyToBeClosed(ActivityDto actdto);

	void closeActivity(ActivityDto actdto);

	ActivityDto findActivityById(int id_act);

	List<ActivityDto> listActivitiesWithInscriptions();

	ActivityDto findActivityByIdInscripcion(int id_insc);
	
	List<SesionDto> findSesionesOfActivity(int id_act);
	
	List<CosteInscripcionDto> findCostesInscripcionesOfActividad(int id_act);
	
	int getNextActivityId();
	
	List<ActivityDto> findActivitiesB2D(List<ActivityDto> activities, Date sd, Date ed);

}
