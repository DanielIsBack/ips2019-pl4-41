package persistence;

import java.util.List;

public interface Gateway<T> {

	public void add(T o);

	public void delete(int id);

	public void update(T o);

	public List<T> findAll();

}
