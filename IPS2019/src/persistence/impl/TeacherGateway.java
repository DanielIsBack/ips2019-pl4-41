package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import business.dto.TeacherDto;
import config.Conf;
import persistence.Gateway;
import util.DbUtil;

public class TeacherGateway implements Gateway<TeacherDto> {

	@Override
	public void add(TeacherDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(TeacherDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<TeacherDto> findAll() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		List<TeacherDto> teachers = new ArrayList<TeacherDto>();
		TeacherDto t = null;

		try {
			c = DbUtil.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(Conf.getInstance().getProperty("SQL_LIST_TEACHERS"));

			while (rs.next()) {
				t = new TeacherDto();
				t.dni = rs.getString("dni");
				t.name = rs.getString("nombre");
				t.surname = rs.getString("apellidos");
				teachers.add(t);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return teachers;
	}

	public TeacherDto findTeacherByDni(String dni) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		TeacherDto t = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_TEACHER_BY_DNI"));
			s.setString(1, dni);
			rs = s.executeQuery();

			if (rs.next()) {
				t = new TeacherDto();
				t.id = rs.getInt("id");
				t.dni = rs.getString("dni");
				t.name = rs.getString("nombre");
				t.surname = rs.getString("apellidos");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return t;
	}

	public TeacherDto findTeacherById(int id) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		TeacherDto t = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_TEACHER_BY_ID"));
			s.setInt(1, id);
			rs = s.executeQuery();

			if (rs.next()) {
				t = new TeacherDto();
				t.id = rs.getInt("id");
				t.dni = rs.getString("dni");
				t.name = rs.getString("nombre");
				t.surname = rs.getString("apellidos");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return t;
	}

	public List<TeacherDto> findTeachersOfActivity(int activityid) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<TeacherDto> teacherList = new LinkedList<TeacherDto>();
		TeacherDto teacher = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_TEACHERS_OF_ACTIVITY"));
			s.setInt(1, activityid);
			rs = s.executeQuery();

			while (rs.next()) {
				teacher = new TeacherDto();
				
				teacher.id = rs.getInt("id");
				teacher.dni = rs.getString("dni");
				teacher.name = rs.getString("nombre");
				teacher.surname = rs.getString("apellidos");
				
				teacherList.add(teacher);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		
		return teacherList;
	}

}
