package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.dto.ActivityTeacherDto;
import config.Conf;
import persistence.Gateway;
import util.DbUtil;

public class ActivityTeacherGateway implements Gateway<ActivityTeacherDto> {

	@Override
	public void add(ActivityTeacherDto activityTeacher) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_ACTIVITY_TEACHER"));
			
			// set properties
			s.setInt(1, activityTeacher.activity_id);
			s.setInt(2, activityTeacher.teacher_id);
			s.setDouble(3, activityTeacher.remuneration);

			// execute insertion
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		} 
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(ActivityTeacherDto activityTeacher) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ActivityTeacherDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<ActivityTeacherDto> findAllTeachersOfActivity(int activity_id) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<ActivityTeacherDto> teachers = new ArrayList<ActivityTeacherDto>();
		ActivityTeacherDto t;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_ALL_TEACHERS_OF_ACTIVITY"));
			
			// set properties
			s.setInt(1, activity_id);

			// execute insertion
			rs = s.executeQuery();
			
			while(rs.next()) {
				t = new ActivityTeacherDto();
				t.activity_id = rs.getInt("id_act");
				t.teacher_id = rs.getInt("id_profesor");
				t.remuneration = rs.getDouble("remuneracion");
				teachers.add(t);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		} 
		return teachers;
	}

}
