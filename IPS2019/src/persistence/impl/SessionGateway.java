package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import business.dto.SesionDto;
import config.Conf;
import persistence.Gateway;
import util.DbUtil;

public class SessionGateway implements Gateway<SesionDto> {

	@Override
	public void add(SesionDto session) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_SESSION"));
			
			// set properties
			s.setInt(1, session.id_act);
			s.setTimestamp(2, new Timestamp(session.f_comienzo.getTime()));
			s.setTimestamp(3, new Timestamp(session.f_finalizacion.getTime()));
			s.setString(4, session.lugar);

			// execute insertion
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(SesionDto session) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<SesionDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
