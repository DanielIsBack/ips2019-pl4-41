package persistence.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import business.dto.InscripcionDto;
import business.dto.PagoInscripcionDto;
import business.dto.ParticipanteDto;
import config.Conf;
import factory.ServiceFactory;
import persistence.Gateway;
import util.DbUtil;

public class PaymentGateway implements Gateway<PagoInscripcionDto> {

	@Override
	public void add(PagoInscripcionDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(PagoInscripcionDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<PagoInscripcionDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public String registerPayment(PagoInscripcionDto o) {
		Connection con = null;

		InscripcionDto inscripcion = ServiceFactory.getInscriptionService().findInscriptionsById(o.id_insc);

		String tipo = o.tipo;

		double cantidadPago = o.cantidad;
		double cantidadPagosPrevios = ServiceFactory.getInscripcionCrudService()
				.calcularCantidadAbonadaAInscripcionById(inscripcion.id);
		double cantidad = 0;
		double costeInscripcion = ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripcion.id);
		
		String estado;
		
		if (tipo.equals("Devolución")) {
			cantidad = cantidadPagosPrevios - cantidadPago;
			if(inscripcion.estado_pago.equals("Cancelada_Sin_Devolver"))
				estado = "Cancelada_Devuelta";
			else if(inscripcion.estado_pago.equals("Cancelada_Devuelta"))
				estado = "Cancelada_Devuelta";
			else
				estado = calcularEstado(cantidad, costeInscripcion);
		} else if (tipo.equals("Pago")) {
			cantidad = cantidadPagosPrevios + cantidadPago;
			if(inscripcion.estado_pago.equals("Cancelada_Sin_Devolver"))
				estado = "Cancelada_Sin_Devolver";
			else if(inscripcion.estado_pago.equals("Cancelada_Devuelta"))
				estado = "Cancelada_Devuelta";
			else
				estado = calcularEstado(cantidad, costeInscripcion);
		} else
			throw new RuntimeException("ERROR. EL TIPO DE PAGO NO ES CORRECTO");

		Date fecha = o.fecha;
		java.sql.Date fechaSql = new java.sql.Date(fecha.getTime());

		try {
			con = DbUtil.getConnection();

			PreparedStatement pstUpdateEstadoPago = con
					.prepareStatement(Conf.getInstance().getProperty("UPDATE_ESTADO_INSCRIPCION"));

			pstUpdateEstadoPago.setString(1, estado);
			pstUpdateEstadoPago.setInt(2, inscripcion.id);

			pstUpdateEstadoPago.executeUpdate();

			PreparedStatement pstCrearPago = con.prepareStatement(Conf.getInstance().getProperty("CREATE_PAGO"));

			pstCrearPago.setInt(1, inscripcion.id);
			pstCrearPago.setDate(2, fechaSql);
			pstCrearPago.setDouble(3, cantidadPago);
			pstCrearPago.setString(4, tipo);

			pstCrearPago.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con);
		}

		return estado;
	}

	private String calcularEstado(double cantidad, double costeInscripcion) {
		String estado = "";

		if (cantidad > costeInscripcion)
			estado = "Incorrecto_Mas";
		else if (cantidad < costeInscripcion)
			estado = "Incorrecto_Menos";
		else if (cantidad == 0)
			estado = "Sin_pagar";
		else if (cantidad == costeInscripcion)
			estado = "Correcto";

		return estado;

	}

	public void enviarEmail(List<ParticipanteDto> list, String mensaje) {
		for (ParticipanteDto c : list) {
			String email = c.email;
			String ruta = "Emails/" + email + ".txt";
			File archivo = new File(ruta);
			BufferedWriter bw;
			try {
				if (archivo.exists()) {
					bw = new BufferedWriter(new FileWriter(archivo));
					bw.write(mensaje);
				} else {
					archivo.createNewFile();
					bw = new BufferedWriter(new FileWriter(archivo));
					bw.write(mensaje);
				}
				bw.close();
			} catch (IOException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		}

	}

	public List<PagoInscripcionDto> getPagosByIdInscripcion(int id) {
		Connection con = null;
		ArrayList<PagoInscripcionDto> pagos = new ArrayList<PagoInscripcionDto>();

		try {
			con = DbUtil.getConnection();

			PreparedStatement pst = con
					.prepareStatement(Conf.getInstance().getProperty("SQL_GET_PAGOS_BY_IDINSCRIPCION"));
			pst.setInt(1, id);
			ResultSet rs = pst.executeQuery();

			while (rs.next()) {
				PagoInscripcionDto pago = new PagoInscripcionDto();
				pago.cantidad = rs.getDouble("cantidad_abonada");
				pago.fecha = rs.getDate("f_pago");
				pago.id_insc = rs.getInt("id_ins");
				pago.id = rs.getInt("id");
				pago.tipo = rs.getString("tipo");
				pagos.add(pago);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con);
		}

		return pagos;
	}
}
