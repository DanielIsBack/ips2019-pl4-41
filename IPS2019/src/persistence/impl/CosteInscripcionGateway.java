package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import config.Conf;
import factory.ServiceFactory;
import persistence.Gateway;
import util.DbUtil;

public class CosteInscripcionGateway implements Gateway<CosteInscripcionDto> {

	@Override
	public void add(CosteInscripcionDto o) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_COSTE_INSCRIPCION"));

					
			// set properties
			s.setInt(1, o.id_act);
			s.setString(2, o.nombre);
			s.setDouble(3, o.cantidad);

			// execute insertion
			s.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(CosteInscripcionDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<CosteInscripcionDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<CosteInscripcionDto> costeInscripcionByIdActividad(int id) {
		Connection con = null;
		CosteInscripcionDto costeInscripcionDto = null;
		List<CosteInscripcionDto> costes = new ArrayList<CosteInscripcionDto>();

		try {
			con = DbUtil.getConnection();

			PreparedStatement pstCosteInsc = con
					.prepareStatement(Conf.getInstance().getProperty("GET_COSTE_INSCRIPCION_BY_IDACTIVIDAD"));
			pstCosteInsc.setInt(1, id);
			ResultSet rsCosteInsc = pstCosteInsc.executeQuery();

			while(rsCosteInsc.next()) {
				costeInscripcionDto = new CosteInscripcionDto();
				costeInscripcionDto.id_act = id;
				costeInscripcionDto.cantidad = rsCosteInsc.getDouble("cantidad");
				costeInscripcionDto.nombre = rsCosteInsc.getString("nombre");
				costes.add(costeInscripcionDto);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return costes;
	}

	public double getCosteByIdInscripcion(int id) {
		ActivityDto a = ServiceFactory.getActivityService().findActivityByIdInscripcion(id);

		String colectivo = ServiceFactory.getParticipanteCrudService().getColectivoByIdInscripcion(id);

		List<CosteInscripcionDto> c = ServiceFactory.getCosteInscripcionService().getCosteInscripcionByIdActividad(a.id);
		
		for(CosteInscripcionDto coste : c) {
			if(coste.nombre.equals(colectivo)) {
				return coste.cantidad;
			}
		}
		
		return 0;
	}

	public double getCosteByIdActividadAndIdParticipante(int idActividad, int idParticipante) {
		try {
			return ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(ServiceFactory.getInscripcionCrudService().findInscriptionByActivityAndParticipante(idActividad, idParticipante).id);
		} catch (NullPointerException e) {
			return -1;
		}
	}

}
