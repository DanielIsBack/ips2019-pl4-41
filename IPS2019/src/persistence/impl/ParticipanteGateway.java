package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import business.dto.ParticipanteDto;
import config.Conf;
import persistence.Gateway;
import util.DbUtil;

public class ParticipanteGateway implements Gateway<ParticipanteDto> {

	@Override
	public void add(ParticipanteDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(ParticipanteDto o) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ParticipanteDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public ParticipanteDto findParticipanteById(int id) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		ParticipanteDto p = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_PARTICIPANTE_BY_ID"));
			s.setInt(1, id);
			rs = s.executeQuery();

			if (rs.next()) {
				p = new ParticipanteDto();
				p.id = id;
				p.apellidos = rs.getString("apellidos");
				p.nombre = rs.getString("nombre");
				p.email = rs.getString("email");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return p;
	}

	public List<ParticipanteDto> findParticipanteByIdInscripcion(int idInscripcion) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		ArrayList<ParticipanteDto> p = new ArrayList<ParticipanteDto>();
		ParticipanteDto participante = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_PARTICIPANTE_BY_IDINSCRIPCION"));
			s.setInt(1, idInscripcion);
			rs = s.executeQuery();

			while (rs.next()) {
				participante = new ParticipanteDto();
				participante.id = rs.getInt("id");
				participante.apellidos = rs.getString("apellidos");
				participante.nombre = rs.getString("nombre");
				participante.email = rs.getString("email");
				p.add(participante);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return p;
	}

	public String getColectivoByIdInscripcion(int idInscripcion) {
		Connection con = null;

		String colectivo = "General";

		try {
			con = DbUtil.getConnection();

			PreparedStatement pstColectivo = con
					.prepareStatement(Conf.getInstance().getProperty("GET_COLECTIVO_BY_IDINSCRIPCION"));
			pstColectivo.setInt(1, idInscripcion);
			ResultSet rsColectivo = pstColectivo.executeQuery();

			if (rsColectivo.next())
				colectivo = rsColectivo.getString("colectivo");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(con);
		}
		return colectivo;
	}

}
