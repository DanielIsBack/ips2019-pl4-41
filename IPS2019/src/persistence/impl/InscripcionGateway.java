package persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import business.dto.ActivityDto;
import business.dto.InscripcionDto;
import business.dto.PagoInscripcionDto;
import business.dto.SesionDto;
import config.Conf;
import factory.ServiceFactory;
import filter.validables.specific.BetweenTwoDates;
import persistence.Gateway;
import util.DbUtil;

public class InscripcionGateway implements Gateway<InscripcionDto> {

	@Override
	public void add(InscripcionDto inscription) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(InscripcionDto inscription) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<InscripcionDto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<InscripcionDto> findInscriptionsByActivity(int activityId) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<InscripcionDto> inscriptions = new ArrayList<InscripcionDto>();
		InscripcionDto i = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_INSCRIPTIONS_BY_ACTIVITY"));
			s.setInt(1, activityId);
			rs = s.executeQuery();

			while (rs.next()) {
				i = new InscripcionDto();
				i.id = rs.getInt("id");
				i.id_act = rs.getInt("id_act");
				i.id_part = rs.getInt("id_part");
				i.f_insc = rs.getDate("f_insc");
				//i.colectivo = rs.getString("colectivo");
				i.estado_pago = rs.getString("estado_pago");
				inscriptions.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return inscriptions;
	}

	public InscripcionDto findInscriptionsById(int id) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;

		InscripcionDto i = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_INSCRIPTIONS_BY_ID"));
			s.setInt(1, id);
			rs = s.executeQuery();

			if (rs.next()) {
				i = new InscripcionDto();
				i.id = rs.getInt("id");
				i.id_act = rs.getInt("id_act");
				i.id_part = rs.getInt("id_part");
				i.f_insc = rs.getDate("f_insc");
				i.estado_pago = rs.getString("estado_pago");
				i.colectivo = rs.getString("colectivo");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return i;
	}

	public double calcularCantidadAbonadaAInscripcionById(int id) {

		List<PagoInscripcionDto> lista = ServiceFactory.getPaymentService().getPagosByIdInscripcion(id);

		double cantidadTotal = 0;

		for (PagoInscripcionDto p : lista) {
			if (p.tipo.equals("Devoluci�n"))
				cantidadTotal -= p.cantidad;
			else if (p.tipo.equals("Pago"))
				cantidadTotal += p.cantidad;
		}

		return cantidadTotal;
	}

	public void cancelarInscripcion(InscripcionDto inscripcionSeleccionada) {
		Connection con = null;

		double cantidadAbonada = ServiceFactory.getInscripcionCrudService()
				.calcularCantidadAbonadaAInscripcionById(inscripcionSeleccionada.id);
		double cantidadAPagar = ServiceFactory.getCosteInscripcionService().getCosteByIdInscripcion(inscripcionSeleccionada.id);
		int devolver = 0;

		try {
			con = DbUtil.getConnection();

			ActivityDto actividad = ServiceFactory.getActivityService()
					.findActivityById(inscripcionSeleccionada.id_act);
			String nombre = actividad.name;

			devolver = comprobarFecha(actividad);

			double cantidadADevolver = calcularCantidadADevolver(cantidadAbonada, cantidadAPagar,  devolver);

			PreparedStatement pst = con.prepareStatement(Conf.getInstance().getProperty("SQL_CANCELAR_INSCRIPCION"));
			
			String estado = "Cancelada_Sin_Devolver";
			String mensaje = "Se ha cancelado tu inscripcion a la actividad " + nombre + ".";
			String mensaje2 = "Se ha cancelado la inscripci�n correctamente.";
			
			if(cantidadADevolver == 0) {
				estado = "Cancelada_Devuelta";
			}else if(cantidadADevolver>0) {
				mensaje += "\nSe te proceder� a devolverte " + cantidadADevolver + "�";
				mensaje2 += "\nSe ha de devolver " + cantidadADevolver + "�";
			}else {
				mensaje += "\nA�n debes pagar " + -cantidadADevolver + "�";
				mensaje2 += "\nA�n quedan pendientes de pagar " + -cantidadADevolver + "�";
			}
			
			pst.setString(1, estado);
			pst.setInt(2, inscripcionSeleccionada.id);

			pst.executeUpdate();
			
			actualizarEstadoActividad(actividad);

			ServiceFactory.getPaymentService().enviarEmail(ServiceFactory.getParticipanteCrudService()
					.getParticipanteByIdInscripcion(inscripcionSeleccionada.id), mensaje);

			JOptionPane.showMessageDialog(null, mensaje2);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private void actualizarEstadoActividad(ActivityDto actividad) {
		Date date = new Date();
		Date startingDate = actividad.startInscriptionDate;
		Date endDate = actividad.endInscriptionDate;
		
		boolean entreFechas = new BetweenTwoDates(date, startingDate, endDate).isValid();
		
		if(entreFechas) {
			actividad.state = "Periodo de Inscripci�n";
			ServiceFactory.getActivityService().updateActivity(actividad);
		}
	}

	private static double calcularCantidadADevolver(double cantidadAbonada, double cantidadAPagar, int opcion) {
		if (opcion == 1) {
			return cantidadAbonada;
			
		} else if (opcion == 2) {
			return cantidadAbonada - (cantidadAPagar * 0.5);
		} else {
			return cantidadAbonada - cantidadAPagar;
		}
	}

	private static int comprobarFecha(ActivityDto actividad) {
		List<SesionDto> sesiones = ServiceFactory.getActivityService().findSesionesOfActivity(actividad.id);
		
		Date fechaActividad = null;
		
		for(SesionDto s: sesiones) {
			if(fechaActividad == null) {
				fechaActividad = s.f_comienzo;
			}else if(s.f_comienzo.before(fechaActividad)) {
				fechaActividad = s.f_comienzo;
			}
		}
		
		Date fechaHoy = new Date();

		int dias = (int) ((fechaActividad.getTime() - fechaHoy.getTime()) / 86400000);

		if (dias >= 7)
			return 1;
		else if (dias >= 3)
			return 2;
		else if (dias >= 0)
			return 3;
		else
			throw new IllegalArgumentException(
					"No se puede cancelar una inscripcion despues de que la actividad se haya celebrado");
	}

	public List<InscripcionDto> findNonPaidInscriptionsByActivity(int activityId) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<InscripcionDto> inscriptions = new ArrayList<InscripcionDto>();
		InscripcionDto i = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_NON_PAID_INSCRIPTIONS_BY_ACTIVITY"));
			s.setInt(1, activityId);
			rs = s.executeQuery();

			while (rs.next()) {
				i = new InscripcionDto();
				i.id = rs.getInt("id");
				i.id_act = rs.getInt("id_act");
				i.id_part = rs.getInt("id_part");
				i.f_insc = rs.getDate("f_insc");
				i.colectivo = rs.getString("colectivo");
				i.estado_pago = rs.getString("estado_pago");
				inscriptions.add(i);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return inscriptions;
	}

	public double getCantidadAbonadaTotal(int id) {
		double cantidadTotal = 0;

		ArrayList<PagoInscripcionDto> pagos = (ArrayList<PagoInscripcionDto>) ServiceFactory.getPaymentService().getPagosByIdInscripcion(id);

		for (PagoInscripcionDto pago : pagos) {
			cantidadTotal += pago.cantidad;
		}

		return cantidadTotal;
	}

	public void updateEstadoInscripcion(int id, String estado) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("UPDATE_ESTADO_INSCRIPCION"));
			s.setString(1, estado);
			s.setInt(2, id);
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
	}

	public InscripcionDto findInscriptionsByActivityAndParticipante(int idActividad, int idParticipante) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;

		InscripcionDto i = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_INSCRIPTIONS_BY_ACTIVITY_AND_PARTICIPANTE"));
			s.setInt(1, idActividad);
			s.setInt(2, idParticipante);
			rs = s.executeQuery();

			if (rs.next()) {
				i = new InscripcionDto();
				i.id = rs.getInt("id");
				i.id_act = rs.getInt("id_act");
				i.id_part = rs.getInt("id_part");
				i.f_insc = rs.getDate("f_insc");
				i.estado_pago = rs.getString("estado_pago");
				i.colectivo = rs.getString("colectivo");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return i;
	}

}
