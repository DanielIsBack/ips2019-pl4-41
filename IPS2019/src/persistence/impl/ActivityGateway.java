package persistence.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import business.dto.ActivityDto;
import business.dto.CosteInscripcionDto;
import business.dto.SesionDto;
import config.Conf;
import persistence.Gateway;
import util.DbUtil;

public class ActivityGateway implements Gateway<ActivityDto> {

	@Override
	public void add(ActivityDto activity) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			
			if(activity.state == null)
				s = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_ACTIVITY"));
			else
				s = c.prepareStatement(Conf.getInstance().getProperty("SQL_ADD_ACTIVITY_CON_ESTADO"));
				

			//TODO
			
			// set properties
			s.setString(1, activity.name);
			s.setString(2, activity.objectives);
			s.setString(3, activity.contents);
			s.setDate(4, new Date(activity.startInscriptionDate.getTime()));
			s.setDate(5, new Date(activity.endInscriptionDate.getTime()));
			s.setInt(6, activity.numOfPlaces);
			
			if(activity.state != null)
				s.setString(7, activity.state);

			// execute insertion
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}

	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(ActivityDto activity) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			// s =
			// c.prepareStatement(Conf.getInstance().getProperty("SQL_UPDATE_ACTIVITY_STATUS"));
			s = c.prepareStatement("UPDATE actividad SET estado=? WHERE id=?"); 

			// set properties
//			s.setString(1, activity.name);
//			s.setString(2, activity.objectives);
//			s.setString(3, activity.contents);
//			s.setInt(4, activity.teacher);
//			s.setDouble(5, activity.remuneration);
//			s.setDate(6, new Date(activity.curseDate.getTime()));
//			s.setDate(7, new Date(activity.startInscriptionDate.getTime()));
//			s.setDate(8, new Date(activity.endInscriptionDate.getTime()));
//			s.setString(9, activity.space);
//			s.setInt(10, activity.numOfPlaces);
//			s.setDouble(11, activity.inscriptionCost);
//			s.setTime(12, activity.sHour);
//			s.setTime(13, activity.eHour);
//			s.setInt(14, activity.id);
			s.setString(1, activity.state);
			s.setInt(2, activity.id);

			// execute insertion
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}

	}

	@Override
	public List<ActivityDto> findAll() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		List<ActivityDto> activities = new ArrayList<ActivityDto>();
		ActivityDto a = null;

		try {
			c = DbUtil.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(Conf.getInstance().getProperty("SQL_LIST_ACTIVITIES"));

			//TODO
			
			while (rs.next()) {
				a = new ActivityDto();
				a.id = rs.getInt("id");
				a.name = rs.getString("nombre");
				a.numOfPlaces = rs.getInt("plazas");
				a.objectives = String.valueOf(rs.getClob("objetivos"));
				a.contents = String.valueOf(rs.getClob("contenidos"));
//				a.teacher = rs.getInt("id_profesor"); TODO
//				a.remuneration = rs.getDouble("remuneracion"); TODO
				a.startInscriptionDate = rs.getDate("f_apertura_ins");
				a.endInscriptionDate = rs.getDate("f_cierre_ins");
				//a.space = rs.getString("lugar"); TODO
				//a.inscriptionCost = rs.getDouble("coste_ins"); //TODO
//				a.curseDate = rs.getDate("f_celebracion"); TODO
				a.state = rs.getString("estado");
				activities.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return activities;
	}

	public ActivityDto findActivityById(int id) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		ActivityDto a = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_ACTIVITY_BY_ID"));
			s.setInt(1, id);
			rs = s.executeQuery();

			//TODO
			
			if (rs.next()) {
				a = new ActivityDto();
				a.id = rs.getInt("id");
				a.name = rs.getString("nombre");
				a.numOfPlaces = rs.getInt("plazas");
				a.objectives = rs.getString("objetivos");
				a.contents = rs.getString("contenidos");
//				a.teacher = rs.getInt("id_profesor"); TODO
//				a.remuneration = rs.getDouble("remuneracion"); TODO
				a.startInscriptionDate = rs.getDate("f_apertura_ins");
				a.endInscriptionDate = rs.getDate("f_cierre_ins");
				//a.space = rs.getString("lugar"); TODO
				//a.inscriptionCost = rs.getDouble("coste_ins"); //TODO
//				a.curseDate = rs.getDate("f_celebracion"); TODO
				a.state = rs.getString("estado");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return a;
	}

	public List<ActivityDto> listActivitiesWithInscriptions() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		List<ActivityDto> activities = new ArrayList<ActivityDto>();
		ActivityDto a = null;

		try {
			c = DbUtil.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(Conf.getInstance().getProperty("SQL_LIST_ACTIVITIES_WITH_NON_PAID_INSCRIPTIONS"));

			while (rs.next()) {
				a = new ActivityDto();
				a.id = rs.getInt("id");
				a.name = rs.getString("nombre");
				a.numOfPlaces = rs.getInt("plazas");
				a.objectives = rs.getString("objetivos");
				a.contents = rs.getString("contenidos");
				a.startInscriptionDate = rs.getDate("f_apertura_ins");
				a.endInscriptionDate = rs.getDate("f_cierre_ins");
				a.state = rs.getString("estado");
				activities.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
		return activities;
	}

	public void cambiarActividadAInscripcionCerrada(ActivityDto activity) {
		Connection c = null;
		PreparedStatement s = null;

		try {
			c = DbUtil.getConnection();
			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_CAMBIAR_ACTIVIDAD_A_INSCRIPCION_CERRADA"));

			// set properties
			s.setInt(1, activity.id);

			// execute update
			s.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtil.close(c);
		}
	}

	public ActivityDto findActivityByIdInscripcion(int id_insc) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		ActivityDto a = null;
		try {
			c = DbUtil.getConnection();

			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_ACTIVIDAD_BY_IDINSCRIPCION"));
			s.setInt(1, id_insc);
			rs = s.executeQuery();

			if (rs.next()) {
				a = new ActivityDto();
				a.id = rs.getInt("id");
				a.name = rs.getString("nombre");
				a.numOfPlaces = rs.getInt("plazas");
				a.objectives = String.valueOf(rs.getClob("objetivos"));
				a.contents = String.valueOf(rs.getClob("contenidos"));
				a.state = rs.getString("estado");

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			DbUtil.close(c);

		}

		return a;

	}
	
	public List<SesionDto> findSesionesOfActivity(int id_act) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<SesionDto> sesiones = new LinkedList<SesionDto>();
		SesionDto sesion = null;
		try {
			c = DbUtil.getConnection();

			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_SESIONES_OF_ACTIVITY"));
			s.setInt(1, id_act);
			rs = s.executeQuery();

			while (rs.next()) {
				sesion = new SesionDto();
				sesion.id = rs.getInt("id");
				sesion.id_act = rs.getInt("id_act");
				sesion.f_comienzo = rs.getTimestamp("f_comienzo");
				sesion.f_finalizacion = rs.getTimestamp("f_finalizacion");
				sesion.lugar = rs.getString("lugar");
				sesiones.add(sesion);
				//System.out.println(sesion.toString());

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			DbUtil.close(c);

		}

		return sesiones;
	}
	
	
	public List<CosteInscripcionDto> findCostesInscripcionesOfActividad(int id_act) {
		Connection c = null;
		PreparedStatement s = null;
		ResultSet rs = null;
		List<CosteInscripcionDto> costs = new ArrayList<CosteInscripcionDto>();
		
		try {
			c = DbUtil.getConnection();

			s = c.prepareStatement(Conf.getInstance().getProperty("SQL_FIND_COSTES_INSCRICIONES_OF_ACTIVIDAD"));
			s.setInt(1, id_act);
			rs = s.executeQuery();

			while (rs.next()) {
				CosteInscripcionDto coste = new CosteInscripcionDto();
				coste.id_act = rs.getInt("id_act");
				coste.nombre = rs.getString("nombre");
				coste.cantidad = rs.getDouble("cantidad");
				costs.add(coste);
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			DbUtil.close(c);

		}

		return costs;
	}
	
	public int getNextActivityId() {
		Connection c = null;
		Statement s = null;
		ResultSet rs = null;
		
		try {
			c = DbUtil.getConnection();
			s = c.createStatement();
			rs = s.executeQuery(Conf.getInstance().getProperty("SQL_FIND_HIGHEST_ACTIVITY_ID"));
			if (rs.next()) {
				return rs.getInt("id") + 1;
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			DbUtil.close(c);
		}

		return -1;
	}

}
